# sil:	(Java) SImple Library
Copyright (c) 2017 Christophe Marc BERTONCINI (see LICENSE text file).

## Aim
This library is born from my "way of programming". It doesn't provide any "new" or "incredible" feature.
It's here to simplify my work.

## Convenience
All classes designed to "simplify" some current actions are prefixed with "CVS_" as "Convenience Store".
This permits to quickly see them in autocomplete mode.

# Dependencies
<table>
<tr>
	<th>Project</th>	<th>Website</th>	<th>License</th>
</tr>
<tr>
	<td>JavaMail API</td>	<td>https://javaee.github.io/javamail/</td>	<td>CDDL 1.1 & GPL 2.0</td>
</tr>
<tr>
	<td>SLF4J API</td>	<td>https://www.slf4j.org/</td>	<td>MIT License</td>
</tr>
</table>

# Keywords
- common needs
- resources
- cache (memory / file)
- logs
- validation (with math expressions) 
- gui specific

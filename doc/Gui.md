# GUI
Package sil.gui provides some way to handle views with "model".

## Model
Model:
- is observed by views to show values, states, ...
- modified depending on user entries

## Linkers
Component in views are updated for different reasons:
- titles/labels depending on selected language
- texts from some stream read, logs ?
- enabled/disabled depending on some model state

To get the most flexibility, component in views can be coupled with different linkers to achieve needs.

## Components
Some specific components are provided.

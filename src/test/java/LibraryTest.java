/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

import org.junit.Test;

import fr.bcm.lib.sil.CVS_Jar;
import fr.bcm.lib.sil.math.MathBase;

import java.net.URL;

import static org.junit.Assert.*;

/**
 * Test library
 */
public class LibraryTest {

	/**
	 * Test CVS_Jar 
	 */
	@Test
	public void testJarURL()
	{
		URL url = CVS_Jar.getJarURL(LibraryTest.class);
		assertTrue("Must find Jar URL", url != null);
		System.out.println("Jar URL : " + url.toString());

	}

	/**
	 * Test MathBase
	 */
	@Test
	public void testMathBase()
	{
		String decimal	= "13";
		String binary	= "1101";
		String hexa		= "d";

		MathBase base10 = new MathBase(10);
		MathBase base2 = new MathBase(2);
		MathBase base16 = new MathBase(16);

		String t;
		t = MathBase.convert(base10, base2, decimal);
		assertTrue(t.compareTo(binary) == 0);

		t = MathBase.convert(base10, base16, decimal);
		assertTrue(t.compareTo(hexa) == 0);

		t = MathBase.convert(base16, base2, hexa);
		assertTrue(t.compareTo(binary) == 0);
	}

}

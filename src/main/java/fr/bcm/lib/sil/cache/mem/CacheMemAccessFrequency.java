/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.mem;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.cache.ECacheStrategy;
import fr.bcm.lib.sil.cache.state.AccessFrequencyState;

/**
 * Manage cache in memory - AccessFrequency strategy
 * 
 * @param <Key> @see ICacheMem
 * @param <Value> @see ICacheMem
 */
public class CacheMemAccessFrequency<Key, Value> extends CacheMemBase<Key, Value, AccessFrequencyState> {

	/**
	 * Maximum number of cached values
	 */
	protected int maxSize;

	/***
	 * 
	 * @return max size
	 */
	public int getMaxSize()
	{
		return this.maxSize;
	}

	/**
	 * Ctor
	 * @param maxSize
	 */
	public CacheMemAccessFrequency(int maxSize)
	{
		DebugBox.predicate(maxSize > 0);
		this.strategy = ECacheStrategy.ACCESS_FREQUENCY;
		this.maxSize = maxSize;
	}

	@Override
	protected AccessFrequencyState createDefaultState()
	{
		return new AccessFrequencyState();
	}

	@Override
	public void sweep()
	{
		this.sweepBySize(this.maxSize);
	}

}
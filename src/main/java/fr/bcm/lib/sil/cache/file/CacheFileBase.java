/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.TreeMap;

import fr.bcm.lib.sil.cache.CacheBase;
import fr.bcm.lib.sil.cache.state.ICacheState;
import fr.bcm.lib.sil.file.CVS_File;
import fr.bcm.lib.sil.file.SubFileLocator;

/**
 * Base class to manage cached files
 */
public abstract class CacheFileBase<State extends ICacheState & Comparable<State>> extends CacheBase implements ICacheFile {

	/**  */
	SubFileLocator	fileLocator;

	/**
	 * Cache state of files to limit IO
	 */
	Map<String, State>	stateCache;

	/**
	 * Ctor
	 * @param root
	 */
	public CacheFileBase(final File root)
	{
		this.fileLocator = new SubFileLocator(root);
		this.stateCache = new TreeMap<>();
	}

	@Override
	public void clear()
	{
		// Clear state cache
		this.stateCache.clear();
		// And files
		CVS_File.delete(this.fileLocator.getRoot());
	}

	/** Let child create default state */
	protected abstract State createDefaultState();

	/** Let child create state */
	protected abstract State createState(final File file);

	// Has sense ?
	// protected void sweepBySize(final int maxSize)

	/**
	 * Clear all entries that are (strictly) less than given state
	 */
	protected void sweepByLimit(final State limit)
	{
		// Scan all files
		String rootPrefix = this.fileLocator.getRoot().getAbsolutePath();
		Path dir = Paths.get(rootPrefix);
		try(DirectoryStream<Path> ds = Files.newDirectoryStream(dir))
		{
			int prefixSize = rootPrefix.length();

			for(Path path : ds)
			{
				File file = path.toFile();
				if(!file.isFile())
				{
					continue;
				}

				String key = file.getAbsolutePath().substring(0, prefixSize);
				State state = this.stateCache.get(key);
				if(state == null)
				{	// Not in cache => create one
					state = this.createState(file);
				}

				if((state != null) && (state.compareTo(limit) < 0))
				{	// Valid state and "older"
					file.delete();
					this.stateCache.remove(key);
				}
			}
		}
		catch(IOException e)
		{

		}
	}

	@Override
	public FileInputStream getInputStream(String key) throws IOException
	{
		File file = this.fileLocator.cptFileLocation(key);

		State state = this.stateCache.get(key);
		if(state == null)
		{
			state = this.createState(file);
			if(state == null)
			{	// File does not exist
				return null;
			}

			this.stateCache.put(key, state);
		}

		state.readHit();
		return new FileInputStream(file);
	}

	@Override
	public FileOutputStream getOutputStream(String key) throws IOException
	{
		File file = this.fileLocator.cptFileLocation(key);

		State state = this.stateCache.get(key);
		if(state == null)
		{
			if(!file.exists())
			{	// Create empty file if needed
				file.getParentFile().mkdirs();
				file.createNewFile();
			}

			state = this.createState(file);
			if(state == null)
			{	// File does not exist
				throw new FileNotFoundException();
			}

			this.stateCache.put(key, state);
		}

		state.writeHit();
		return new FileOutputStream(file);
	}

	@Override
	public FileInputStream getInputStream(String key, ICacheStreamInitier initier) throws IOException
	{
		FileInputStream is = this.getInputStream(key);
		if(is == null)
		{
			try(FileOutputStream os = this.getOutputStream(key))
			{
				initier.initStreamContent(os);
			}

			is = this.getInputStream(key);
		}

		return is;
	}

	@Override
	public void remove(String key)
	{
		this.stateCache.remove(key);
		File file = this.fileLocator.cptFileLocation(key);
		if(file.isFile())
		{
			file.delete();
		}
	}

}
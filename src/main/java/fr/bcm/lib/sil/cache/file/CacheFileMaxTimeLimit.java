/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.file;

import java.io.File;
import java.time.Duration;
import java.time.LocalDateTime;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.cache.ECacheStrategy;
import fr.bcm.lib.sil.cache.state.WriteTimeLimitState;
import fr.bcm.lib.sil.rsrc.ResourceInfo;

/**
 * Manage cached files - MaxTimeLimit strategy
 */
public class CacheFileMaxTimeLimit extends CacheFileBase<WriteTimeLimitState> {

	/**
	 * Delay before expiration
	 */
	protected Duration expireDelay;

	/**
	 * 
	 * @return expiration delay
	 */
	public Duration getExpireDelay()
	{
		return this.expireDelay;
	}

	/**
	 * Ctor
	 * @param root
	 * @param expireDelay
	 */
	public CacheFileMaxTimeLimit(final File root, final Duration expireDelay)
	{
		super(root);

		DebugBox.predicateNotNull(expireDelay);
		DebugBox.predicate(!expireDelay.isNegative() && !expireDelay.isZero());
		this.strategy = ECacheStrategy.WRITE_TIME_LIMIT;
		this.expireDelay = expireDelay;
	}

	@Override
	protected WriteTimeLimitState createDefaultState()
	{
		return new WriteTimeLimitState();
	}

	@Override
	protected WriteTimeLimitState createState(File file)
	{
		DebugBox.predicateNotNull(file);
		ResourceInfo ri = new ResourceInfo(file);
		LocalDateTime dt = ri.getLastModified();

		return (dt != null) ? new WriteTimeLimitState(dt) : null;
	}

	@Override
	public void sweep()
	{
		WriteTimeLimitState tls = WriteTimeLimitState.createLimitFromNow(this.expireDelay);
		this.sweepByLimit(tls);
	}

}
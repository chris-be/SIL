/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache;

/**
 * @Convenience
 */
public abstract class CacheBase implements ICache {

	/**
	 * Strategy
	 */
	protected ECacheStrategy strategy;

	/**
	 * @see ICache
	 */
	@Override
	public ECacheStrategy getStrategy()
	{
		return this.strategy;
	}

	/**
	 * @see ICache
	 */
	@Override
	public abstract void clear();

	/**
	 * @see ICache
	 */
	@Override
	public abstract void sweep();

}
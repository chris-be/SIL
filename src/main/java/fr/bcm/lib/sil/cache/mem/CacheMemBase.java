/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.mem;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.cache.CacheBase;
import fr.bcm.lib.sil.cache.ICache;
import fr.bcm.lib.sil.cache.state.ICacheState;
import fr.bcm.lib.sil.patterns.Capsule;

/**
 * Base class to manage cache in memory
 * 
 * @param <Key> @see ICacheMem
 * @param <Value> @see ICacheMem
 * @param <State> Class used to manage state of item in cache
 */
public abstract class CacheMemBase<Key, Value, State extends ICacheState & Comparable<State>> extends CacheBase implements ICacheMem<Key, Value> {

	protected class CacheStateValue
	{
		public Value value;
		public State state;

		CacheStateValue(Value value, State state)
		{
			DebugBox.predicateNotNull(state);

			this.value = value;
			this.state = state;
		}
	}

	Map<Key, CacheStateValue>	entries;

	/**
	 * Use TreeMap by default
	 */
	public CacheMemBase()
	{
		this(new TreeMap<>());
	}

	/**
	 * @param entries Custom Map implementation
	 */
	public CacheMemBase(Map<Key, CacheStateValue> entries)
	{
		DebugBox.predicateNotNull(entries);
		this.entries = entries;
	}

	/**
	 * @see ICache
	 */
	@Override
	public void clear()
	{
		this.entries.clear();
	}

	protected CacheStateValue getEntry(final Key key)
	{
		return this.entries.get(key);
	}

	protected void setEntry(final Key key, final CacheStateValue csv)
	{
		this.entries.put(key, csv);
	}

	/** Let child create default state */
	protected abstract State createDefaultState();

	@Override
	public Capsule<Value> get(final Key key)
	{
		CacheStateValue csv = this.getEntry(key);
		if(csv == null)
		{	// No entry
			return new Capsule<>();
		}

		csv.state.readHit();
		return new Capsule<>(csv.value);
	}

	@Override
	public void set(final Key key, final Value value)
	{
		CacheStateValue csv = this.getEntry(key);
		if(csv == null)
		{
			csv = new CacheStateValue(value, this.createDefaultState());
			this.setEntry(key, csv);
		}

		csv.state.writeHit();
	}

	@Override
	public void remove(final Key key)
	{
		this.entries.remove(key);
	}

	/**
	 * Clear all entries that are more than maxSize (use Compare to select "lower" states)
	 * @param maxSize
	 */
	protected void sweepBySize(final int maxSize)
	{
		int nbToRemove = this.entries.size() - maxSize;
		if(nbToRemove <= 0)
		{	// Nothing to do
			return;
		}

		// Scan all entries to select the "oldest" ones
		TreeMap<State, Key> select = new TreeMap<>();
		for(Map.Entry<Key, CacheStateValue> entry : this.entries.entrySet())
		{
			State state = entry.getValue().state;
			if(select.size() < nbToRemove)
			{	// Not enough entries
				select.put(state, entry.getKey());
			}
			else
			{	// Add entry only if "less than" max
				State max = select.lastKey();
				if(state.compareTo(max) < 0)
				{
					select.remove(max);
					select.put(state, entry.getKey());
				}
			}
		}

		// Remove selected entries
		for(Key key : select.values())
		{
			this.entries.remove(key);
		}
	}

	/**
	 * Clear all entries that are (strictly) less than given state
	 * @param limit
	 */
	protected void sweepByLimit(final State limit)
	{
		// Scan all entries to select
		Iterator<Map.Entry<Key, CacheStateValue>> iter = this.entries.entrySet().iterator();
		while(iter.hasNext())
		{
			Map.Entry<Key, CacheStateValue> entry = iter.next();
			State toTest = entry.getValue().state;
			if(toTest.compareTo(limit) < 0)
			{
				iter.remove();
			}
		}
	}

}
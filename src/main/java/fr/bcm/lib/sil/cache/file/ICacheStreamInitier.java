/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.file;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Contract for cache using streams.
 */
public interface ICacheStreamInitier {

	public void initStreamContent(OutputStream outputStream) throws IOException;

}
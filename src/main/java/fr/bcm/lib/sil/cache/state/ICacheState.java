/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.state;

/**
 * Contract to handle cache values life cycle
 */
public interface ICacheState {

	/**
	 * "value" was read from cache
	 */
	public void readHit();

	/**
	 * "value" was "updated/written" in cache
	 */
	public void writeHit();

}
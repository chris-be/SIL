/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.file;

import java.io.File;
import java.util.Iterator;
import java.util.zip.Adler32;
import java.util.zip.Checksum;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.math.MathBase;

/**
 * Create key for file
 * <br>generate sub directory from hash to limit number of files per directory.
 */
public class KeyFileMaker {

	/**
	 * No need to be very good
	 * @return
	 */
	static Checksum createDefaultHasher()
	{
		return new Adler32();
	}

	static private String	hashCodes = "0123456789abcdefghijklmnopqrstuvwxyz";

	protected Checksum hasher;

	protected MathBase encrypter;

	/**
	 * Use default hasher
	 */
	public KeyFileMaker()
	{
		this(KeyFileMaker.createDefaultHasher());
	}

	/**
	 * 
	 */
	public KeyFileMaker(Checksum hasher)
	{
		DebugBox.predicateNotNull(hasher);
		this.hasher = hasher;
		this.encrypter = new MathBase(hashCodes);
	}

	/**
	 * Create a "sub directory" from hash
	 * @param resourceName Should be a valid file name (no check)
	 * @return "encoded hash"
	 */
	public String createSubDir(String resourceName)
	{
		this.hasher.reset();
		for(Iterator<Integer> iter = resourceName.codePoints().iterator() ; iter.hasNext() ; )
		{
			int cp = iter.next();
			for( ; cp > 0 ; cp >>= 8)
			{
				byte b = (byte)(cp);
				hasher.update(b);
			}
		}

		return this.encrypter.toString(hasher.getValue());
	}

	/**
	 * Create key (computed sub dir / resourceName)
	 * @param resourceName Should be a valid file name (no check)
	 * @return Key: "encoded hash" / resourceName
	 */
	public String createKey(String resourceName)
	{
		String prefix = this.createSubDir(resourceName);

		StringBuilder sb = new StringBuilder(prefix);
		sb.append(File.separator);
		sb.append(resourceName);

		return sb.toString();
	}

}
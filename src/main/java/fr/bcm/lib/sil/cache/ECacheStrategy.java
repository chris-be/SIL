/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache;

/**
 * Cache strategies
 * <br>By default, each cache keeps everything. It's up to user to call "sweep" from time to time (@see SweepCacheThread).
 */
public enum ECacheStrategy {

	/** Limit size
	 * <br>"sweep" removes entries only if "size limit" is reached
	 * <br>entries with less access frequency are deleted
	 */
	  ACCESS_FREQUENCY

	/** Time limit
	 * <br>"sweep" removes any entry which last access (read or write) is too old
	 */
	, TIME_LIMIT

	/** Write Time limit
	 * <br>"sweep" removes any entry which last write access is too old
	 * <br>"get" removes also an entry if "delay expired"
	 */
	, WRITE_TIME_LIMIT

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.mem;

import fr.bcm.lib.sil.cache.ICache;
import fr.bcm.lib.sil.patterns.Capsule;

/**
 * Contract for cache in memory
 * 
 * @param <Key> Key used to identify cache entry
 * @param <Value> Value of cache entry
 */
public interface ICacheMem<Key, Value> extends ICache {

	/**
	 * Get value - read hit
	 * @param key
	 * @return Capsule of value (caontins nothinf if cache has no entry)
	 */
	public Capsule<Value> get(final Key key);

	/**
	 * Add (or replace) value - write hit
	 * @param key
	 * @param value
	 */
	public void set(final Key key, final Value value);

	/**
	 * Remove entry from cache
	 * @param key
	 */
	public void remove(final Key key);

}
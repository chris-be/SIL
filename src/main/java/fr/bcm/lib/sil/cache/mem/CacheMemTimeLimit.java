/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.mem;

import java.time.Duration;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.cache.ECacheStrategy;
import fr.bcm.lib.sil.cache.state.TimeLimitState;

/**
 * Manage cache in memory - TimeLimit strategy
 * 
 * @param <Key> @see ICacheMem
 * @param <Value> @see ICacheMem 
 */
public class CacheMemTimeLimit<Key, Value> extends CacheMemBase<Key, Value, TimeLimitState> {

	/**
	 * Delay before expiration
	 */
	protected Duration expireDelay;

	/**
	 * 
	 * @return expiration delay
	 */
	public Duration getExpireDelay()
	{
		return this.expireDelay;
	}

	/**
	 * Ctor
	 * @param expireDelay
	 */
	public CacheMemTimeLimit(final Duration expireDelay)
	{
		DebugBox.predicateNotNull(expireDelay);
		DebugBox.predicate(!expireDelay.isNegative() && !expireDelay.isZero());
		this.strategy = ECacheStrategy.TIME_LIMIT;
		this.expireDelay = expireDelay;
	}

	@Override
	protected TimeLimitState createDefaultState()
	{
		return new TimeLimitState();
	}

	@Override
	public void sweep()
	{
		TimeLimitState tls = TimeLimitState.createLimitFromNow(this.expireDelay);
		this.sweepByLimit(tls);
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import fr.bcm.lib.sil.cache.ICache;

/**
 * Contract for cache in memory
 */
public interface ICacheFile extends ICache {

	/**
	 * Open for read - read hit
	 * @param key Valid filename
	 * @throws IOException
	 */
	public FileInputStream getInputStream(String key) throws IOException;

	/**
	 * Open for write - write hit
	 * @param key Valid filename
	 * @throws IOException
	 */
	public FileOutputStream getOutputStream(String key) throws IOException;

	/**
	 * Open for read - read hit
	 * @param key Valid filename
	 * @param initier Called to create/fill file content if not exists
	 * @throws IOException
	 */
	public FileInputStream getInputStream(String key, ICacheStreamInitier initier) throws IOException;

	/**
	 * Remove entry from cache - delete file
	 * @param key
	 */
	public void remove(String key);

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache;

/**
 * Contract for cache
 * <br>common methods
 */
public interface ICache {

	/**
	 * Strategy
	 * @return strategy
	 */
	ECacheStrategy getStrategy();

	/**
	 * Clear all cache - delete everything
	 */
	public void clear();

	/**
	 * Sweep old entries depending on strategy
	 */
	public void sweep();

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.state;

import java.time.Duration;
import java.time.LocalDateTime;

import fr.bcm.lib.sil.CVS_Comparable;
import fr.bcm.lib.sil.DebugBox;

/**
 * CacheState for "time limit" strategy
 * 
 * <br>Comparable: from older to newer
 */
public class TimeLimitState implements ICacheState, Comparable<TimeLimitState> {

	/** Last read
	 * <br>null: never
	 */
	protected LocalDateTime readTimestamp;

	/**
	 * Ctor
	 */
	public TimeLimitState()
	{
		this.reset();
	}

	/**
	 * To create a "limit"
	 * @param readTimestamp
	 */
	public TimeLimitState(LocalDateTime readTimestamp)
	{
		DebugBox.predicateNotNull(readTimestamp);
		this.readTimestamp = readTimestamp;
	}

	/**
	 * Set to "never"
	 */
	public void reset()
	{
		this.readTimestamp = null;
	}

	@Override
	public void readHit()
	{
		this.readTimestamp = LocalDateTime.now();
	}

	@Override
	public void writeHit()
	{	}

	@Override
	public int compareTo(TimeLimitState o)
	{
		return CVS_Comparable.compare(this.readTimestamp, o.readTimestamp);
	}

	/**
	 * 
	 * @param duration
	 * @return TimeLimitState set to now - duration
	 */
	public static TimeLimitState createLimitFromNow(final Duration duration)
	{
		LocalDateTime limit = LocalDateTime.now().minus(duration);
		return new TimeLimitState(limit);
	}
	
}
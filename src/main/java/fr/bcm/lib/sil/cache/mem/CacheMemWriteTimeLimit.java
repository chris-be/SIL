/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.mem;

import java.time.Duration;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.cache.ECacheStrategy;
import fr.bcm.lib.sil.cache.state.WriteTimeLimitState;
import fr.bcm.lib.sil.patterns.Capsule;

/**
 * Manage cache in memory - WriteTimeLimit strategy
 * 
 * @param <Key> @see ICacheMem
 * @param <Value> @see ICacheMem
 */
public class CacheMemWriteTimeLimit<Key, Value> extends CacheMemBase<Key, Value, WriteTimeLimitState> {

	/**
	 * Delay before expiration
	 */
	protected Duration expireDelay;

	/**
	 * 
	 * @return expiration delay
	 */
	public Duration getExpireDelay()
	{
		return this.expireDelay;
	}

	/**
	 * Ctor
	 * @param expireDelay
	 */
	public CacheMemWriteTimeLimit(final Duration expireDelay)
	{
		DebugBox.predicateNotNull(expireDelay);
		DebugBox.predicate(!expireDelay.isNegative() && !expireDelay.isZero());
		this.strategy = ECacheStrategy.WRITE_TIME_LIMIT;
		this.expireDelay = expireDelay;
	}

	@Override
	protected WriteTimeLimitState createDefaultState()
	{
		return new WriteTimeLimitState();
	}

	@Override
	public void sweep()
	{
		WriteTimeLimitState tls = WriteTimeLimitState.createLimitFromNow(this.expireDelay);
		this.sweepByLimit(tls);
	}

	@Override
	public Capsule<Value> get(final Key key)
	{
		CacheMemBase<Key, Value, WriteTimeLimitState>.CacheStateValue csv = super.getEntry(key);
		if(csv == null)
		{	// No entry
			return new Capsule<>();
		}

		WriteTimeLimitState tls = WriteTimeLimitState.createLimitFromNow(this.expireDelay);
		if(csv.state.compareTo(tls) < 0)
		{	// Elapsed
			super.remove(key);
			return new Capsule<>();
		}

		csv.state.readHit();
		return new Capsule<>(csv.value);
	}

}
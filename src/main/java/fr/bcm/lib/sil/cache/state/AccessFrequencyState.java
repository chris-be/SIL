/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.state;

/**
 * CacheState for "frequency" strategy
 * <br>Reads and writes are counted together
 * <br>Should only be "read hits" ? Cache is made to "read" more than "write"...
 * 
 * <br>Comparable: from less hit to most hit
 */
public class AccessFrequencyState implements ICacheState, Comparable<AccessFrequencyState> {

	/** Hit counter */
	protected int hits;

	/**
	 * Ctor
	 */
	public AccessFrequencyState()
	{
		this.reset();
	}

	/**
	 * No more hit
	 */
	public void reset()
	{
		this.hits = 0;
	}

	/**
	 * @see ICacheState
	 */
	@Override
	public void readHit()
	{
		++this.hits;
	}

	/**
	 * @see ICacheState
	 */
	public void writeHit()
	{
		++this.hits;
	}

	@Override
	public int compareTo(AccessFrequencyState o)
	{
		return this.hits - o.hits;
	}

}
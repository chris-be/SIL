/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.cache.state;

import java.time.Duration;
import java.time.LocalDateTime;

import fr.bcm.lib.sil.CVS_Comparable;
import fr.bcm.lib.sil.DebugBox;

/**
 * CacheState for "max time limit" strategy
 * 
 * <br>Comparable: from older to newer
 */
public class WriteTimeLimitState implements ICacheState, Comparable<WriteTimeLimitState> {

	/** Last write
	 * <br>null : never
	 */
	protected LocalDateTime writeTimestamp;

	/**
	 * Ctor
	 */
	public WriteTimeLimitState()
	{
		this.reset();
	}

	/**
	 * To create a "limit"
	 * @param writeTimestamp
	 */
	public WriteTimeLimitState(LocalDateTime writeTimestamp)
	{
		DebugBox.predicateNotNull(writeTimestamp);
		this.writeTimestamp = writeTimestamp;
	}

	/**
	 * Set to "never"
	 */
	public void reset()
	{
		this.writeTimestamp = null;
	}

	@Override
	public void readHit()
	{	}

	@Override
	public void writeHit()
	{
		this.writeTimestamp = LocalDateTime.now();
	}

	@Override
	public int compareTo(WriteTimeLimitState o)
	{
		return CVS_Comparable.compare(this.writeTimestamp, o.writeTimestamp);
	}

	/**
	 * 
	 * @param duration
	 * @return TimeLimitState set to now - duration
	 */
	public static WriteTimeLimitState createLimitFromNow(final Duration duration)
	{
		LocalDateTime limit = LocalDateTime.now().minus(duration);
		return new WriteTimeLimitState(limit);
	}
	
}
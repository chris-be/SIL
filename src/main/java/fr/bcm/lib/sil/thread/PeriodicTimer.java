/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.thread;

import fr.bcm.lib.sil.DebugBox;

/**
 * TimeWatcher
 */
public class PeriodicTimer extends TimeWatcher {

	public static int DEFAULT_DELAY = 1000;

	/** Delay between each action */
	protected volatile int	sleepDelayMs;

	public int getSleepDelay()
	{
		return this.sleepDelayMs;
	}

	public void setSleepDelay(int ms)
	{
		DebugBox.predicate(ms > 0);
		this.sleepDelayMs = ms;
	}

	public PeriodicTimer()
	{
		this.setSleepDelay(PeriodicTimer.DEFAULT_DELAY);
		this.start();
	}

	public PeriodicTimer(int sleepDelayMs)
	{
		this.setSleepDelay(sleepDelayMs);
	}

	/**
	 * @Convenience
	 * @throws InterruptedException
	 */
	public void sleepIfNeeded() throws InterruptedException
	{
		this.stop();
		long delta = this.elapsed().toMillis() % this.sleepDelayMs;

		Thread.sleep(delta);
	}

}
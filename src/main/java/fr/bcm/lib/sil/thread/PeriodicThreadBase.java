/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.thread;

import fr.bcm.lib.sil.log.Log;

/**
 * PeriodicThreadBase: for thread that periodically do an action
 */
public abstract class PeriodicThreadBase extends Thread {

	/** Timer */
	protected PeriodicTimer timer;

	public int getSleepDelay()
	{
		return this.timer.getSleepDelay();
	}

	public void setSleepDelay(int ms)
	{
		this.timer.setSleepDelay(ms);
	}

	public PeriodicThreadBase(int sleepDelayMs)
	{
		this.timer = new PeriodicTimer(sleepDelayMs);
	}

	/**
	 * Call/override if needed
	 * @param e
	 */
	protected void unhandledException(Exception e)
	{
		Log.printError(e);
	}

	/**
	 * join thread
	 * @Convenience
	 */
	public void tryJoin()
	{
		try
		{
			this.join();
		}
		catch (InterruptedException e)
		{	// Ignore
		}
	}

}
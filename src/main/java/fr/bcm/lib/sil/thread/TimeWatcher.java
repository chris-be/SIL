/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.thread;

import java.time.Duration;
import java.time.Instant;

/**
 * TimeWatcher
 */
public class TimeWatcher {

	protected Instant startInstant;
	protected Instant stopInstant;

	public Instant getStart()
	{
		return this.startInstant;
	}

	public Instant getStop()
	{
		return this.stopInstant;
	}

	public TimeWatcher()
	{
	}

	public void start()
	{
		this.startInstant = Instant.now();
	}

	public void stop()
	{
		this.stopInstant = Instant.now();
	}

	public Duration elapsed()
	{
		return Duration.between(this.startInstant, this.stopInstant);
	}

}
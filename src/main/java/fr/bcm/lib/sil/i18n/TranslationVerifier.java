/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.i18n;

import java.util.ResourceBundle;
import java.util.TreeSet;

import fr.bcm.lib.sil.DebugBox;

/**
 * Use in "debug mode" for example to verify at startup if every translation is ok.
 * @Convenience
 */
public class TranslationVerifier {

	protected ResourceBundle rb;

	protected TreeSet<String> missingKeys;

	/**
	 * Memorize keys that were not found by check methods
	 */
	protected boolean keepMissingKeys;

	public boolean isKeepMissingKeys()
	{
		return this.keepMissingKeys;
	}

	public void setKeepMissingKeys(boolean v)
	{
		this.keepMissingKeys = v;
	}

	/**
	 * 
	 * @param rb ResourceBundle to use
	 */
	public TranslationVerifier(final ResourceBundle rb)
	{
		this.missingKeys = new TreeSet<>();
		this.keepMissingKeys = false;

		this.setResourceBundle(rb);	
	}

	public void setResourceBundle(final ResourceBundle rb)
	{
		DebugBox.predicateNotNull(rb);
		this.rb = rb;
	}

	/**
	 * @return All checked keys that were missing
	 */
	public Iterable<String> getMissingKeys()
	{
		return this.missingKeys;
	}

	/**
	 * Check if key is known
	 * @param key
	 */
	public boolean check(final String key)
	{
		DebugBox.predicateNotNull(key);
		boolean ok = rb.containsKey(key);
		if(!ok && this.keepMissingKeys)
		{
			this.missingKeys.add(key);
		}

		return ok;
	}

	/**
	 * @Convenience
	 * @param key
	 * @param ignoreNull If true and key is null, return true
	 */
	public boolean check(final String key, boolean ignoreNull)
	{
		if(key == null)
		{
			return ignoreNull;
		}

		return this.check(key);
	}

}
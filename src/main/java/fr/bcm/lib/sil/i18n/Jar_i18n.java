/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.i18n;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import fr.bcm.lib.sil.CVS_Jar;
import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.file.SubFileLocator;
import fr.bcm.lib.sil.reflection.CVS_Reflection;
import fr.bcm.lib.sil.rsrc.ResourceClassLoader;

/**
 * Allow to use a path in jar file for translations. 
 * @Convenience
 */
public class Jar_i18n {

	protected ResourceClassLoader classLoader;
	protected SubFileLocator fileLocator;

	/**
	 * Default: caller class JAR and i18n path
	 * @throws IOException 
	 * @throws IllegalArgumentException
	 */
	public Jar_i18n() throws IllegalArgumentException, IOException
	{
		Class<?> cl = CVS_Reflection.getCallerClass();
		this.subCtor(cl, "i18n");
	}

	/**
	 * Default: caller class JAR
	 * @param path
	 * @throws IOException
	 */
	public Jar_i18n(String path) throws IOException
	{
		Class<?> cl = CVS_Reflection.getCallerClass();
		this.subCtor(cl, path);
	}

	/**
	 * 
	 * @param cl Classpath of jar
	 * @param path Path in jar
	 * @throws IOException
	 */
	public Jar_i18n(final Class<?> cl, String path) throws IOException
	{
		this.subCtor(cl, path);
	}

	protected final void subCtor(final Class<?> cl, String path) throws IOException
	{
		DebugBox.predicateNotNull(cl);
		this.classLoader = CVS_Jar.getResourceClassLoader(cl);

		this.fileLocator = new SubFileLocator();
		this.fileLocator.setRoot(path);
	}

	/**
	 * Get ResourceBundle in jar under path
	 * @param baseName
	 * @param locale
	 */
	public ResourceBundle getResourceBundle(String baseName, final Locale locale)
	{
		File file = this.fileLocator.cptFileLocation(baseName);
		return ResourceBundle.getBundle(file.getPath(), locale, this.classLoader);
	}

	/** Clear cache */
	public void clearCache()
	{
		ResourceBundle.clearCache(this.classLoader);
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.i18n;

/**
 * Contract to translate an entity
 * <br>@see TranslationVerifier
 */
public interface ITranslateKey {

	/**
	 * Get key to translate
	 */
	public String getTranslateKey();

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.function.Consumer;

import fr.bcm.lib.sil.reflection.CVS_Reflection;
import fr.bcm.lib.sil.rsrc.FileLoader;
import fr.bcm.lib.sil.rsrc.IResourceLoader;
import fr.bcm.lib.sil.rsrc.JNLPLoader;
import fr.bcm.lib.sil.rsrc.JarLoader;
import fr.bcm.lib.sil.rsrc.ResourceClassLoader;

/**
 * "Convenience" for Jar
 *
 * @Convenience
 */
public class CVS_Jar {

	/**
	 * Separator used in Java for package
	 */
	public static final char PACKAGE_SEPARATOR = '.';

	/**
	 * File like
	 */
	public static final char separatorChar = PACKAGE_SEPARATOR;
	/**
	 *  File like
	 */
	public static final String separator = Character.toString(separatorChar);

	/**
	 * Check if url has file protocol
	 * @param url
	 * @return true if url has file protocol
	 */
	private static boolean hasFileProtocol(final URL url)
	{
		return "file".equalsIgnoreCase(url.getProtocol());
	}

	/**
	 * Convert classPath to "file path"
	 * (from "ddd.xxx.yyy.zzz.ext" to "ddd/xxx/yyy/zzz/ext")
	 * 
	 * @param classPath
	 * @return Converted path
	 */
	public static String createPath(final String classPath)
	{
		DebugBox.predicateNotNull(classPath);
		return classPath.replace(CVS_Jar.separator, File.separator);
	}

	/**
	 * Create location of "resourceName" depending on classPath
	 * @param classPath Can be null or empty
	 * @param resourceName
	 * @return File location (ddd/xxx/.../file.ext)
	 */
	public static File createFileLocation(final String classPath, final String resourceName)
	{
		File innerFile;
		if(!CVS_String.isNullOrEmpty(classPath))
		{
			String innerPath = CVS_Jar.createPath(classPath);
			innerFile = new File(innerPath, resourceName);
		}
		else
		{
			innerFile = new File(resourceName);
		}

		return innerFile;
	}

	/**
	 * Create URL for an entry of a Jar file
	 * @param jarFile
	 * @param jarEntryName
	 * @throws MalformedURLException
	 */
	public static URL createLocalURL(final String jarFile, final String jarEntryName) throws MalformedURLException
	{
		// jar: file:/path/file.jar !/ entryPath/entry
		String file = "file:" + jarFile + "!/" + jarEntryName;
		return new URL("jar", null, file);
	}

	/**
	 * Get Jar location where given class lies, or directory if not a Jar file (uncompressed or debugging from an IDE)
	 *
	 * @param cl Class "inside" Jar
	 * @return URL of Jar file / directory
	 * @throws InternalError
	 */
	public static URL getJarURL(final Class<?> cl) throws InternalError
	{
		URL url;

		try
		{
			ProtectionDomain	protectionDomain = cl.getProtectionDomain();
			CodeSource			codeSource = protectionDomain.getCodeSource();

			url	= codeSource.getLocation();
			String	path = url.getPath();
			if(!path.startsWith("/"))
			{
				url = new URL(url.getProtocol(), url.getHost(), "/" + path);
			}
		}
		catch(Exception e)
		{
			// url = null;
			throw new InternalError(e.getMessage());
		}

		assert url != null : "Should throw an exception if a problem occurs";
		return url;
	}

	/**
	 * Get ResourceClassLoader
	 *
	 * @param cl Class "inside" Jar
	 * @return ResourceClassLoader
	 * @throws IOException
	 * @throws IllegalArgumentException
	 */
	public static ResourceClassLoader getResourceClassLoader(final Class<?> cl) throws IllegalArgumentException, IOException
	{
		IResourceLoader resourceLoader = CVS_Jar.getResourceLoader(cl);
		return new ResourceClassLoader(resourceLoader);
	}

	/**
	 * Create adapted IResourceLoader for jarURL
	 *
	 * @param jarURL URL of Jar
	 * @return Adapted ResourceLoader
	 */
	public static IResourceLoader getResourceLoader(final URL jarURL) throws IllegalArgumentException, IOException
	{
		DebugBox.predicateNotNull(jarURL);
		DebugBox.predicate(CVS_Jar.hasFileProtocol(jarURL));

		File file = new File(jarURL.getFile());
		if(file.isDirectory())
		{	// Not a Jar file
			return new FileLoader(file);
		}

		return new JarLoader(file);
	}

	/**
	 * Create adapted IResourceLoader where "cl" lies
	 *
	 * @param cl Class lying in "jar" Ex: MyClass.class
	 * @return Adapted ResourceLoader (or null)
	 */
	public static IResourceLoader getResourceLoader(final Class<?> cl) throws IllegalArgumentException, IOException
	{
		DebugBox.predicateNotNull(cl);

		if(CVS_JNLP.isRunning())
		{	// JNLP case ?
			return new JNLPLoader(cl.getClassLoader());
		}

		// Go for File
		URL url = CVS_Jar.getJarURL(cl);
		if(CVS_Jar.hasFileProtocol(url))
		{
			return CVS_Jar.getResourceLoader(url);
		}

		throw new IOException("Unhandled protocol");
	}

	/**
	 * Create adapted IResourceLoader where "class of caller" lies
	 *
	 * @return Adapted ResourceLoader (or null) 
	 */
	public static IResourceLoader getResourceLoader() throws IllegalArgumentException, IOException
	{
		Class<?> cl = CVS_Reflection.getCallerClass();
		return CVS_Jar.getResourceLoader(cl);
	}

//
//
//

	/**
	 * Create an input stream from a jar resource (works also in debug mode when no jar is created) and call consumer.
	 * @Convenience
	 *
	 * @param cl Class lying in "jar" Ex: MyClass.class
	 * @param classPath
	 * @param resourceName
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public static void workOnResource(final Class<?> cl, final String classPath, final String resourceName, Consumer<InputStream> consumer) throws IllegalArgumentException, Exception
	{
		DebugBox.predicateNotNull(cl);
		DebugBox.predicateNotNull(resourceName);

		try(IResourceLoader loader = CVS_Jar.getResourceLoader(cl))
		{
			assert loader != null : "Should throw an exception if a problem occurs";

			File innerFile = CVS_Jar.createFileLocation(classPath, resourceName);
			try(InputStream is = loader.openResource(innerFile))
			{
				consumer.accept(is);
			}
		}
	}

}
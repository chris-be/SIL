/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.mail;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import fr.bcm.lib.sil.CVS_Collection;
import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.old2new.CVS_Date;

/**
 * Send mail
 * <br>Requires com.sun.mail:javax.mail
 * @Convenience
 */
public class Mail {

	/**
	 * Default smtp server to use
	 */
	public static String DEFAULT_SMTP_SERVER		= "localhost";

	protected String from;
	protected ArrayList<String> receivers;

	protected LocalDateTime date;
	protected String subject;
	protected String message;
	protected String messageType;

	public void setFrom(String from)
	{
		this.from = from;
	}

	public void setDate(LocalDateTime date)
	{
		this.date = date;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	/**
	 * Set message content
	 * @param message
	 * @param messageType "text/plain" "text/html"
	 */
	public void setMessage(String message, String messageType)
	{
		DebugBox.predicateNotNull(message);
		DebugBox.predicateNotNull(messageType);
		this.message = message;
		this.messageType = messageType;
	}

	public Mail()
	{
		this.date = null;
		this.receivers = new ArrayList<>();
	}

	/**
	 * @Convenience
	 */
	public Mail(String from, String subject)
	{
		this();
		this.setFrom(from);
		this.setSubject(subject);
	}

	/**
	 * Add one or more receivers
	 * @param receivers
	 */
	public void addReceiver(String... receivers)
	{
		CVS_Collection.addAll(this.receivers, receivers);
	}

	/**
	 * Add receivers list - RFC 822 @see InternetAddress.parse
	 * @param receivers
	 * @throws AddressException
	 */
	public void addReceiverList(String receivers) throws AddressException
	{
		// InternetAddress.parse("address_list", false)
		for(InternetAddress ia : InternetAddress.parse(receivers))
		{
			this.receivers.add(ia.toString());
		}
	}

//
// JavaMail API
//

	/**
	 * @Convenience
	 * @throws AddressException
	 */
	public InternetAddress[] createReceiversInternetAdresses() throws AddressException
	{
		InternetAddress[] ias = new InternetAddress[this.receivers.size()];
		int i = 0;
		for(String receiver : this.receivers)
		{
			ias[i++] = new InternetAddress(receiver);
		}

		return ias;
	}

	public MimeMessage prepareMail(Session session) throws MessagingException
	{
		DebugBox.predicateNotNull(this.from);
		DebugBox.predicateNotNull(this.message);
		DebugBox.predicate(this.receivers.size() > 0);

		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(from);
		InternetAddress[] ias = this.createReceiversInternetAdresses();
		msg.setRecipients(Message.RecipientType.TO, ias);
		// msg.setReplyTo(list);

		LocalDateTime ldt = (this.date != null) ? this.date : LocalDateTime.now();
		msg.setSentDate(CVS_Date.toDate(ldt));

		msg.setSubject(this.subject);
		msg.setContent(this.message, this.messageType);

		return msg;
	}

	/**
	 * Contact smtp and send mail
	 * @param mail
	 * @param smtpServer Smtp server to contact (null: use default")
	 * @param debug Enable debug
	 * @throws MessagingException
	 */
	public static void send(Mail mail, String smtpServer, boolean debug) throws MessagingException
	{
		if(smtpServer == null)
		{
			smtpServer = Mail.DEFAULT_SMTP_SERVER;
		}

		Properties props = new Properties();
		// SMTP
		props.put("mail.smtp.host", smtpServer);

		Session session = Session.getDefaultInstance(props, null);
		// "mail.debug" ?
		session.setDebug(debug);

		MimeMessage msg = mail.prepareMail(session);

		Transport.send(msg);
	}

	/**
	 * Contact smtp and send mail - debug false
	 * @see #send(Mail mail, String smtpServer, boolean debug)
	 * @Convenience
	 */
	public static void send(Mail mail, String smtpServer) throws MessagingException
	{
		Mail.send(mail, smtpServer, false);
	}

	/**
	 * @Convenience
	 * @param address
	 * @return True if valid
	 */
	public static boolean isAddressValid(String address)
	{
		boolean valid;

		try
		{
			@SuppressWarnings("unused")
			InternetAddress ia = new InternetAddress(address);
			valid = true;
		}
		catch(AddressException ae)
		{
			valid = false;
		}

		return valid;
	}

}
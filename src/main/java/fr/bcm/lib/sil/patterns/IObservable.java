/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.patterns;

/**
 * Contract for an Observable
 * 
 * @param <Event> Type of event
 */
public interface IObservable<Event extends IEvent> {

	/**
	 * Add observer
	 * @param observer
	 */
	void addObserver(IObserver<Event> observer);

	/**
	 * Remove observer
	 * @param observer
	 */
	void removeObserver(final IObserver<Event> observer);

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.patterns;

/**
 * Mini value container
 * <br>Permits to keep track of "null" value
 * 
 * @param <TValue> Type of value to hold
 */
public class Capsule<TValue> {

	/** Value is set */
	private boolean set;

	/** Value */
	private TValue	value;

	/***
	 * 
	 * @return if a value was set
	 */
	public boolean isSet()
	{
		return this.set;
	}

	/**
	 * 
	 * @return value
	 */
	public TValue getValue()
	{
		return this.value;
	}

	/**
	 * Ctor: no value
	 */
	public Capsule()
	{
		this.set = false;
	}

	/**
	 * Ctor
	 * @param value Value to use
	 */
	public Capsule(final TValue value)
	{
		this.set(value);
	}

	/**
	 * Change state to "hold nothing"
	 */
	public void unset()
	{
		this.set = false;
	}

	/**
	 * Set value and change state to "hold value"
	 * @param value
	 */
	public void set(final TValue value)
	{
		this.set = true;
		this.value = value;
	}

	/**
	 * Needed ???
	 */
	public void reset()
	{
		this.unset();
		this.value = null;
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.patterns;

/**
 * Contract for "deep copy"
 * <br>To use CVS_Pattern#getCopy, constructor must be public
 * 
 * @param <Type> Type that can be copied
 */
public interface IDeepSettable<Type> {

	/**
	 * Set this values with "toCopy" values
	 * <br>Must copy value, not copy references !!!!!
	 * @param toCopy
	 */
	public void deepSet(final Type toCopy);

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.patterns;

import java.util.HashSet;
import java.util.function.Consumer;

import fr.bcm.lib.sil.DebugBox;

/**
 * To manage observers
 */
public class ObserverSet<TEvent extends IEvent> {

	private HashSet<IObserver<TEvent>>	observers;

	public ObserverSet()
	{
		this.observers = new HashSet<>();
	}

	public void add(IObserver<TEvent> observer)
	{
		DebugBox.predicateNotNull(observer);
		this.observers.add(observer);
	}

	public void remove(final IObserver<TEvent> observer)
	{
		DebugBox.predicateNotNull(observer);
		this.observers.remove(observer);
	}

	/**
	 * Fire event to all observers
	 * @param event
	 * @param source
	 */
	public void fireEvent(final TEvent event, final Object source)
	{
		for(IObserver<TEvent> observer : this.observers)
		{
			try
			{
				observer.observed(event, source);
			}
			catch(Exception e)
			{
				// Fail silently
			}
		}
	}

	/**
	 * Fire event to all observers
	 * @param action What to do
	 */
	public void fireEvent(final Consumer<IObserver<TEvent>> action)
	{
		for(IObserver<TEvent> observer : this.observers)
		{
			action.accept(observer);
		}
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.patterns;

import java.util.Iterator;

import fr.bcm.lib.sil.DebugBox;

/**
 * IteratorWrapper: let to pass an "iterable" object starting from an iterator one.
 * 
 * @Convenience
 */
public class IteratorWrapper<T extends Object> implements Iterable<T>
{
	/**
	 * Iterator wrapped
	 */
	protected Iterator<T> iterator;

	/**
	 * 
	 * @param iterator
	 */
	public IteratorWrapper(Iterator<T> iterator)
	{
		DebugBox.predicateNotNull(iterator);
		this.iterator = iterator;
	}

	@Override
	public Iterator<T> iterator()
	{
		return this.iterator;
	}

}
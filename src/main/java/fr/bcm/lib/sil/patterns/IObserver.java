/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.patterns;

/**
 * Contract for an Observer
 */
public interface IObserver<TEvent> {

	/**
	 * 
	 * @param event
	 * @param source Instance that fired event
	 */
	void observed(final TEvent event, final Object source);

}
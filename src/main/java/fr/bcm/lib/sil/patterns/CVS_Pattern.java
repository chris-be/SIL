/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.patterns;

import fr.bcm.lib.sil.DebugBox;

/**
 * "Convenience" for Collection
 *
 * @Convenience
 */
public class CVS_Pattern {

	/**
	 * Create a "deep copy" by using "set"
	 * @param toCopy Values to copy - T constructor must be public
	 * @throws RuntimeException
	*/
	@SuppressWarnings("unchecked")
	public static <T extends IDeepSettable<T>> T getCopy(T toCopy) throws RuntimeException
	{
		if(toCopy == null)
		{
			return null;
		}

		Class<T> cc = (Class<T>)toCopy.getClass();
		return CVS_Pattern._getCopy(cc, toCopy);
	}

	/**
	 * Create an instance of given type and call set
	 * @param type Type of copy wanted - CC constructor must be public
	 * @param toCopy Values to copy
	 * @throws RuntimeException
	*/
	public static <T, CC extends IDeepSettable<T>> CC getCopy(Class<CC> type, T toCopy) throws RuntimeException
	{
		DebugBox.predicateNotNull(type);

		if(toCopy == null)
		{
			return null;
		}

		return CVS_Pattern._getCopy(type, toCopy);
	}

	/**
	 * Create an instance of given type and call set
	 * <br/<Internal method: assumes type and toCopy aren't null !
	 * @param type Type of copy wanted - CC constructor must be public
	 * @param toCopy Values to copy
	 * @throws RuntimeException
	*/
	private static <T, CC extends IDeepSettable<T>> CC _getCopy(Class<CC> type, T toCopy) throws RuntimeException
	{
		CC	copy;

		try
		{
			copy = (CC)( type.newInstance() );
			copy.deepSet(toCopy);
		}
		catch(Throwable t)
		{
			throw new RuntimeException(t.getMessage());
		}

		return copy;
	}

}
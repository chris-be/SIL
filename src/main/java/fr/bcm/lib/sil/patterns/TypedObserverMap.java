/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.patterns;

import java.util.TreeMap;
import java.util.function.Consumer;

import fr.bcm.lib.sil.DebugBox;

/**
 * To manage observers by type
 */
public class TypedObserverMap {

	private TreeMap<String, ObserverSet<? extends IEvent>>	map;

	public TypedObserverMap()
	{
		this.map = new TreeMap<>();
	}

	/**
	 * Get observers
	 *
	 * @param type
	 * @param create Create if not exists
	 */
	@SuppressWarnings("unchecked")
	protected <TEvent extends IEvent> ObserverSet<TEvent> getObservers(String type, boolean create)
	{
		ObserverSet<? extends IEvent> observers = this.map.get(type);
		if(observers == null)
		{
			if(!create)
			{
				return null;
			}

			observers = new ObserverSet<>();
			this.map.put(type, observers);
		}

		return (ObserverSet<TEvent>)(observers);
	}

	protected <TEvent extends IEvent> void add(final String key, IObserver<TEvent> observer)
	{
		DebugBox.predicateNotNull(observer);
		ObserverSet<TEvent> observers = this.<TEvent>getObservers(key, true);
		observers.add(observer);
	}

	protected <TEvent extends IEvent> void remove(final String key, final IObserver<TEvent> observer)
	{
		DebugBox.predicateNotNull(observer);
		ObserverSet<TEvent> observers = this.<TEvent>getObservers(key, false);
		if(observers != null)
		{
			observers.remove(observer);
		}
	}

	/**
	 * Fire event
	 * 
	 */
	protected <TEvent extends IEvent> void fireEvent(final String key, final TEvent event, final Object source)
	{
		ObserverSet<TEvent> observers = this.<TEvent>getObservers(key, false);
		if(observers != null)
		{
			observers.fireEvent(event, source);
		}
	}

	/**
	 * Fire event
	 * 
	 * @param action What to do
	 */
	protected <TEvent extends IEvent> void fireEvent(final String key, final Consumer<IObserver<TEvent>> action)
	{
		ObserverSet<TEvent> observers = this.<TEvent>getObservers(key, false);
		if(observers != null)
		{
			observers.fireEvent(action);
		}
	}

//
//
//

	public <TEvent extends IEvent> void add(final Class<TEvent> type, IObserver<TEvent> observer)
	{
		DebugBox.predicateNotNull(type);
		String key = type.toString();
		this.add(key, observer);
	}

	public <TEvent extends IEvent> void remove(final Class<TEvent> type, final IObserver<TEvent> observer)
	{
		DebugBox.predicateNotNull(type);
		String key = type.toString();
		this.remove(key, observer);
	}

	public <TEvent extends IEvent> void fireEvent(final TEvent event, final Object source)
	{
		DebugBox.predicateNotNull(event);
		String key = event.getClass().toString();
		this.fireEvent(key, event, source);
	}

	public <TEvent extends IEvent> void fireEvent(final Class<TEvent> type, final Consumer<IObserver<TEvent>> action)
	{
		DebugBox.predicateNotNull(type);
		String key = type.toString();
		this.fireEvent(key, action);
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil;

import java.util.Iterator;
import java.util.StringTokenizer;

import fr.bcm.lib.sil.old2new.CastedEnumerationIterator;
import fr.bcm.lib.sil.old2new.StringSplitIterator;
import fr.bcm.lib.sil.patterns.IteratorWrapper;

/**
 * "Convenience" for Stream
 * 
 * @Convenience
 */
public class CVS_String {

	/**
	 * Return if string is null or empty
	 * @param str
	 * @return true if string is null or empty
	 */
	public static boolean isNullOrEmpty(String str)
	{
		return (str != null) ? str.isEmpty() : true;
	}

	/**
	 * Quote a string
	 *
	 * @return Quoted string
	 */
	public static String quote(String toQuote, char quote)
	{
		return CVS_String.quote(toQuote, quote, quote);
	}

	/**
	 * Quote a string
	 *
	 * @param toQuote
	 * @param prefix Prefix to add
	 * @param suffix Suffix to add
	 * @return Quoted string
	 */
	public static String quote(String toQuote, char prefix, char suffix)
	{
		return (toQuote == null) ? toQuote : prefix + toQuote + suffix;
	}


	/**
	 * Quote a string
	 *
	 * @param toQuote
	 * @param quote Prefix and Suffix to add
	 * @return Quoted string
	 */
	public static String quote(String toQuote, String quote)
	{
		return CVS_String.quote(toQuote, quote, quote);
	}

	/**
	 * Quote a string
	 *
	 * @param toQuote
	 * @param prefix Prefix to add
	 * @param suffix Suffix to add
	 * @return Quoted string
	 */
	public static String quote(String toQuote, String prefix, String suffix)
	{
		assert (prefix != null) : "Strange use";
		assert (suffix != null) : "Strange use";

		return (toQuote == null) ? toQuote : prefix + toQuote + suffix;
	}

	/**
	 * Split string (case sensitive)
	 * <br>Why? String.split / Pattern.split return String[]. Not very modern :|
	 * 
	 * @param toSplit
	 * @param delimiter Regex
	 * @return Tokens
	 */
	public static Iterable<String> split(String toSplit, String delimiter)
	{
		return CVS_String.split(toSplit, delimiter, false);
	}

	/**
	 * Split string
	 * <br>Why? String.split / Pattern.split return String[]. Not very modern :|
	 * 
	 * @param toSplit
	 * @param delimiter Regex
	 * @param caseInsensitive All expression
	 * @return Tokens
	 */
	public static Iterable<String> split(String toSplit, String delimiter, boolean caseInsensitive)
	{
		Iterator<String> iterator = new StringSplitIterator(toSplit, delimiter, caseInsensitive);
		return new IteratorWrapper<>(iterator);
	}

	/**
	 * Split string using character
	 * <br>Why? String.split / Pattern.split return String[]. Not very modern :|
	 * 
	 * @param toSplit
	 * @param delimiters @see StringTokenizer
	 * @return Tokens
	 */
	public static Iterable<String> splitWithCharacters(String toSplit, String delimiters)
	{
		StringTokenizer st = new StringTokenizer(toSplit, delimiters);
		CastedEnumerationIterator<String> iterator = new CastedEnumerationIterator<String>(st);

		return iterator.createIterable();
	}

	/**
	 * Create string repeating given pattern
	 * @param pattern
	 * @param repeat
	 */
	public static String create(String pattern, int repeat)
	{
		if(repeat <= 0)
		{
			return "";
		}

		StringBuilder sb = new StringBuilder(repeat * pattern.length());
		for(int i = repeat ; i > 0 ; --i)
		{
			sb.append(pattern);
		}

		return sb.toString();
	}

}
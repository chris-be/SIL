/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.fmt;

import java.util.Iterator;

import fr.bcm.lib.sil.CVS_String;
import fr.bcm.lib.sil.patterns.IteratorWrapper;

/**
 * @Convenience
 */
public class ThrowableFormatter {

	protected String		indentation;

	protected String		startLine;
	protected StringBuilder sb;
	
	public ThrowableFormatter()
	{
		this.indentation = "\t";
		this.sb = new StringBuilder();
	}

	protected void addLine(String line)
	{
		this.sb.append(startLine);
		this.sb.append(line);
		this.sb.append("\n");
	}

	public String format(Throwable throwable, String callerClass)
	{
		this.sb.setLength(0);
		this.startLine = this.indentation;
		this.addLine("Exception {");

		this.startLine = CVS_String.create(this.indentation, 2);
		String msg = throwable.getMessage();
		if(msg != null)
		{
			this.addLine( CVS_String.quote(msg, "'") );
		}
		this.addStackTrace(throwable, callerClass);
		this.tryAddCause(throwable, 0);

		this.startLine = this.indentation;
		this.addLine("}");

		return this.sb.toString();
	}

	/**
	 * Limit call stack - stop just after first callerClassName is found
	 * @param throwable 
	 * @param limitName
	 */
	protected void addStackTrace(Throwable throwable, String limitName)
	{
		Iterator<StackTraceElement> iter = new StackTraceIterator(throwable, limitName);
		for(StackTraceElement stel : new IteratorWrapper<>(iter))
		{
			this.addLine(stel.toString());
		}
	}

	/**
	 * 
	 * @param parent
	 * @param parentDepth
	 */
	protected void tryAddCause(Throwable parent, int parentDepth)
	{
		Throwable cause = parent.getCause();
		if(cause == null)
		{
			return;
		}
		int depth = parentDepth+1;
		String msg = "" + depth + " >";
		String dtl = cause.getMessage();
		if(dtl != null)
		{
			msg = msg + " " + dtl;
		}
		this.addLine(msg);

		StackTraceElement[] st = parent.getStackTrace();
		String limitLevel = (st.length == 0) ? null : st[0].getClassName();

		this.addStackTrace(cause, limitLevel);
		this.tryAddCause(cause, depth);
	}

}
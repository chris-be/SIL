/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.crt;

import java.io.Closeable;
import java.io.Flushable;

/**
 * Contract for writers
 */
public interface ILogWriter extends ILogPacketConsumer, Flushable, Closeable {

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.fmt;

import java.util.ArrayList;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.string.IStringCreator;
import fr.bcm.lib.sil.string.StringCreator;
import fr.bcm.lib.sil.string.ParserStringCreator;

/**
 * @Convenience
 */
public class LogFormatterBase<TOn> extends ParserStringCreator {

	protected String						format;

	protected ArrayList<IStringCreator>	generatorList;
	protected TOn	on;

	public LogFormatterBase()
	{
		super('%', '%', "%");
		this.generatorList = new ArrayList<>();
	}

	public void setFormat(String format)
	{
		DebugBox.predicateNotNull(format);
		this.format = format;
		this.generatorList = this.parse(format);
	}

	protected String generate()
	{
		return StringCreator.createWith(this.generatorList);
	}

	public String apply(TOn on)
	{
		this.on = on;
		return this.generate();
	}

}
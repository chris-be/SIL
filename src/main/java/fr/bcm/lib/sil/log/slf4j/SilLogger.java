/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.slf4j;

import java.util.logging.Level;

import org.slf4j.Marker;

import fr.bcm.lib.sil.log.Log;
import fr.bcm.lib.sil.log.LogBlock;

/**
 * To use SIL Log with slf4j
 */
public class SilLogger extends LoggerBase {

	protected static Level TRACE_LEVEL		= Level.FINEST;
	protected static Level DEBUG_LEVEL		= Level.CONFIG;
	protected static Level INFO_LEVEL		= Level.INFO;
	protected static Level WARN_LEVEL		= Level.WARNING;
	protected static Level ERROR_LEVEL		= Level.SEVERE;

	public SilLogger(final String channel)
	{
		super(channel);
	}

	@Override
	public boolean isTraceEnabled()
	{
		return Log.isOn(this.channel, SilLogger.TRACE_LEVEL);
	}

	@Override
	public boolean isTraceEnabled(Marker marker)
	{
		return Log.isOn(this.channel, SilLogger.TRACE_LEVEL);
	}

	@Override
	public boolean isDebugEnabled()
	{
		return Log.isOn(this.channel, SilLogger.DEBUG_LEVEL);
	}

	@Override
	public boolean isDebugEnabled(Marker marker)
	{
		return Log.isOn(this.channel, SilLogger.DEBUG_LEVEL);
	}

	@Override
	public boolean isInfoEnabled()
	{
		return Log.isOn(this.channel, SilLogger.INFO_LEVEL);
	}

	@Override
	public boolean isInfoEnabled(Marker marker)
	{
		return Log.isOn(this.channel, SilLogger.INFO_LEVEL);
	}

	@Override
	public boolean isWarnEnabled()
	{
		return Log.isOn(this.channel, SilLogger.WARN_LEVEL);
	}

	@Override
	public boolean isWarnEnabled(Marker marker)
	{
		return Log.isOn(this.channel, SilLogger.WARN_LEVEL);
	}

	@Override
	public boolean isErrorEnabled()
	{
		return Log.isOn(this.channel, SilLogger.ERROR_LEVEL);
	}

	@Override
	public boolean isErrorEnabled(Marker marker)
	{
		return Log.isOn(this.channel, SilLogger.ERROR_LEVEL);
	}

	@Override
	protected void _trace(String message, Throwable t)
	{
		LogBlock lb = new LogBlock(SilLogger.TRACE_LEVEL, message, t);
		Log.logThrough(this.channel, lb);
	}

	@Override
	protected void _trace(Marker marker, String message, Throwable t)
	{
		LogBlock lb = new LogBlock(SilLogger.TRACE_LEVEL, message, t);
		Log.logThrough(this.channel, lb);
	}

	@Override
	protected void _debug(String message, Throwable t)
	{
		LogBlock lb = new LogBlock(SilLogger.DEBUG_LEVEL, message, t);
		Log.logThrough(this.channel, lb);
	}

	@Override
	protected void _debug(Marker marker, String message, Throwable t)
	{
		LogBlock lb = new LogBlock(SilLogger.DEBUG_LEVEL, message, t);
		Log.logThrough(this.channel, lb);
	}

	@Override
	protected void _info(String message, Throwable t)
	{
		LogBlock lb = new LogBlock(SilLogger.INFO_LEVEL, message, t);
		Log.logThrough(this.channel, lb);
	}

	@Override
	protected void _info(Marker marker, String message, Throwable t)
	{
		LogBlock lb = new LogBlock(SilLogger.INFO_LEVEL, message, t);
		Log.logThrough(this.channel, lb);
	}

	@Override
	protected void _warn(String message, Throwable t)
	{
		LogBlock lb = new LogBlock(SilLogger.WARN_LEVEL, message, t);
		Log.logThrough(this.channel, lb);
	}

	@Override
	protected void _warn(Marker marker, String message, Throwable t)
	{
		LogBlock lb = new LogBlock(SilLogger.WARN_LEVEL, message, t);
		Log.logThrough(this.channel, lb);
	}

	@Override
	protected void _error(String message, Throwable t)
	{
		LogBlock lb = new LogBlock(SilLogger.ERROR_LEVEL, message, t);
		Log.logThrough(this.channel, lb);
	}

	@Override
	protected void _error(Marker marker, String message, Throwable t)
	{
		LogBlock lb = new LogBlock(SilLogger.ERROR_LEVEL, message, t);
		Log.logThrough(this.channel, lb);
	}

}
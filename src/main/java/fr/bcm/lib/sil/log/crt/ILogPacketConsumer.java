/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.crt;

import java.io.IOException;

/**
 * Contract for consuming log packets
 */
@FunctionalInterface
public interface ILogPacketConsumer {

	/**
	 * @param packet
	 */
	public void consume(LogPacket packet) throws IOException;

}
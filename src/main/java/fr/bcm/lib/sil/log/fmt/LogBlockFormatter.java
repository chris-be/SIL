/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.fmt;

import fr.bcm.lib.sil.log.LogBlock;
import fr.bcm.lib.sil.log.LogInfo;
import fr.bcm.lib.sil.string.IStringCreator;

/**
 * %level%, %timestamp%, %title%, %thread_id%, %caller%, %msgs%, %infos%, %exception%
 */
public class LogBlockFormatter extends LogFormatterBase<LogBlock> {

	protected ThrowableFormatter	throwableFormatter;

	protected LogInfoFormatter		logInfoFormatter;

	public void setInfoFormat(String format)
	{
		this.logInfoFormatter.setFormat(format);
		this.logInfoFormatter.setMessagePattern("\t - %s\n");
	}

	public LogBlockFormatter()
	{
		super();
		this.throwableFormatter = new ThrowableFormatter();
		this.logInfoFormatter = new LogInfoFormatter();

		this.setKeyword("level", new IStringCreator() {
			@Override
			public String createString()
			{
				return on.getLevel().getName();
			}
		}
		);

		this.setKeyword("timestamp", new IStringCreator() {
			@Override
			public String createString()
			{
				return on.getTimestamp().toString();
			}
		}
		);

		this.setKeyword("message", new IStringCreator() {
			@Override
			public String createString()
			{
				return on.getMessage();
			}
		}
		);

		this.setKeyword("thread_id", new IStringCreator() {
			@Override
			public String createString()
			{
				return Long.toString(on.getThreadId());
			}
		}
		);

		this.setKeyword("caller", new IStringCreator() {
			@Override
			public String createString()
			{
				return on.getCallerName();
			}
		}
		);

		this.setKeyword("exception", new IStringCreator() {
			@Override
			public String createString()
			{
				Throwable th = on.getThrowable();
				return (th == null) ? null : throwableFormatter.format(th, on.getCallerName());
			}
		}
		);

		this.setKeyword("infos", new IStringCreator() {
			@Override
			public String createString()
			{
				Iterable<LogInfo> lis = on.getInfos();
				if(lis == null)
				{
					return null;
				}

				StringBuilder sb = new StringBuilder();
				for(LogInfo li : lis)
				{
					String s = logInfoFormatter.apply(li);
					sb.append(s);
				}

				return sb.toString();
			}
		}
		);

	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.writer;

import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;

import fr.bcm.lib.sil.log.LogBlock;
import fr.bcm.lib.sil.log.crt.LogPacket;

/**
 * Write logs to console
 */
public class ConsoleLogWriter extends LogTextWriter {

	protected int stdErrLevel;

	/**
	 * Set level where System.err is used (instead of System.out) 
	 * @param level
	 */
	public void setStdErrLevel(Level level)
	{
		this.stdErrLevel = level.intValue();
	}

	/**
	 * Ctor
	 */
	public ConsoleLogWriter()
	{
		this.setStdErrLevel(Level.WARNING);
	}

	@Override
	protected void write(LogPacket logPacket) throws IOException
	{
		LogBlock logBlock = logPacket.getLogBlock();
		// Bad idea to close std...
		@SuppressWarnings("resource")
		PrintStream ps = (logBlock.getLevel().intValue() < this.stdErrLevel) ? System.out : System.err;

		String msg = this.createText(logPacket);
			String channel = logPacket.getChannel();
			ps.print("CHANNEL[" + channel + "] - ");
		ps.print(msg);
	}

	@Override
	public void flush() throws IOException
	{
		System.out.flush();
		System.err.flush();
	}

	@Override
	public void close() throws IOException
	{
		// Nothing to do
	}

}
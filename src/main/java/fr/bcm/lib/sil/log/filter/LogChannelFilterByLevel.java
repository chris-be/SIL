/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.filter;

import java.util.logging.Level;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.log.Log;
import fr.bcm.lib.sil.log.crt.ILogChannelFilter;

/**
 * Filter all channels of level {@code <= limit }
 */
public class LogChannelFilterByLevel implements ILogChannelFilter {

	/**
	 * Limit level
	 */
	protected int	limitLevel;

	public void setLimit(Level level)
	{
		DebugBox.predicateNotNull(level);
		this.limitLevel = level.intValue();
	}

	public LogChannelFilterByLevel()
	{
		this(Log.DEFAULT_FILTER_LEVEL);
	}

	public LogChannelFilterByLevel(Level level)
	{
		this.setLimit(level);
	}

	/**
	 * Get level to use for channel - here always limit
	 * @param channel
	 */
	protected int getLevel(String channel)
	{
		return this.limitLevel;
	}

	@Override
	public boolean accept(String channel, Level level)
	{
		int minLevel = this.getLevel(channel);

		return (level.intValue() >= minLevel);
	}

}
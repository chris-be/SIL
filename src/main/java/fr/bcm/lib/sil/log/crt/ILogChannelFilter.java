/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.crt;

import java.util.logging.Level;

/**
 * Contract for filtering
 */
@FunctionalInterface
public interface ILogChannelFilter {

	/**
	 * @param channel
	 * @param level
	 * @return True if log is enabled for channel and level
	 */
	public boolean accept(String channel, final Level level);

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log;

import java.util.ArrayList;

import fr.bcm.lib.sil.CVS_Collection;

/**
 * LogInfo: title, message(s)
 */
public class LogInfo {

	protected String title;
	protected ArrayList<String> messages;

	public String getTitle()
	{
		return this.title;
	}

	public Iterable<String> getMessages()
	{
		return this.messages;
	}

	public void add(String msg)
	{
		this.messages.add(msg);
	}

	public void addAll(String... msg)
	{
		CVS_Collection.addAll(this.messages, msg);
	}

	public LogInfo(String title)
	{
		this.title = title;
		this.messages = new ArrayList<>();
	}

	public LogInfo(String title, String message)
	{
		this(title);
		this.add(message);
	}

}
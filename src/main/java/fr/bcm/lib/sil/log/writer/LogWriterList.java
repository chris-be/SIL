/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.writer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import fr.bcm.lib.sil.CVS_Collection;
import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.log.crt.ILogWriter;
import fr.bcm.lib.sil.log.crt.LogPacket;

/**
 * List of writers
 */
public class LogWriterList implements ILogWriter {

	/**
	 * Writers
	 */
	protected ArrayList<ILogWriter>	writers;

	public LogWriterList()
	{
		this.writers = new ArrayList<>();
	}

	public void add(ILogWriter toAdd)
	{
		DebugBox.predicateNotNull(toAdd);
		this.writers.add(toAdd);
	}

	public void addAll(LogWriterList toAdd)
	{
		DebugBox.predicateNotNull(toAdd);
		CVS_Collection.addAll(this.writers, toAdd.writers);
	}

	public void remove(ILogWriter toRemove)
	{
		DebugBox.predicateNotNull(toRemove);
		Iterator<ILogWriter> iter = this.writers.iterator();
		while(iter.hasNext())
		{
			ILogWriter writer = iter.next();
			if(writer.equals(toRemove))
			{
				iter.remove();
			}
		}
	}

	/**
	 * Remove all writers by type
	 * @param writerClass
	 * @return Removed writers (to close them)
	 */
	public LogWriterList removeAll(Class<?> writerClass)
	{
		LogWriterList removedList = new LogWriterList();

		DebugBox.predicateNotNull(writerClass);
		Iterator<ILogWriter> iter = this.writers.iterator();
		while(iter.hasNext())
		{
			ILogWriter writer = iter.next();
			if(writer.getClass()== writerClass)
			{
				removedList.add(writer);
				iter.remove();
			}
		}

		return removedList;
	}

	public Iterable<ILogWriter> iterate()
	{
		return this.writers;
	}

	@Override
	public void consume(LogPacket packet) throws IOException
	{
		for(ILogWriter writer : this.iterate())
		{
			writer.consume(packet);
		}
	}

	@Override
	public void flush() throws IOException
	{
		for(ILogWriter toFlush : this.iterate())
		{
			toFlush.flush();
		}
	}

	/**
	 * Close and remove writers
	 */
	@Override
	public void close() throws IOException
	{
		Iterator<ILogWriter> iter = this.writers.iterator();
		while(iter.hasNext())
		{
			ILogWriter writer = iter.next();
			writer.close();
			iter.remove();
		}
	}

}
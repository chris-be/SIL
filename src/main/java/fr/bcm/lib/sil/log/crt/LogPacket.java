/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.crt;

import fr.bcm.lib.sil.log.LogBlock;

/**
 * LogBlock and destination channel
 */
public class LogPacket {

	protected final LogBlock logBlock;
	protected final String channel;

	public LogBlock getLogBlock()
	{
		return this.logBlock;
	}

	public String getChannel()
	{
		return this.channel;
	}

	public LogPacket(LogBlock logBlock, String channel)
	{
		this.logBlock = logBlock;
		this.channel = channel;
	}

}
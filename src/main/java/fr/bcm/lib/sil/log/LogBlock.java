/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Level;

import fr.bcm.lib.sil.reflection.CVS_Reflection;

/**
 * LogBlock: level, timestamp, caller class, log infos
 * 
 */
public class LogBlock {

	protected Level			level;
	protected LocalDateTime	timestamp;
	protected long			threadId;
	protected Class<?>		callerClass;

	protected String	message;
	protected Throwable	throwable;

	protected ArrayList<LogInfo> infos;

	public Level getLevel()
	{
		return this.level;
	}

	public LocalDateTime getTimestamp()
	{
		return this.timestamp;
	}

	public long getThreadId()
	{
		return this.threadId;
	}

	public Class<?> getCaller()
	{
		return this.callerClass;
	}

	public String getMessage()
	{
		return this.message;
	}

	public Throwable getThrowable()
	{
		return this.throwable;
	}

	public String getCallerName()
	{
		return (this.callerClass != null) ?this. callerClass.getName() : "'Unknown caller class'";
	}

	public Iterable<LogInfo> getInfos()
	{
		return this.infos;
	}

	public LogBlock(Level level, String message)
	{
		this(level, message, null, null);
	}

	public LogBlock(Level level, String message, Throwable throwable)
	{
		this(level, message, throwable, null);
	}

	public LogBlock(Level level, String message, Throwable throwable, Class<?> callerClass)
	{
		this.timestamp		= LocalDateTime.now();
		this.threadId		= Thread.currentThread().getId();
		this.level			= level;
		this.callerClass	= callerClass;

		this.message		= message;
		this.throwable		= throwable;
		this.infos			= null;
	}

	public void _add(LogInfo info)
	{
		if(this.infos == null)
		{	// Create only if additional infos
			this.infos = new ArrayList<>();
		}
		this.infos.add(info);
	}

	public void add(LogInfo info)
	{
		this._add(info);
	}

	public LogInfo addInfo(String title)
	{
		LogInfo li = new LogInfo(title);
		this._add(li);
		return li;
	}

//
//
//

	public static LogBlock createWithCaller(Level level, String message)
	{
		return new LogBlock(level, message, null, CVS_Reflection.getCallerClass());
	}

	public static LogBlock createWithCaller(Level level, String message, Exception exception)
	{
		return new LogBlock(level, message, exception, CVS_Reflection.getCallerClass());
	}

}
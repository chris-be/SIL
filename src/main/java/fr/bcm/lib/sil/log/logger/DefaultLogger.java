/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.logger;

import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;

import fr.bcm.lib.sil.log.crt.ILogChannelFilter;
import fr.bcm.lib.sil.log.crt.ILogWriter;
import fr.bcm.lib.sil.log.crt.LogPacket;
import fr.bcm.lib.sil.log.filter.LogChannelFilterByLevel;
import fr.bcm.lib.sil.log.writer.LogWriterList;
import fr.bcm.lib.sil.log.writer.LogWriterList_TS;

/**
 * Default logger: use a thread to handle logs in background
 */
public class DefaultLogger extends LoggerBase {

	/**
	 * Thread safe queue
	 */
	protected ConcurrentLinkedQueue<LogPacket>	queue;

	protected ThreadLogWriter logThread;

	/**
	 * Writers to forward packet
	 */
	protected LogWriterList_TS		writers;

	/**
	 * Ctor
	 */
	public DefaultLogger()
	{
		this(new LogChannelFilterByLevel());
	}

	/**
	 * Ctor
	 * @param frontFilter
	 */
	public DefaultLogger(ILogChannelFilter frontFilter)
	{
		super(frontFilter);
		this.queue = new ConcurrentLinkedQueue<>();
		this.writers = new LogWriterList_TS();

		this.logThread = new ThreadLogWriter(this);
		this.logThread.start();
	}

	/**
	 * Add a writer
	 * @param toAdd
	 */
	public void addWriter(ILogWriter toAdd)
	{
		this.writers.add(toAdd);
	}

	/**
	 * Remove a writer
	 * @param toRemove
	 */
	public void removeWriter(ILogWriter toRemove)
	{
		this.writers.remove(toRemove);
	}

	/**
	 * Remove multiple writers
	 * @param writerClass
	 * @return removed writers
	 */
	public LogWriterList removeWriters(Class<?> writerClass)
	{
		return this.writers.removeWriters(writerClass);
	}

	@Override
	public void consume(final LogPacket packet) throws IOException
	{
		this.queue.add(packet);
	}

	@Override
	public void close() throws IOException
	{
		this.logThread.interrupt();

		this.flush();
		this.writers.close();
	}

	@Override
	public void flush() throws IOException
	{
		this.flushQueue();
		// this.writers.flush();
	}

	/**
	 * Flush queue
	 * @throws IOException
	 */
	public void flushQueue() throws IOException
	{
		LogPacket lp;
		while((lp = this.queue.poll()) != null)
		{
			this.writers.consume(lp);
		}
	}

}
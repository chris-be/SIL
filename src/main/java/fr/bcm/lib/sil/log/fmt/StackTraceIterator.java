/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.fmt;

import fr.bcm.lib.sil.CVS_String;
import fr.bcm.lib.sil.old2new.ArrayIterator;

/**
 * @Convenience
 */
public class StackTraceIterator extends ArrayIterator<StackTraceElement> {

	protected String	limitClassName;

	public StackTraceIterator(Throwable throwable, String className)
	{
		this(throwable.getStackTrace(), className);
	}

	public StackTraceIterator(StackTraceElement[] stack, String className)
	{
		super(stack);
		this.limitClassName = CVS_String.isNullOrEmpty(className) ? null : className;
	}

	@Override
	public StackTraceElement next()
	{
		StackTraceElement stel = super.next();

		if(this.limitClassName != null)
		{
			String tname = stel.getClassName();
			if(tname.compareTo(this.limitClassName) == 0)
			{	// Stop at the limit level
				super.toEnd();
			}
		}

		return stel;
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.writer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.file.SubFileLocator;
import fr.bcm.lib.sil.log.crt.LogPacket;

/**
 * Write logs to files
 */
public class LogFileWriter extends LogTextWriter {

	protected TreeMap<String, OutputStream>	outputs;

	protected SubFileLocator fileLocator;

	public LogFileWriter()
	{
		this(new File("./logs"));
	}

	public LogFileWriter(File root)
	{
		DebugBox.predicateNotNull(root);
		this.fileLocator = new SubFileLocator(root);
		this.outputs = new TreeMap<>();

		File file = this.fileLocator.getRoot();
		if(!file.exists())
		{	// Create empty file if needed
			file.mkdirs();
		}
	}

	protected String createFileName(LogPacket logPacket)
	{
		String channel = logPacket.getChannel();
		String fileName = channel.replace('.', '_');

		/*
		fileName = fileName + "T-" + logPacket.getThreadId().toString();
		 */

		return fileName + ".txt-log";
	}

	@Override
	protected void write(LogPacket logPacket) throws IOException
	{
		String fileName = this.createFileName(logPacket);

		OutputStream os = this.outputs.get(fileName);
		if(os == null)
		{
			File file = this.fileLocator.cptFileLocation(fileName);
			os = new FileOutputStream(file, true);
			this.outputs.put(fileName, os);
		}

		byte[] buf = this.createTextBuffer(logPacket);
		os.write(buf);
	}

	@Override
	public void flush() throws IOException
	{
		for(OutputStream os : this.outputs.values())
		{
			os.flush();
		}
	}

	@Override
	public void close() throws IOException
	{
		Iterator<Entry<String, OutputStream>> iter = this.outputs.entrySet().iterator();
		while(iter.hasNext())
		{
			Entry<String, OutputStream> entry = iter.next();
			entry.getValue().close();
			iter.remove();
		}
	}

}
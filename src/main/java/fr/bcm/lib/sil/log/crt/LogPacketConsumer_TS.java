/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.crt;

import java.io.IOException;

import fr.bcm.lib.sil.DebugBox;

/**
 * Threadsafe ILogPacketConsumer wrapper
 */
public class LogPacketConsumer_TS implements ILogPacketConsumer {

	protected ILogPacketConsumer wrapped;

	public LogPacketConsumer_TS(ILogPacketConsumer lc)
	{
		DebugBox.predicateNotNull(lc);
		this.wrapped = lc;
	}

	@Override
	public void consume(LogPacket packet) throws IOException
	{
		synchronized(this.wrapped)
		{
			this.wrapped.consume(packet);
		}
	}

}
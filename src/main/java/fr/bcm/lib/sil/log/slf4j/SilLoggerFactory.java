/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.slf4j;

import java.util.Map;
import java.util.TreeMap;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;

/**
 * To use SIL Log with slf4j
 */
public class SilLoggerFactory implements ILoggerFactory {

	private static Map<String, Logger> cache;

	static {
		cache = new TreeMap<>();
	}

	@Override
	public Logger getLogger(String name)
	{
		Logger logger;
		synchronized (cache)
		{
			logger = cache.get(name);
			if(logger == null)
			{
				logger = new SilLogger(name);
			}
			cache.put(name, logger);
		}

		return logger;
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.writer;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.log.crt.ILogWriter;
import fr.bcm.lib.sil.log.crt.LogPacket;

/**
 * List of writers "thread safe"
 */
public class LogWriterList_TS implements ILogWriter {

	/**
	 * Writers
	 */
	protected AtomicReference<LogWriterList>	writers;

	public LogWriterList_TS()
	{
		LogWriterList wl = new LogWriterList();
		this.writers = new AtomicReference<>(wl);
	}

	public void add(ILogWriter toAdd)
	{
		DebugBox.predicateNotNull(toAdd);
		LogWriterList newList = new LogWriterList();
		newList.add(toAdd);
		newList.addAll(this.writers.get());

		this.writers.set(newList);
	}

	public void remove(ILogWriter toRemove)
	{
		DebugBox.predicateNotNull(toRemove);
		LogWriterList newList = new LogWriterList();

		for(ILogWriter writer : this.iterate())
		{
			if(!writer.equals(toRemove))
			{
				newList.add(writer);
			}
		}

		this.writers.set(newList);
	}

	/**
	 * Remove all writers by type
	 * @param writerClass
	 * @return Removed writers (to close them)
	 */
	public LogWriterList removeWriters(Class<?> writerClass)
	{
		DebugBox.predicateNotNull(writerClass);
		LogWriterList newList = new LogWriterList();
		LogWriterList removedList = new LogWriterList();

		for(ILogWriter writer : this.iterate())
		{
			if(writer.getClass() == writerClass)
			{
				removedList.add(writer);
			}
			else
			{
				newList.add(writer);
			}
		}

		this.writers.set(newList);
		return removedList;
	}

	public Iterable<ILogWriter> iterate()
	{
		return this.writers.get().iterate();
	}

	@Override
	public void consume(LogPacket packet) throws IOException
	{
		this.writers.get().consume(packet);
	}

	@Override
	public void flush() throws IOException
	{
		this.writers.get().flush();
	}

	/**
	 * Close and remove writers
	 */
	@Override
	public void close() throws IOException
	{
		this.writers.get().close();
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.logger;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.log.Log;
import fr.bcm.lib.sil.log.crt.ILogWriter;
import fr.bcm.lib.sil.thread.PeriodicThreadBase;

/**
 * Thread using a LogWriter
 */
public class ThreadLogWriter extends PeriodicThreadBase {

	protected final ILogWriter	logWriter;

	public ThreadLogWriter(ILogWriter logWriter)
	{
		this(logWriter, Log.DEFAULT_QUEUE_PERIOD);
	}

	public ThreadLogWriter(ILogWriter logWriter, int sleepDelayMs)
	{
		super(sleepDelayMs);
		DebugBox.predicateNotNull(logWriter);
		this.logWriter = logWriter;
	}

	@Override
	public void run()
	{
		try
		{
			this.timer.start();

			boolean goOn = true;
			while(goOn)
			{
				this.logWriter.flush();
				this.timer.sleepIfNeeded();
			}
		}
		catch(InterruptedException ie)
		{	// Ignore
		}
		catch(Exception ex)
		{
			this.unhandledException(ex);
		}
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.fmt;

import fr.bcm.lib.sil.log.LogBlock;
import fr.bcm.lib.sil.log.crt.ILogTextFormatter;

/**
 * TextFormatter for logs
 */
public class LogTextFormatter implements ILogTextFormatter {

	public static final String DEFAULT_BLOCK_FORMAT = "%level%\t%timestamp% T-%thread_id% {\n\t'%message%'\n%exception%%infos%}\n";
	public static final String DEFAULT_INFO_FORMAT	= "\t[%title%] {\n%msgs%\t}\n";

	LogBlockFormatter		formatter;

	public LogTextFormatter()
	{
		this(DEFAULT_BLOCK_FORMAT, DEFAULT_INFO_FORMAT);
	}

	public LogTextFormatter(String blockFormat, String infoFormat)
	{
		this.formatter = new LogBlockFormatter();
		this.formatter.setFormat(blockFormat);
		this.formatter.setInfoFormat(infoFormat);
	}

	@Override
	public String format(LogBlock logBlock)
	{
		return this.formatter.apply(logBlock);
	}

}
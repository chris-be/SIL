/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.writer;

import java.nio.charset.Charset;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.log.LogBlock;
import fr.bcm.lib.sil.log.crt.ILogPacketFilter;
import fr.bcm.lib.sil.log.crt.ILogTextFormatter;
import fr.bcm.lib.sil.log.crt.LogPacket;
import fr.bcm.lib.sil.log.filter.LogPacketFilterKeepAll;
import fr.bcm.lib.sil.log.fmt.LogTextFormatter;

/**
 * LogTextWriter base class
 * @Convenience
 */
public abstract class LogTextWriter extends LogWriterBase {

	protected ILogTextFormatter	formatter;
	protected Charset	charset;

	public void setTextFormatter(ILogTextFormatter formatter)
	{
		DebugBox.predicateNotNull(formatter);
		this.formatter = formatter;
	}

	public void setCharset(Charset charset)
	{
		DebugBox.predicateNotNull(charset);
		this.charset = charset;
	}

	public LogTextWriter()
	{
		this(new LogPacketFilterKeepAll(), new LogTextFormatter());
	}

	public LogTextWriter(ILogPacketFilter packetFilter, ILogTextFormatter formatter)
	{
		this.charset = Charset.forName("UTF-8");
		this.setPacketFilter(packetFilter);
		this.setTextFormatter(formatter);
	}

	protected String createText(LogPacket logPacket)
	{
		LogBlock logBlock = logPacket.getLogBlock();
		return this.formatter.format(logBlock);
	}

	protected byte[] createTextBuffer(LogPacket logPacket)
	{
		String msg = this.createText(logPacket);
		return msg.getBytes(this.charset);
	}

}
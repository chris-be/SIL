/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log;

import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;

import fr.bcm.lib.sil.log.crt.ILogger;
import fr.bcm.lib.sil.log.crt.LogPacket;
import fr.bcm.lib.sil.log.logger.DefaultLogger;
import fr.bcm.lib.sil.log.writer.ConsoleLogWriter;

/**
 * Entry point for logging
 */
public class Log {

	/**
	 *  Default Level used by filters
	 */
	public static Level	DEFAULT_FILTER_LEVEL = Level.WARNING;
	/**
	 * Default delay for LogThread to flush queue (ms)
	 */
	public static int	DEFAULT_QUEUE_PERIOD = 1000;

	// Service
	private static ILogger	logger;

	// Ensure minimal service (and object to lock)
	static {
		Log.logger = Log.createDefaultLogger();
	}

	// Static entries only
	private Log()
	{	}

	/**
	 * Change log router
	 * @param logger
	 */
	public static void setLogger(ILogger logger)
	{
		if(logger == null)
		{	// Fallback to default
			logger = Log.createDefaultLogger();
		}

		synchronized(Log.logger)
		{
			if(Log.logger != null)
			{
				try
				{
					Log.logger.close();
				}
				catch (IOException e)
				{	// Ignore error
					Log.printError(e);
				}
				Log.logger = null;
			}

			Log.logger = logger;
		}
	}

	/**
	 * Default logger: console
	 */
	public static ILogger createDefaultLogger()
	{
		DefaultLogger router = new DefaultLogger();
		router.addWriter(new ConsoleLogWriter());

		return router;
	}

	public static boolean isOn(String channel, final Level level)
	{
		synchronized(Log.logger)
		{
			return Log.logger.accept(channel, level);
		}
	}

	public static void log(String channel, LogBlock logBlock)
	{
		try
		{
			synchronized(Log.logger)
			{
				Level level = logBlock.level;
				if(!Log.logger.accept(channel, level))
				{
					return;
				}

				LogPacket lp = new LogPacket(logBlock, channel);
				Log.logger.consume(lp);
			}
		}
		catch(Exception ex)
		{	// Fallback
			Log.printError(ex);
		}
	}

	/**
	 * Log without taking channel configuration into account
	 * @param channel
	 * @param logBlock
	 */
	public static void logThrough(String channel, LogBlock logBlock)
	{
		try
		{
			LogPacket lp = new LogPacket(logBlock, channel);
			synchronized(Log.logger)
			{
				Log.logger.consume(lp);
			}
		}
		catch(Exception ex)
		{	// Fallback
			Log.printError(ex);
		}
	}

//
// Convenience
//

	/**
	 * Print message and cause stack trace
	 * @param ps Stream destination
	 */
	public static void printError(Exception e, PrintStream ps)
	{
		e.printStackTrace(ps);
	}

	/**
	 * Print message and cause stack trace to System.err
	 */
	public static void printError(Exception e)
	{
		Log.printError(e, System.err);
	}

	public static void log(String channel, Level level, String message, Throwable th)
	{
		if(Log.isOn(channel, level))
		{
			LogBlock lb = new LogBlock(level, message, th);
			Log.logThrough(channel, lb);
		}
	}

	public static void info(String channel, String message)
	{
		Log.log(channel, Level.INFO, message, null);
	}

	public static void warn(String channel, String message)
	{
		Log.log(channel, Level.WARNING, message, null);
	}

	public static void warn(String channel, String message, Throwable th)
	{
		Log.log(channel, Level.WARNING, message, th);
	}

	public static void error(String channel, String message)
	{
		Log.log(channel, Level.SEVERE, message, null);
	}

	public static void error(String channel, String message, Throwable th)
	{
		Log.log(channel, Level.SEVERE, message, th);
	}

//
// Class
//

	public static String makeChannel(Class<?> cl)
	{
		return cl.getName();
	}

	public static void log(Class<?> cl, LogBlock logBlock)
	{
		String channel = Log.makeChannel(cl);
		Log.log(channel, logBlock);
	}

	public static void info(Class<?> cl, String message)
	{
		Log.info(cl.getName(), message);
	}

	public static void warn(Class<?> cl, String message)
	{
		String channel = Log.makeChannel(cl);
		Log.warn(channel, message);
	}

	public static void warn(Class<?> cl, String message, Throwable th)
	{
		String channel = Log.makeChannel(cl);
		Log.warn(channel, message, th);
	}

	public static void error(Class<?> cl, String message)
	{
		String channel = Log.makeChannel(cl);
		Log.error(channel, message);
	}

	public static void error(Class<?> cl, String message, Throwable th)
	{
		String channel = Log.makeChannel(cl);
		Log.error(channel, message, th);
	}

}
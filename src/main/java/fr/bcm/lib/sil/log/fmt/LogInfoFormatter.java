/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.fmt;

import fr.bcm.lib.sil.log.LogInfo;
import fr.bcm.lib.sil.string.IStringCreator;

/**
 * %title%, %msgs%
 */
public class LogInfoFormatter extends LogFormatterBase<LogInfo> {

	protected String messagePattern;

	public void setMessagePattern(String pattern)
	{
		this.messagePattern = pattern;
	}

	public LogInfoFormatter()
	{
		this.setMessagePattern(" - %s\n");

		this.setKeyword("title", new IStringCreator() {
			@Override
			public String createString()
			{
				return on.getTitle();
			}
		}
		);

		this.setKeyword("msgs", new IStringCreator() {
			@Override
			public String createString()
			{
				StringBuilder sb = new StringBuilder();
				for(String msg : on.getMessages())
				{
					String s = String.format(messagePattern, msg);
					sb.append(s);
				}

				return sb.toString();
			}
		}
		);
	}

}
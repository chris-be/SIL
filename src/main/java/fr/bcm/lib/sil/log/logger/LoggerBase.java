/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.logger;

import java.io.IOException;
import java.util.logging.Level;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.log.crt.ILogChannelFilter;
import fr.bcm.lib.sil.log.crt.ILogger;
import fr.bcm.lib.sil.log.crt.LogPacket;

/**
 * Base class for logger using channel filter
 * @Convenience
 */
public abstract class LoggerBase implements ILogger {

	/**
	 * Filter used
	 */
	protected ILogChannelFilter	frontFilter;

	public void setFrontFilter(ILogChannelFilter frontFilter)
	{
		DebugBox.predicateNotNull(frontFilter);
		this.frontFilter = frontFilter;
	}

	public LoggerBase()
	{	}

	public LoggerBase(ILogChannelFilter frontFilter)
	{
		this.setFrontFilter(frontFilter);
	}

	@Override
	public boolean accept(String channel, Level level)
	{
		return this.frontFilter.accept(channel, level);
	}

	@Override
	public abstract void consume(final LogPacket packet) throws IOException;

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.slf4j;

import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

/**
 * @Convenience - DRY
 */
public abstract class LoggerBase implements Logger {

	protected final String channel;

	public String getChannel()
	{
		return this.channel;
	}

	public LoggerBase(final String channel)
	{
		this.channel = channel;
	}

	protected abstract void _trace(String message, Throwable t);
	protected abstract void _trace(Marker marker, String message, Throwable t);

	protected abstract void _debug(String message, Throwable t);
	protected abstract void _debug(Marker marker, String message, Throwable t);

	protected abstract void _info(String formessagemat, Throwable t);
	protected abstract void _info(Marker marker, String message, Throwable t);

	protected abstract void _warn(String message, Throwable t);
	protected abstract void _warn(Marker marker, String message, Throwable t);

	protected abstract void _error(String message, Throwable t);
	protected abstract void _error(Marker marker, String message, Throwable t);

	/**
	 * Use of MessageFormatter
	 */
	protected String format(String format, Object arg)
	{
		FormattingTuple tuple = MessageFormatter.format(format, arg);
		return tuple.getMessage();
	}

	/**
	 * Use of MessageFormatter
	 */
	protected String format(String format, Object... args)
	{
		FormattingTuple tuple = MessageFormatter.arrayFormat(format, args);
		return tuple.getMessage();
	}

	@Override
	public String getName()
	{
		return this.getClass().getName();
	}

	@Override
	public void trace(String msg)
	{
		if(this.isTraceEnabled())
		{
			this._trace(msg, null);
		}
	}

	@Override
	public void trace(String format, Object arg)
	{
		if(this.isTraceEnabled())
		{
			String msg = this.format(format, arg);
			this._trace(msg, null);
		}
	}

	@Override
	public void trace(String format, Object arg1, Object arg2)
	{
		if(this.isTraceEnabled())
		{
			String msg = this.format(format, arg1, arg2);
			this._trace(msg, null);
		}
	}

	@Override
	public void trace(String format, Object... arguments)
	{
		if(this.isTraceEnabled())
		{
			String msg = this.format(format, arguments);
			this._trace(msg, null);
		}
	}

	@Override
	public void trace(String msg, Throwable t)
	{
		if(this.isTraceEnabled())
		{
			this._trace(msg, t);
		}
	}

	@Override
	public void trace(Marker marker, String msg)
	{
		if(this.isTraceEnabled(marker))
		{
			this._trace(marker, msg, null);
		}
	}

	@Override
	public void trace(Marker marker, String format, Object arg)
	{
		if(this.isTraceEnabled(marker))
		{
			String msg = this.format(format, arg);
			this._trace(marker, msg, null);
		}
	}

	@Override
	public void trace(Marker marker, String format, Object arg1, Object arg2)
	{
		if(this.isTraceEnabled(marker))
		{
			String msg = this.format(format, arg1, arg2);
			this._trace(marker, msg, null);
		}
	}

	@Override
	public void trace(Marker marker, String format, Object... argArray)
	{
		if(this.isTraceEnabled(marker))
		{
			String msg = this.format(format, argArray);
			this._trace(marker, msg, null);
		}
	}

	@Override
	public void trace(Marker marker, String msg, Throwable t)
	{
		if(this.isTraceEnabled(marker))
		{
			this._trace(marker, msg, t);
		}
	}

	
	@Override
	public void debug(String msg)
	{
		if(this.isDebugEnabled())
		{
			this._debug(msg, null);
		}
	}

	@Override
	public void debug(String format, Object arg)
	{
		if(this.isDebugEnabled())
		{
			String msg = this.format(format, arg);
			this._debug(msg, null);
		}
	}

	@Override
	public void debug(String format, Object arg1, Object arg2)
	{
		if(this.isDebugEnabled())
		{
			String msg = this.format(format, arg1, arg2);
			this._debug(msg, null);
		}
	}

	@Override
	public void debug(String format, Object... arguments)
	{
		if(this.isDebugEnabled())
		{
			String msg = this.format(format, arguments);
			this._debug(msg, null);
		}
	}

	@Override
	public void debug(String msg, Throwable t)
	{
		if(this.isDebugEnabled())
		{
			this._debug(msg, t);
		}
	}

	@Override
	public void debug(Marker marker, String msg)
	{
		if(this.isDebugEnabled(marker))
		{
			this._debug(marker, msg, null);
		}
	}

	@Override
	public void debug(Marker marker, String format, Object arg)
	{
		if(this.isDebugEnabled(marker))
		{
			String msg = this.format(format, arg);
			this._debug(marker, msg, null);
		}
	}

	@Override
	public void debug(Marker marker, String format, Object arg1, Object arg2)
	{
		if(this.isDebugEnabled(marker))
		{
			String msg = this.format(format, arg1, arg2);
			this._debug(marker, msg, null);
		}
	}

	@Override
	public void debug(Marker marker, String format, Object... arguments)
	{
		if(this.isDebugEnabled(marker))
		{
			String msg = this.format(format, arguments);
			this._debug(marker, msg, null);
		}
	}

	@Override
	public void debug(Marker marker, String msg, Throwable t)
	{
		if(this.isDebugEnabled(marker))
		{
			this._debug(marker, msg, t);
		}
	}

	@Override
	public void info(String msg)
	{
		if(this.isInfoEnabled())
		{
			this._info(msg, null);
		}
	}

	@Override
	public void info(String format, Object arg)
	{
		if(this.isInfoEnabled())
		{
			String msg = this.format(format, arg);
			this._info(msg, null);
		}
	}

	@Override
	public void info(String format, Object arg1, Object arg2)
	{
		if(this.isInfoEnabled())
		{
			String msg = this.format(format, arg1, arg2);
			this._info(msg, null);
		}
	}

	@Override
	public void info(String format, Object... arguments)
	{
		if(this.isInfoEnabled())
		{
			String msg = this.format(format, arguments);
			this._info(msg, null);
		}
	}

	@Override
	public void info(String msg, Throwable t)
	{
		if(this.isInfoEnabled())
		{
			this._info(msg, t);
		}
	}

	@Override
	public void info(Marker marker, String msg)
	{
		if(this.isInfoEnabled(marker))
		{
			this._info(marker, msg, null);
		}
	}

	@Override
	public void info(Marker marker, String format, Object arg)
	{
		if(this.isInfoEnabled(marker))
		{
			String msg = this.format(format, arg);
			this._info(marker, msg, null);
		}
	}

	@Override
	public void info(Marker marker, String format, Object arg1, Object arg2)
	{
		if(this.isInfoEnabled(marker))
		{
			String msg = this.format(format, arg1, arg2);
			this._info(marker, msg, null);
		}
	}

	@Override
	public void info(Marker marker, String format, Object... arguments)
	{
		if(this.isInfoEnabled(marker))
		{
			String msg = this.format(format, arguments);
			this._info(marker, msg, null);
		}
	}

	@Override
	public void info(Marker marker, String msg, Throwable t)
	{
		if(this.isInfoEnabled(marker))
		{
			this._info(marker, msg, t);
		}
	}

	@Override
	public void warn(String msg)
	{
		if(this.isWarnEnabled())
		{
			this._warn(msg, null);
		}
	}

	@Override
	public void warn(String format, Object arg)
	{
		if(this.isWarnEnabled())
		{
			String msg = this.format(format, arg);
			this._warn(msg, null);
		}
	}

	@Override
	public void warn(String format, Object arg1, Object arg2)
	{
		if(this.isWarnEnabled())
		{
			String msg = this.format(format, arg1, arg2);
			this._warn(msg, null);
		}
	}

	@Override
	public void warn(String format, Object... arguments)
	{
		if(this.isWarnEnabled())
		{
			String msg = this.format(format, arguments);
			this._warn(msg, null);
		}
	}

	@Override
	public void warn(String msg, Throwable t)
	{
		if(this.isWarnEnabled())
		{
			this._warn(msg, t);
		}
	}

	@Override
	public void warn(Marker marker, String msg)
	{
		if(this.isWarnEnabled(marker))
		{
			this._warn(marker, msg, null);
		}
	}

	@Override
	public void warn(Marker marker, String format, Object arg)
	{
		if(this.isWarnEnabled(marker))
		{
			String msg = this.format(format, arg);
			this._warn(marker, msg, null);
		}
	}

	@Override
	public void warn(Marker marker, String format, Object arg1, Object arg2)
	{
		if(this.isWarnEnabled(marker))
		{
			String msg = this.format(format, arg1, arg2);
			this._warn(marker, msg, null);
		}
	}

	@Override
	public void warn(Marker marker, String format, Object... arguments)
	{
		if(this.isWarnEnabled(marker))
		{
			String msg = this.format(format, arguments);
			this._warn(marker, msg, null);
		}
	}

	@Override
	public void warn(Marker marker, String msg, Throwable t)
	{
		if(this.isWarnEnabled(marker))
		{
			this._warn(marker, msg, t);
		}
	}

	@Override
	public void error(String msg)
	{
		if(this.isErrorEnabled())
		{
			this._error(msg, null);
		}
	}

	@Override
	public void error(String format, Object arg)
	{
		if(this.isErrorEnabled())
		{
			String msg = this.format(format, arg);
			this._error(msg, null);
		}
	}

	@Override
	public void error(String format, Object arg1, Object arg2)
	{
		if(this.isErrorEnabled())
		{
			String msg = this.format(format, arg1, arg2);
			this._error(msg, null);
		}
	}

	@Override
	public void error(String format, Object... arguments)
	{
		if(this.isErrorEnabled())
		{
			String msg = this.format(format, arguments);
			this._error(msg, null);
		}
	}

	@Override
	public void error(String msg, Throwable t)
	{
		if(this.isErrorEnabled())
		{
			this._error(msg, t);
		}
	}

	@Override
	public void error(Marker marker, String msg)
	{
		if(this.isErrorEnabled(marker))
		{
			this._error(marker, msg, null);
		}
	}

	@Override
	public void error(Marker marker, String format, Object arg)
	{
		if(this.isErrorEnabled(marker))
		{
			String msg = this.format(format, arg);
			this._error(marker, msg, null);
		}
	}

	@Override
	public void error(Marker marker, String format, Object arg1, Object arg2)
	{
		if(this.isErrorEnabled(marker))
		{
			String msg = this.format(format, arg1, arg2);
			this._error(marker, msg, null);
		}
	}

	@Override
	public void error(Marker marker, String format, Object... arguments)
	{
		if(this.isErrorEnabled(marker))
		{
			String msg = this.format(format, arguments);
			this._error(marker, msg, null);
		}
	}

	@Override
	public void error(Marker marker, String msg, Throwable t)
	{
		if(this.isErrorEnabled(marker))
		{
			this._error(marker, msg, t);
		}
	}

}
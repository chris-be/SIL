/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.crt;

/**
 * Contract for logger - filter and writer
 */
public interface ILogger extends ILogChannelFilter, ILogWriter {

}
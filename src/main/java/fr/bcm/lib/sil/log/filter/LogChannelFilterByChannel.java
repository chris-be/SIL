/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.filter;

import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;

import fr.bcm.lib.sil.DebugBox;

/**
 * Filter depending on channel configuration.
 */
public class LogChannelFilterByChannel extends LogChannelFilterByLevel {

	public static String PACKAGE_SIGNATURE = ".*";

	/**
	 * Channel levels
	 */
	protected TreeMap<String, Level>	channelLevels;

	/**
	 * Package levels
	 */
	protected TreeMap<String, Level>	packageLevels;

	public LogChannelFilterByChannel()
	{
		super();
		this.channelLevels = new TreeMap<>();
		this.packageLevels = new TreeMap<>();
	}

	protected String createPackageEntry(String channel)
	{
		// Strip only *
		int i = channel.length() - 1;
		return channel.substring(0, i);
	}

	public void setChannel(String channel, Level level)
	{
		DebugBox.predicate(!channel.endsWith("."));
		this.channelLevels.put(channel, level);
	}

	public void unsetChannel(String channel)
	{
		DebugBox.predicate(!channel.endsWith("."));
		this.channelLevels.remove(channel);
	}

	public void setPackage(String channel, Level level)
	{
		DebugBox.predicate(channel.endsWith("."));
		this.packageLevels.put(channel, level);
	}

	public void unsetPackage(String channel)
	{
		DebugBox.predicate(channel.endsWith("."));
		this.packageLevels.remove(channel);
	}

	/**
	 * Distinct channel from package if ends with PACKAGE_SIGNATURE
	 * @param channel
	 * @param level
	 */
	public void setLevel(String channel, Level level)
	{
		if(channel.endsWith(PACKAGE_SIGNATURE))
		{
			channel = this.createPackageEntry(channel);
			this.setPackage(channel, level);
		}
		else
		{
			this.setChannel(channel, level);
		}
	}

	/**
	 * Distinct channel from package if ends with PACKAGE_SIGNATURE
	 * @param channel
	 */
	public void unset(String channel)
	{
		if(channel.endsWith(PACKAGE_SIGNATURE))
		{
			channel = this.createPackageEntry(channel);
			this.unsetPackage(channel);
		}
		else
		{
			this.unsetChannel(channel);
		}
	}

	@Override
	protected int getLevel(String channel)
	{
		// Test channel level
		Level channelLevel = this.channelLevels.get(channel);
		if(channelLevel != null)
		{
			return channelLevel.intValue();
		}

		// Test package level
		Entry<String, Level> entry = this.packageLevels.lowerEntry(channel);
		if(entry != null)
		{
			if(channel.startsWith(entry.getKey()))
			{
				return entry.getValue().intValue();
			}
		}

		// Default level
		return this.limitLevel;
	}

}
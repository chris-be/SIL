/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.crt;

import fr.bcm.lib.sil.log.LogBlock;

/**
 * Contract to create string representation for LogInfo
 */
public interface ILogTextFormatter {

	public String format(LogBlock logBlock);

}
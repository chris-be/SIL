/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.log.writer;

import java.io.IOException;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.log.crt.ILogPacketFilter;
import fr.bcm.lib.sil.log.crt.ILogWriter;
import fr.bcm.lib.sil.log.crt.LogPacket;
import fr.bcm.lib.sil.log.filter.LogPacketFilterKeepAll;

/**
 * LogWriter base class
 * @Convenience
 */
public abstract class LogWriterBase implements ILogWriter {

	ILogPacketFilter	packetFiler;

	public void setPacketFilter(ILogPacketFilter packetFilter)
	{
		DebugBox.predicateNotNull(packetFilter);
		this.packetFiler = packetFilter;
	}

	public LogWriterBase()
	{
		this(new LogPacketFilterKeepAll());
	}

	public LogWriterBase(ILogPacketFilter packetFilter)
	{
		this.setPacketFilter(packetFilter);
	}

	@Override
	public void consume(final LogPacket logPacket) throws IOException
	{
		if(this.packetFiler.accept(logPacket))
		{
			this.write(logPacket);
		}
	}

	protected abstract void write(final LogPacket logPacket) throws IOException;

}
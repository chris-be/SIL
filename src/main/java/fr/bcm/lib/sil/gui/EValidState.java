/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui;

/**
 * Valid values available (for ValidTextField, maybe other components ?)
 */
public enum EValidState {

	/**
	 * Not verified
	 */
	  UNKNOWN
	/**
	 * Error
	 */
	, ERROR
	/**
	 * Ok
	 */
	, VALID
	;

	public static EValidState worse(final EValidState s1, final EValidState s2)
	{
		if(s1 == null)
		{
			return s2;
		}

		if(s2 == null)
		{
			return s1;
		}

		if((s1 == EValidState.ERROR) || (s2 == EValidState.ERROR))
		{
			return EValidState.ERROR;
		}

		if((s1 == EValidState.UNKNOWN) || (s2 == EValidState.UNKNOWN))
		{
			return EValidState.UNKNOWN;
		}

		return EValidState.VALID;
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.model;

import javax.swing.ImageIcon;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.AGModel;
import fr.bcm.lib.sil.gui.crt.IGIconModel;

/**
 * GIcon default model
 *
 */
public class GIconModel extends AGModel implements IGIconModel {

	protected ImageIcon	icon;

	/**
	 * Ctor
	 */
	public GIconModel()
	{
		this.icon = new ImageIcon();
	}

	/**
	 * Ctor
	 * @param icon
	 */
	public GIconModel(ImageIcon icon)
	{
		this.icon = (icon != null) ? icon : new ImageIcon();
	}

	/**
	 * 
	 * @param toCopy
	 */
	public void set(final GIconModel toCopy)
	{
		DebugBox.predicateNotNull(toCopy);
		this.setIcon(toCopy.getIcon());
	}

	@Override
	public ImageIcon getIcon()
	{
		return this.icon;
	}

	@Override
	public boolean setIcon(ImageIcon icon)
	{
		// Time consuming comparing width, height and than pixels !!
		boolean changed = true; // AGModel.isDifferent(this.icon, icon);
		if(changed)
		{
			this.icon = icon;
			this.wasModified = true;
		}
		
		return changed;
	}

/*
	@Override
	public boolean isNull()
	{
		return this.icon == null;
	}

	// Time consuming
	, Comparable<GIconModel>
	@Override
	public int compareTo(GIconModel o)
	{
		DebugBox.predicateNotNull(o);
		return CVS_Comparable.compare(this.icon, o.icon);
	}
*/
}
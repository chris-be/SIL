/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.crt;

import javax.swing.ImageIcon;

/**
 * Contract for icon model
 */
public interface IGIconModel extends IGModel
{
	/**
	 * @return Current icon
	 */
	public ImageIcon getIcon();

	/**
	 * 
	 * @param icon
	 * @return If value changed
	 */
	public boolean setIcon(ImageIcon icon);

}
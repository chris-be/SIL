/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui;

import java.awt.Color;

/**
 * Colors
 * 
 * Some default values
 */
public class GColors {

	public static final Color TRANSPARENT_BK_COLOR			= new Color(255, 255, 255, 0);

	// Used for validation
	public static final Color DEFAULT_UNKNOWN_STATE_COLOR	= new Color(128, 128, 128, 192);
	public static final Color DEFAULT_ERROR_STATE_COLOR		= new Color(255,  20,  10, 192);
	public static final Color DEFAULT_VALID_STATE_COLOR		= new Color( 40, 255,  80, 192);

}
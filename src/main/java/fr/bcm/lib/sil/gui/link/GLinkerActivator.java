/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.link;

import java.awt.Component;

import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGBooleanModel;

/**
 * GLinker to enable/disable component
 *
 */
public class GLinkerActivator extends GLinkerComponent<Component, IGBooleanModel> {

	/**
	 * Default value if no model attached
	 */
	public static final boolean	defaultNoModelActivate = false;

	/** Default "activation" when no model attached */
	protected boolean noModelActivate;

	/**
	 * Ctor
	 * @param component
	 * @param model
	 * @param noModelActivate
	 */
	public GLinkerActivator(Component component, IGBooleanModel model, boolean noModelActivate)
	{
		super(component, model);
		this.noModelActivate = defaultNoModelActivate;
	}

	/**
	 * Ctor
	 * @param component
	 * @param model
	 */
	public GLinkerActivator(Component component, IGBooleanModel model)
	{
		this(component, model, false);
	}

	/**
	 * Ctor
	 * @param component
	 */
	public GLinkerActivator(Component component)
	{
		this(component, null);
	}

	/**
	 * Ctor
	 * @param component
	 * @param noModelActivate
	 */
	public GLinkerActivator(Component component, boolean noModelActivate)
	{
		this(component, null, noModelActivate);
	}

	/**
	 * 
	 * @return no model activate
	 */
	public boolean isNoModelActivate()
	{
		return this.noModelActivate;
	}

	/**
	 * Set no model activate
	 * @param v
	 */
	public void setNoModelActivate(boolean v)
	{
		boolean changed = (this.noModelActivate != v);
		if(changed)
		{
			this.noModelActivate = v;
			this.modelChanged();
		}
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
			assert this.model == event.getModel();
			this.component.setEnabled(this.model.isOn());
		}
		else
		{	// No model attached
			this.component.setEnabled(this.noModelActivate);
		}
	}

}
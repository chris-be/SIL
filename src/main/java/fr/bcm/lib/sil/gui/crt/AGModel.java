/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.crt;

import java.util.function.Consumer;

import fr.bcm.lib.sil.CVS_Comparable;
import fr.bcm.lib.sil.CVS_String;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.lib.sil.patterns.TypedObserverMap;

/**
 * A default model 
 * @implNote Not really abstract. Just to know it can be used as a base.
 */
public abstract class AGModel implements IGModel {

	/**
	 * Flag: true when any "method" was called that modified a property
	 */
	protected boolean wasModified = false;

	/**
	 * Observers
	 */
	protected TypedObserverMap	observers = new TypedObserverMap();

	/**
	 * Flag: true while firing event
	 */
	protected boolean inFireModelChanged = false;

	@Override
	public void addObserver(IObserver<GModelChangedEvent> observer)
	{
		this.observers.add(GModelChangedEvent.class, observer);
	}

	@Override
	public void removeObserver(IObserver<GModelChangedEvent> observer)
	{
		this.observers.remove(GModelChangedEvent.class, observer);
	}

	@Override
	public void fireModelChanged(final Object source)
	{
		if(!this.wasModified)
		{	// Nothing to do
			return;
		}

		if(this.inFireModelChanged)
		{	// Break loop silently
			assert false : "loop !";
			return;
		}

		GModelChangedEvent event = new GModelChangedEvent(this);
		Consumer<IObserver<GModelChangedEvent>> c = new Consumer<IObserver<GModelChangedEvent>>() {
			public void accept(final IObserver<GModelChangedEvent> observer)
			{
				if(observer != source)
				{	// If source is given, prevent calling it
					observer.observed(event.getCopy(), source);
				}
			}
		};

		try
		{
			this.inFireModelChanged = true;
			this.wasModified = false;
			this.observers.fireEvent(GModelChangedEvent.class, c);
		}
		finally
		{
			this.inFireModelChanged = false;
		}
	}

	/**
	 * Check if c1 and c2 are different (null and empty are different) 
	 * @param c1
	 * @param c2
	 */
	public static <T extends Comparable<T>> boolean isDifferent(final T c1, final T c2)
	{
		return CVS_Comparable.compare(c1, c2) != 0;
	}

	/**
	 * Compare s1 and s2 (null and empty are considered the same)
	 * @param s1
	 * @param s2
	 */
	public static int compare(final String s1, final String s2)
	{

		boolean s2Empty = CVS_String.isNullOrEmpty(s2);
		if(CVS_String.isNullOrEmpty(s1))
		{
			return (s2Empty) ? 0 : -1;
		}

		return (s2Empty) ? +1 : s1.compareTo(s2);
	}

	/**
	 * Check if s1 and s2 are different (null and empty are considered the same)
	 * @param s1
	 * @param s2
	 */
	public static boolean isDifferent(final String s1, final String s2)
	{
		return AGModel.compare(s1, s2) != 0;
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.crt;

import fr.bcm.lib.sil.gui.EValidState;

/**
 * Contract for ValidState model
 */
public interface IGValidStateModel extends IGModel {

	/**
	 * Get "valid state"
	 */
	public EValidState getValidState();

}
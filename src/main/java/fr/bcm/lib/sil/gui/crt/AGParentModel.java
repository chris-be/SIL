/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.crt;

import java.util.HashSet;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.patterns.IObserver;

/**
 * AGModel which is parent of any other GModel
 * @implNote Not really abstract. Just to know it can be used as a base.
 */
public abstract class AGParentModel extends AGModel {

	protected HashSet<IGModel>	childs;

	protected IObserver<GModelChangedEvent>	childObserver;

	/**
	 * Ctor
	 */
	public AGParentModel()
	{
		this.childs = new HashSet<>();

		this.childObserver = new IObserver<GModelChangedEvent>() {
			@Override
			public void observed(GModelChangedEvent event, Object source)
			{
				AGParentModel.this.forwardChildEvent(event, source);
			}
		};
	}

	protected void addChilds(IGModel... childs)
	{
		for(IGModel child : childs)
		{
			DebugBox.predicateNotNull(child);
			if(!this.childs.contains(child))
			{
				child.addObserver(this.childObserver);
				this.childs.add(child);
			}
		}
	}

	protected void removeChilds(IGModel... childs)
	{
		for(IGModel child : childs)
		{
			DebugBox.predicateNotNull(child);
			if(this.childs.contains(child))
			{
				this.childs.remove(child);
				child.removeObserver(this.childObserver);
			}
		}
	}

	/**
	 * Forward children changes
	 */
	protected void forwardChildEvent(GModelChangedEvent event, Object source)
	{
		// A child changed: forward !
		if(this.inFireModelChanged)
		{	// Break loop silently
			assert false : "loop !";
			return;
		}

		GModelChangedEvent parentEvent = event.getCopy(this);
		this.observers.fireEvent(parentEvent, source);
	}

	/**
	 * In case parent modifies its children
	 * @Convenience
	 * 
	 * @param source
	 */
	public void fireChildrenChanged(final Object source)
	{
		for(IGModel child : this.childs)
		{
			child.fireModelChanged(source);
		}
	}

}
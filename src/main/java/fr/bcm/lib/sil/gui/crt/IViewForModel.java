/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.crt;

/**
 * Contract for a view handling a GModel
 *
 * @param <Model> Type of model handled
 */
public interface IViewForModel<Model extends IGModel> {

	/**
	 * Attach model (View must detach previously attached model)
	 *
	 * @param model If null: as if detach was called
	 */
	void attachModel(Model model);

	/**
	 * Detach current attached model (if any) and disable component
	 */
	void detachModel();

}
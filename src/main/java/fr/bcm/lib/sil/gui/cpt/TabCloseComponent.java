/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.cpt;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.plaf.basic.BasicButtonUI;

import fr.bcm.lib.sil.DebugBox;

/**
 * Close button for tab in JTabbedPane
 *
 * Based on ButtonTabComponent of "The Java Tutorials"
 */
public class TabCloseComponent extends JPanel {

	private static final long serialVersionUID = 730855865333662517L;

	// Parent pane
	protected final JTabbedPane	pane;

	protected JLabel			title;
	protected CloseButton		closeBtn;

	public TabCloseComponent(final JTabbedPane pane)
	{
		// Unset default FlowLayout's gaps
		super(new FlowLayout(FlowLayout.LEFT, 0, 0));

		DebugBox.predicateNotNull(pane);

		this.pane = pane;
		this.setOpaque(false);

		this.title = new JLabel();
		// Add more space between the label and the button
		title.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
		this.add(title);

		this.closeBtn = new CloseButton();
		// Add more space to the top of the component
		this.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
		this.add(this.closeBtn);
	}

	public String getTitle()
	{
		return this.title.getText();
	}

	public void setTitle(String title)
	{
		this.title.setText(title);
	}

	@Override
	public String getToolTipText()
	{
		return this.closeBtn.getToolTipText();
	}

	@Override
	public void setToolTipText(String text)
	{
		this.closeBtn.setToolTipText(text);
	}

	@Override
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		this.closeBtn.setEnabled(enabled);
	}

	private class CloseButton extends JButton implements ActionListener {

		private static final long serialVersionUID = -3492912730034835929L;

		// Size of square
		protected static final int size		= 17;
		// Delta for cross
		protected static final int delta	= 5;
		// Strike for cross
		protected final BasicStroke crossStroke	= new BasicStroke(2);

		public CloseButton()
		{
			this.setPreferredSize(new Dimension(size, size));

			// Make the button looks the same for all Laf's
			this.setUI(new BasicButtonUI());
			// Make it transparent
			this.setContentAreaFilled(false);
			// No need to be focusable
			this.setFocusable(false);
			this.setBorder(BorderFactory.createEtchedBorder());
			this.setBorderPainted(false);
			// Making nice rollover effect
			// We use the same listener for all buttons
			this.addMouseListener(buttonMouseListener);
			this.setRolloverEnabled(true);
			// Close the proper tab by clicking the button
			this.addActionListener(this);
		}
 
		@Override
		public void actionPerformed(ActionEvent e)
		{
			int i = pane.indexOfTabComponent(TabCloseComponent.this);
			if (i != -1) {
				pane.remove(i);
			}
		}
 
		// We don't want to update UI for this button
		@Override
		public void updateUI()
		{	}
 
		// Paint the cross
		@Override
		protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D)g.create();

				ButtonModel bm = this.getModel();
				if (bm.isPressed())
				{	// Shift the image for pressed button
					g2.translate(1, 1);
				}

				Color crossColor = bm.isEnabled() ? Color.BLACK : Color.LIGHT_GRAY;

				// Draw cross
				g2.setColor(crossColor);
				g2.setStroke(this.crossStroke);
				int left	= delta;
				int down	= delta; 
				int right	= this.getWidth() - delta - 1;
				int up		= this.getHeight() - delta - 1;
				g2.drawLine(left, down	, right, up);
				g2.drawLine(left, up	, right, down);

			g2.dispose();
		}
	}
 
	private final static MouseListener buttonMouseListener = new MouseAdapter() {

		@Override
		public void mouseEntered(MouseEvent e)
		{
			Component component = e.getComponent();
			if (component instanceof AbstractButton)
			{
				AbstractButton button = (AbstractButton) component;
				button.setBorderPainted(true);
			}
		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			Component component = e.getComponent();
			if (component instanceof AbstractButton)
			{
				AbstractButton button = (AbstractButton) component;
				button.setBorderPainted(false);
			}
		}

	};

}
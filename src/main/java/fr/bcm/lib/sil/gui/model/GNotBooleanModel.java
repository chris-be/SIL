/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.model;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGBooleanModel;
import fr.bcm.lib.sil.patterns.IObserver;

/**
 * Forward "not" IGBooleanModel
 *
 */
public class GNotBooleanModel implements IGBooleanModel {

	protected IGBooleanModel	model;

	/**
	 * 
	 * @param model
	 */
	public GNotBooleanModel(IGBooleanModel model)
	{
		DebugBox.predicateNotNull(model);
		this.model = model;
	}

	@Override
	public void fireModelChanged(Object source)
	{
		this.model.fireModelChanged(source);
	}

	@Override
	public void addObserver(IObserver<GModelChangedEvent> observer)
	{
		this.model.addObserver(observer);
	}

	@Override
	public void removeObserver(IObserver<GModelChangedEvent> observer)
	{
		this.model.removeObserver(observer);		
	}

	@Override
	public boolean isOn()
	{
		return !this.model.isOn();
	}

	@Override
	public boolean setOn(boolean on)
	{
		return this.model.setOn(!on);
	}

}
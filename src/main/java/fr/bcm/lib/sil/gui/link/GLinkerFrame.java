/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.link;

import javax.swing.JFrame;

import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGLabelModel;

/**
 * GLinker for JFrame
 *
 */
public class GLinkerFrame extends GLinkerComponent<JFrame, IGLabelModel> {

	/**
	 * Ctor
	 * @param component
	 * @param model
	 */
	public GLinkerFrame(JFrame component, IGLabelModel model)
	{
		super(component, model);
	}

	/**
	 * Ctor
	 * @param component
	 */
	public GLinkerFrame(JFrame component)
	{
		this(component, null);
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
			assert this.model == event.getModel();
			this.component.setTitle(this.model.getText());
		}
		else
		{
			this.component.setTitle(null);
		}
	}

}
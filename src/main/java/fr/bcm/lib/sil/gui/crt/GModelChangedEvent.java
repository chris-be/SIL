/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.crt;

import java.util.HashSet;

import fr.bcm.lib.sil.CVS_Collection;
import fr.bcm.lib.sil.patterns.IEvent;

/**
 * Event for model changes
 */
public class GModelChangedEvent implements IEvent {

	protected final IGModel model;

	/**
	 * Which model forwarded child event
	 */
	protected final HashSet<IGModel> forwarders;

	/**
	 * 
	 * @return model that changed
	 */
	public final IGModel getModel()
	{
		return this.model;
	}

	/**
	 * 
	 * @return if any forwarder
	 */
	public boolean hasForwarders()
	{
		return !CVS_Collection.isNullOrEmpty(this.forwarders);
	}

	/**
	 * Has "model" forwarded event ?
	 * @param model
	 * @return if a forwarder is registered for model
	 */
	public boolean isForwarder(final IGModel model)
	{
		return (this.forwarders != null) ? this.forwarders.contains(model) : false;
	}

	/**
	 * 
	 * @param model
	 */
	public GModelChangedEvent(final IGModel model)
	{
		this.model = model;
		this.forwarders = null;
	}

	/**
	 * 
	 * @param toCopy
	 */
	public GModelChangedEvent(final GModelChangedEvent toCopy)
	{
		this.model = toCopy.model;
		if(toCopy.forwarders != null)
		{
			this.forwarders = new HashSet<>();
			this.forwarders.addAll(toCopy.forwarders);
		}
		else
		{
			this.forwarders = null;
		}
	}

	/**
	 * 
	 * @param parent
	 * @param toCopy
	 */
	public GModelChangedEvent(final IGModel parent, final GModelChangedEvent toCopy)
	{
		this.model = toCopy.model;
		this.forwarders = new HashSet<>();
		if(toCopy.forwarders != null)
		{
			this.forwarders.addAll(toCopy.forwarders);
		}
		this.forwarders.add(parent);
	}

	/**
	 * 
	 * @return a copy
	 */
	public GModelChangedEvent getCopy()
	{
		return new GModelChangedEvent(this);
	}

	/**
	 * 
	 * @param parent
	 * @return a copy with specified parent
	 */
	public GModelChangedEvent getCopy(final IGModel parent)
	{
		return new GModelChangedEvent(parent, this);
	}

}
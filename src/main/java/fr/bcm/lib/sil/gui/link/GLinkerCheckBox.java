/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.link;

import javax.swing.JCheckBox;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGBooleanModel;

/**
 * GLinker for JCheckBox selected state (checked/unkchecked)
 *
 */
public class GLinkerCheckBox extends GLinkerComponent<JCheckBox, IGBooleanModel> {

	protected CheckListener	checkListener;

	/**
	 * Ctor
	 * @param component
	 * @param model
	 */
	public GLinkerCheckBox(JCheckBox component, IGBooleanModel model)
	{
		super(component, model);
		this.checkListener = new CheckListener();
		this.component.addChangeListener(this.checkListener);
	}

	/**
	 * Ctor
	 * @param component
	 */
	public GLinkerCheckBox(JCheckBox component)
	{
		this(component, null);
	}

	@Override
	protected void finalize() throws Throwable
	{
		this.component.removeChangeListener(this.checkListener);
		super.finalize();
	}

	@Override
	public void attachModel(IGBooleanModel model)
	{
		super.attachModel(model);
		// Enable component
		this.component.setEnabled(model != null);
	}

	@Override
	public void detachModel()
	{
		if(this.model != null)
		{
			super.detachModel();
			// Disable component
			this.component.setEnabled(false);
		}
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
			assert this.model == event.getModel();
			this.component.setSelected(this.model.isOn());
		}
		else
		{
			this.component.setSelected(false);
		}
	}

	protected void checkChanged()
	{
		assert this.model != null : "isEnabled should be false";

		this.model.setOn(this.component.isSelected());
		this.model.fireModelChanged(this);
	}

	protected class CheckListener implements ChangeListener {

		@Override
		public void stateChanged(ChangeEvent e)
		{
			GLinkerCheckBox.this.checkChanged();
		}

	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.crt;

import fr.bcm.lib.sil.patterns.IObservable;

/**
 * Contract for GModel
 */
public interface IGModel extends IObservable<GModelChangedEvent> {

	/**
	 * Removing content of Document (JTextField / JTexteArea) leads to "remove" and "no insert" {@code => } empty string, not null
	 * To avoid complication, "" must be used (set should change null to "")
	 */
	static String DEFAULT_TEXT_VALUE = "";

	/**
	 * Fire event that model changed
	 * @param source
	 */
	public void fireModelChanged(final Object source);

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.crt;

/**
 * Contract for label (read only text) model
 */
public interface IGLabelModel extends IGModel
{
	/**
	 * @return Current text
	 */
	public String getText();

	/**
	 * @Convenience
	 */
	public boolean isTextNullOrEmpty();

}
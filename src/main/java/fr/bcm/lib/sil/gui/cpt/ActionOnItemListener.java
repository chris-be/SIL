/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.cpt;

import java.util.EventListener;

/**
 * Action on item of list
 */
public interface ActionOnItemListener extends EventListener {

	/**
	 *
	 * @param index Index of item
	 */
	void actionItem(final int index);

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.link;

import java.awt.Component;

import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.text.JTextComponent;

import fr.bcm.lib.sil.gui.cpt.IValidStateComponent;
import fr.bcm.lib.sil.gui.cpt.TabCloseComponent;
import fr.bcm.lib.sil.gui.crt.IGBooleanModel;
import fr.bcm.lib.sil.gui.crt.IGIconModel;
import fr.bcm.lib.sil.gui.crt.IGLabelModel;
import fr.bcm.lib.sil.gui.crt.IGLinker;
import fr.bcm.lib.sil.gui.crt.IGTextModel;
import fr.bcm.lib.sil.gui.crt.IGValidStateModel;
import fr.bcm.lib.sil.gui.link.GLinkerTitleButton;
import fr.bcm.lib.sil.gui.link.GLinkerDialog;
import fr.bcm.lib.sil.gui.link.GLinkerFrame;
import fr.bcm.lib.sil.gui.link.GLinkerLabel;

/**
 * "Convenience" for GLinker
 *
 * Create adapted GLinker for given component and model
 *
 * @Convenience
 */
public class CVS_GLinker {

//
// Title linkers
//

	/**
	 * Create a linker for JFrame title
	 * @param comp
	 * @param model
	 * @return a linker
	 */
	public static IGLinker<IGLabelModel> titleLinker(JFrame comp, IGLabelModel model)
	{
		return new GLinkerFrame(comp, model);
	}

	/**
	 * Create a linker for JDialog title
	 * @param comp
	 * @param model
	 * @return a linker
	 */
	public static IGLinker<IGLabelModel> titleLinker(JDialog comp, IGLabelModel model)
	{
		return new GLinkerDialog(comp, model);
	}

	/**
	 * Create a linker for JLabel title
	 * @param comp
	 * @param model
	 * @return a linker
	 */
	public static IGLinker<IGLabelModel> titleLinker(JLabel comp, IGLabelModel model)
	{
		return new GLinkerLabel(comp, model);
	}

	/**
	 * Create a linker for AbstractButton title
	 * <br>Reminder: JButton, JCheckBox, JMenu, JMenuItem are AbstractButton
	 * @param comp
	 * @param model
	 * @return a linker
	 */
	public static IGLinker<IGLabelModel> titleLinker(AbstractButton comp, IGLabelModel model)
	{
		return new GLinkerTitleButton(comp, model);
	}

	/**
	 * Create a linker for TabCloseComponent title
	 * @param comp
	 * @param model
	 * @return a linker
	 */
	public static IGLinker<IGLabelModel> titleLinker(TabCloseComponent comp, IGLabelModel model)
	{
		return new GLinkerTabCloseComponent(comp, model);
	}

//
// Tooltip linkers
//

	/**
	 * Create a linker for JComponent tooltip
	 * @param comp
	 * @param model
	 * @return a linker
	 */
	public static IGLinker<IGLabelModel> tooltipLinker(JComponent comp, IGLabelModel model)
	{
		return new GLinkerToolTip(comp, model);
	}

//
// Icon linkers
//

	/**
	 * Create a linker for AbstractButton icon
	 * @param comp
	 * @param model
	 * @return a linker
	 */
	public static IGLinker<IGIconModel> iconLinker(AbstractButton comp, IGIconModel model)
	{
		return new GLinkerIcon(comp, model);
	}

//
// Active linkers
//

	/**
	 * Create a linker for Component to know if it's active or not
	 * @param comp
	 * @param model
	 * @return a linker
	 */
	public static IGLinker<IGBooleanModel> activeLinker(Component comp, IGBooleanModel model)
	{
		return new GLinkerActivator(comp, model);
	}

//
// Text linkers
//

	/**
	 * Create a linker for JTextComponent
	 * @param comp
	 * @param model
	 * @return a linker
	 */
	public static IGLinker<IGTextModel> textLinker(JTextComponent comp, IGTextModel model)
	{
		return new GLinkerTextComponent(comp, model);
	}

//
// Check linkers
//

	/**
	 * Create a linker for JCheckBox checked state
	 * @param comp
	 * @param model
	 * @return a linker
	 */
	public static IGLinker<IGBooleanModel> checkLinker(JCheckBox comp, IGBooleanModel model)
	{
		return new GLinkerCheckBox(comp, model);
	}

//
// Valid linkers
//

	/**
	 * Create a linker for IValidStateComponent
	 * @param comp
	 * @param model
	 * @return a linker
	 */
	public static IGLinker<IGValidStateModel> checkLinker(IValidStateComponent comp, IGValidStateModel model)
	{
		return new GLinkerValidComponent(comp, model);
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.link;

import javax.swing.text.JTextComponent;

import fr.bcm.lib.sil.gui.cpt.SimplePlainDocument;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGTextModel;

/**
 * GLinker for JTextComponent
 *
 */
public class GLinkerTextComponent extends GLinkerComponent<JTextComponent, IGTextModel> {

	ChangedListener	dcl;

	/**
	 * 
	 * @param component
	 * @param model
	 */
	public GLinkerTextComponent(JTextComponent component, IGTextModel model)
	{
		super(component, model);

		// Events
		this.dcl = new ChangedListener();
		this.component.setDocument(new SimplePlainDocument(dcl));
	}

	/**
	 * 
	 * @param component
	 */
	public GLinkerTextComponent(JTextComponent component)
	{
		this(component, null);
	}

	@Override
	protected void finalize() throws Throwable
	{
		this.component.setDocument(null);
		super.finalize();
	}

	@Override
	public void attachModel(IGTextModel model)
	{
		super.attachModel(model);
		// Enable component
		this.component.setEnabled(model != null);
	}

	@Override
	public void detachModel()
	{
		if(this.model != null)
		{
			super.detachModel();
			// Disable component
			this.component.setEnabled(false);
		}
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
			assert this.model == event.getModel();
			this.component.setText(this.model.getText());
		}
		else
		{
			this.component.setText(null);
		}
	}

	protected void textModified()
	{
		assert this.model != null : "isEnabled should be false";

		this.model.setText(this.component.getText());
		this.model.fireModelChanged(this);
	}

	protected class ChangedListener implements SimplePlainDocument.DocumentChangedListener {

		@Override
		public void documentChanged()
		{
			if(GLinkerTextComponent.this.model != null)
			{
				GLinkerTextComponent.this.textModified();
			}
		}
	}

}
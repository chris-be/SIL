/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.model;

import fr.bcm.lib.sil.CVS_Comparable;
import fr.bcm.lib.sil.CVS_String;
import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.AGModel;
import fr.bcm.lib.sil.gui.crt.IGModel;
import fr.bcm.lib.sil.gui.crt.IGTextLogModel;

/**
 * GText for log default model
 *
 */
public class GTextLogModel extends AGModel implements IGTextLogModel, Comparable<GTextLogModel> {

	protected StringBuilder		sb;

	/**
	 * 
	 */
	public GTextLogModel()
	{
		this.sb = new StringBuilder();
	}

	/**
	 * 
	 * @param toCopy
	 */
	public void set(final GTextLogModel toCopy)
	{
		DebugBox.predicateNotNull(toCopy);
		this.setText(toCopy.getText());
	}

	@Override
	public String getText()
	{
		return this.sb.toString();
	}

	@Override
	public boolean setText(String text)
	{
		if(text == null)
		{
			text = IGModel.DEFAULT_TEXT_VALUE;
		}

		boolean changed = (this.sb.length() != text.length());
		changed = (changed) || AGModel.isDifferent(this.getText(), text);
		if(changed)
		{
			this.sb.setLength(0);
			this.sb.append(text);
			this.wasModified = true;
		}
		
		return changed;
	}

	@Override
	public boolean isTextNullOrEmpty()
	{
		return (this.sb.length() == 0);
	}

	@Override
	public boolean appendText(String text)
	{
		boolean changed = !CVS_String.isNullOrEmpty(text);
		if(changed)
		{
			this.sb.append(text);
			this.wasModified = true;
		}

		return changed;
	}

	@Override
	public int compareTo(GTextLogModel o)
	{
		DebugBox.predicateNotNull(o);
		int tmp = this.sb.length() - o.sb.length();
		if(tmp != 0)
		{
			return tmp;
		}

		return CVS_Comparable.compare(this.getText(), o.getText());
	}

}
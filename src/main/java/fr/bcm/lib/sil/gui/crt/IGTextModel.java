/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.crt;

/**
 * Contract for text model
 */
public interface IGTextModel extends IGLabelModel
{

	/**
	 * 
	 * @param text
	 * @return If value changed
	 */
	public boolean setText(String text);

}
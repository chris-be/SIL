/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.model;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.AGModel;
import fr.bcm.lib.sil.gui.crt.IGBooleanModel;

/**
 * GBoolean default model
 *
 */
public class GBooleanModel extends AGModel implements IGBooleanModel, Comparable<GBooleanModel> {

	boolean		on;

	@Override
	public boolean isOn()
	{
		return this.on;
	}

	/**
	 * Ctor
	 */
	public GBooleanModel()
	{
		this.on = false;
	}

	/**
	 * 
	 * @param toCopy
	 */
	public void set(final GBooleanModel toCopy)
	{
		DebugBox.predicateNotNull(toCopy);
		this.setOn(toCopy.isOn());
	}

	@Override
	public boolean setOn(boolean on)
	{
		boolean changed = this.on != on;
		if(changed)
		{
			this.on = on;
			this.wasModified = true;
		}
		
		return changed;
	}

	@Override
	public int compareTo(GBooleanModel o)
	{
		DebugBox.predicateNotNull(o);
		return Boolean.compare(this.on, o.on);
	}

}
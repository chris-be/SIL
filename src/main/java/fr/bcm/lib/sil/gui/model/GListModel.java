/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.model;

import java.util.function.Consumer;

import javax.swing.DefaultListModel;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGListModel;
import fr.bcm.lib.sil.old2new.EnumerationIterator;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.lib.sil.patterns.TypedObserverMap;

/**
 * GList default model
 *
 * @param <Item> @see IGListModel
 */
public class GListModel<Item> extends DefaultListModel<Item> implements IGListModel<Item> {

	private static final long serialVersionUID = 4835939395702066616L;

	/**
	 * Flag: true when any "method" was called that modified a property
	 */
	protected boolean wasModified = false;

	/**
	 * Listeners
	 */
	protected TypedObserverMap	observers = new TypedObserverMap();

	/**
	 * Flag: true while firing event
	 */
	protected boolean inFireModelChanged = false;

	/**
	 * 
	 */
	public GListModel()
	{
		super();
	}

	/**
	 * 
	 * @param toCopy
	 */
	public void set(final GListModel<Item> toCopy)
	{
		DebugBox.predicateNotNull(toCopy);
		this.clear();
		this.addAll(toCopy.createIterable());
	}

	/**
	 * 
	 * @return iterable of items
	 */
	public Iterable<Item> createIterable()
	{
		EnumerationIterator<Item> iterator = new EnumerationIterator<>(this.elements());
		return iterator.createIterable();
	}

	/**
	 * 
	 * @param items
	 */
	public void addAll(final Iterable<Item> items)
	{
		DebugBox.predicateNotNull(items);
		for(Item item : items)
		{
			this.addElement(item);
		}
	}

	// IGModel

	@Override
	public void addObserver(IObserver<GModelChangedEvent> observer)
	{
		this.observers.add(GModelChangedEvent.class, observer);
	}

	@Override
	public void removeObserver(IObserver<GModelChangedEvent> observer)
	{
		this.observers.remove(GModelChangedEvent.class, observer);
	}

	@Override
	public void fireModelChanged(final Object source)
	{
		if(!this.wasModified)
		{	// Nothing to do
			return;
		}

		if(this.inFireModelChanged)
		{	// Break loop silently
			assert false : "loop !";
			return;
		}

		GModelChangedEvent event = new GModelChangedEvent(this);
		Consumer<IObserver<GModelChangedEvent>> c = new Consumer<IObserver<GModelChangedEvent>>() {
			public void accept(final IObserver<GModelChangedEvent> observer)
			{
				if(observer != source)
				{	// If source is given, prevent calling it
					observer.observed(event.getCopy(), source);
				}
			}
		};

		try
		{
			this.inFireModelChanged = true;
			this.wasModified = false;
			this.observers.fireEvent(GModelChangedEvent.class, c);
		}
		finally
		{
			this.inFireModelChanged = false;
		}
	}

}
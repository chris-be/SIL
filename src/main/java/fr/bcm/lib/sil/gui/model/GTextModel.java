/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.model;

import fr.bcm.lib.sil.CVS_Comparable;
import fr.bcm.lib.sil.CVS_String;
import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.AGModel;
import fr.bcm.lib.sil.gui.crt.IGModel;
import fr.bcm.lib.sil.gui.crt.IGTextModel;

/**
 * GText default model
 *
 */
public class GTextModel extends AGModel implements IGTextModel, Comparable<GTextModel> {

	protected String	text;

	/**
	 * 
	 */
	public GTextModel()
	{
		this.text = IGModel.DEFAULT_TEXT_VALUE;
	}

	/**
	 * 
	 * @param text
	 */
	public GTextModel(String text)
	{
		this.text = (text != null) ? text : IGModel.DEFAULT_TEXT_VALUE;
	}

	/**
	 * 
	 * @param toCopy
	 */
	public void set(final GTextModel toCopy)
	{
		DebugBox.predicateNotNull(toCopy);
		this.setText(toCopy.getText());
	}

	@Override
	public String getText()
	{
		return this.text;
	}

	@Override
	public boolean setText(String text)
	{
		if(text == null)
		{
			text = IGModel.DEFAULT_TEXT_VALUE;
		}

		boolean changed = AGModel.isDifferent(this.text, text);
		if(changed)
		{
			this.text = text;
			this.wasModified = true;
		}
		
		return changed;
	}

	@Override
	public boolean isTextNullOrEmpty()
	{
		return CVS_String.isNullOrEmpty(this.text);
	}

	@Override
	public int compareTo(GTextModel o)
	{
		DebugBox.predicateNotNull(o);
		return CVS_Comparable.compare(this.text, o.text);
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui;

import java.awt.Color;

import fr.bcm.lib.sil.DebugBox;

/**
 * "Convenience" for Color
 *
 * @Convenience
 */
public class CVS_Color {

	/**
	 * Convert a color to "grey" using luminosity method
	 * @param color
	 */
	public static Color toGreyLumMethod(final Color color)
	{
		DebugBox.predicateNotNull(color);

		int r = color.getRed()   * 2126 / 10000;
		int g = color.getGreen() * 7152 / 10000;
		int b = color.getBlue()  *  722 / 10000;

		int f = Math.min(r+g+b, 255);
		return new Color(f, f, f, color.getAlpha());
	}

}
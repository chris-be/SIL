/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.link;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGLinker;
import fr.bcm.lib.sil.gui.crt.IGModel;
import fr.bcm.lib.sil.patterns.IObserver;

/**
 * Base class for all GLinker of Component.
 *
 * @param <Comp> Component linked
 * @param <Model> @see IGLinker 
 * @Convenience
 */
public abstract class GLinkerComponent<Comp, Model extends IGModel> implements IGLinker<Model>
	, IObserver<GModelChangedEvent> {

	/** Component */
	protected Comp		component; 

	/** Model */
	protected Model	model;

	/**
	 * Ctor
	 * @param component
	 * @param model
	 */
	public GLinkerComponent(Comp component, Model model)
	{
		DebugBox.predicateNotNull(component);
		this.component = component;
		this.setModel(model);
		this.modelChanged();
	}

	/**
	 * Ctor
	 * @param component
	 */
	public GLinkerComponent(Comp component)
	{
		this(component, (Model)(null));
	}

	/**
	 * Use with care
	 * @return Linked component
	 */
	public Comp getComponent()
	{
		return this.component;
	}

	/**
	 * Set model and register observer
	 */
	private void setModel(Model model)
	{
		this.model = model;
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		this.modelChanged();
		this.model.addObserver(this);
	}

	@Override
	public void attachModel(Model model)
	{
		this.detachModel();
		this.setModel(model);
	}

	@Override
	public void detachModel()
	{
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		this.model.removeObserver(this);
		this.model = null;
		this.modelChanged();
	}

	/** @Convenience */
	protected void modelChanged()
	{
		this.observed(new GModelChangedEvent(this.model), null);
	}

}
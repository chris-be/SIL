/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.file.FileExtension;
import fr.bcm.lib.sil.file.FileExtensions;
import fr.bcm.lib.sil.gui.crt.IGModel;
import fr.bcm.lib.sil.gui.crt.IViewForModel;

/**
 * "Convenience" for Gui components
 *
 * @Convenience
*/
public class CVS_Gui {

	/**
	 * Detach model of provided views
	 * @param views
	 */
	@SafeVarargs
	public static void detachModel(IViewForModel<? extends IGModel>... views)
	{
		for(IViewForModel<? extends IGModel> view : views)
		{
			view.detachModel();
		}
	}

	/**
	 * 
	 * @param fc
	 * @param fes
	 */
	public static void addFileFilters(JFileChooser fc, FileExtensions fes)
	{
		DebugBox.predicateNotNull(fc);
		FileNameExtensionFilter ff = null;
		for(FileExtension fe : fes)
		{
			ff = new FileNameExtensionFilter(fe.getDescription(), fe.getExtension());
			fc.addChoosableFileFilter(ff);
		}

		if(ff != null)
		{	// Set selected to last one
			fc.setFileFilter(ff);
		}
	}

}
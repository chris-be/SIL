/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.link;

import javax.swing.AbstractButton;

import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGIconModel;

/**
 * GLinker for AbstractButton
 *
 */
public class GLinkerIcon extends GLinkerComponent<AbstractButton, IGIconModel> {

	/**
	 * Ctor
	 * @param component
	 * @param model
	 */
	public GLinkerIcon(AbstractButton component, IGIconModel model)
	{
		super(component, model);
	}

	/**
	 * Ctor
	 * @param component
	 */
	public GLinkerIcon(AbstractButton component)
	{
		this(component, null);
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
			assert this.model == event.getModel();
			this.component.setIcon(this.model.getIcon());
		}
		else
		{
			this.component.setIcon(null);
		}
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.cpt;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JTextField;

import fr.bcm.lib.sil.gui.CVS_Color;
import fr.bcm.lib.sil.gui.EValidState;
import fr.bcm.lib.sil.gui.GColors;

/**
 * TextField component showing "valid state"
 *
 */
public class ValidTextField extends JTextField implements IValidStateComponent {

	private static final long serialVersionUID = -7992530564684270757L;

	/**
	 * Color used to show "unknown state"
	 */
	protected Color		unknownStateColor;

	/**
	 * Color used to show "error state"
	 */
	protected Color		errorStateColor;

	/**
	 * Color used to show "ok state"
	 */
	protected Color		validStateColor;

	/**
	 * Radius in pixels
	 */
	protected int		radius;

	/**
	 * Valid state
	 */
	protected EValidState	validState;

	public Color getUnknownStateColor()
	{
		return this.unknownStateColor;
	}

	public void setUnknownStateColor(final Color v)
	{
		this.unknownStateColor = v;
	}

	public Color getErrorStateColor()
	{
		return this.errorStateColor;
	}

	public void setErrorStateColor(final Color v)
	{
		this.errorStateColor = v;
	}

	public Color getValidStateColor()
	{
		return this.validStateColor;
	}

	public void setValidStateColor(final Color v)
	{
		this.validStateColor = v;
	}

	public ValidTextField()
	{
		super();
		this.unknownStateColor	= GColors.DEFAULT_UNKNOWN_STATE_COLOR;
		this.errorStateColor	= GColors.DEFAULT_ERROR_STATE_COLOR;
		this.validStateColor	= GColors.DEFAULT_VALID_STATE_COLOR;
		this.radius = 8;
	}

	@Override
	public EValidState getValidState()
	{
		return this.validState;
	}

	@Override
	public void setValidState(EValidState state)
	{
		this.validState = state;
		this.repaintValidState();
	}

	/**
	 * Ask repaint for validState area
	 */
	protected void repaintValidState()
	{
		Rectangle r = this.computeDrawArea();
		this.repaint(r.x, r.y, r.width, r.height);
	}

//
// Paint
//

	protected Rectangle computeDrawArea()
	{
		int x = this.getWidth();
		int y = this.getHeight();
		int r = Math.min(this.radius, y);
		int d = 2*r;

		return new Rectangle(x-r, 1-r, d, d);
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		if(this.isVisible())
		{
			this.paintState(g);
		}
	}

	protected Color getColorFomrCurrentValidState()
	{
		Color color;
		if(this.validState == null)
		{
			color = null;
		}
		else
		{
			switch(this.validState)
			{
				case UNKNOWN:
				{
					color = this.unknownStateColor;
				} break;
				case ERROR:
				{
					color = this.errorStateColor;
				} break;
				case VALID:
				{
					color = this.validStateColor;
				} break;
				default:
				{
					assert false : "Unkown valid state";
					color = null;
				}
			}
			
			if(!this.isEnabled() && (color != null))
			{
				color = CVS_Color.toGreyLumMethod(color);
			}
		}

		return color;
	}

	protected void paintState(Graphics g)
	{
		Color cl = this.getColorFomrCurrentValidState();
		if(cl != null)
		{
			Rectangle r = this.computeDrawArea();
			g.setColor(cl);
			g.fillArc(r.x, r.y, r.width, r.height, 180, 270);
		}
	}

}
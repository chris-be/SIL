/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.cpt;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JList;

/**
 * List component which handles "double click" as an action
 * 
 * @param <TItem> @see JList
 */
public class ActionList<TItem> extends JList<TItem> {

	private static final long serialVersionUID = -8247015945248890274L;

	/**
	 * Ctor
	 */
	public ActionList()
	{
		super();

		// Events
		MouseListener ml = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e)
			{
				// super.mouseClicked(e);
				if((e.getButton() == MouseEvent.BUTTON1) && (e.getClickCount() == 2))
				{
					ActionList.this.doubleLeftClick(e);
				}
			}
		};
		this.addMouseListener(ml);
	}

	protected void doubleLeftClick(MouseEvent e)
	{
		Point p = e.getPoint();

		Rectangle r = this.getCellBounds(0, this.getLastVisibleIndex());
		if((r != null) && r.contains(p))
		{	// Ensure use clicked on a item (and not in free space)
			int index = this.locationToIndex(p);
			this.fireActionOnItem(index);
		}
	}

	/**
	 * Add an ActionOnItemListener
	 * @param listener
	 */
	public void addActionOnItemListener(ActionOnItemListener listener)
	{
		this.listenerList.add(ActionOnItemListener.class, listener);
	}

	/**
	 * Remove the ActionOnItemListener
	 * @param listener
	 */
	public void removeActionOnItemListener(ActionOnItemListener listener)
	{
		this.listenerList.remove(ActionOnItemListener.class, listener);
	}

	protected void fireActionOnItem(int index)
	{
		for(ActionOnItemListener listener : this.listenerList.getListeners(ActionOnItemListener.class))
		{
			listener.actionItem(index);
		}
	}

}
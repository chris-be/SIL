/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.link;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.EValidState;
import fr.bcm.lib.sil.gui.cpt.IValidStateComponent;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGValidStateModel;

/**
 * GLinker for IValidStateComponent
 *
 */
public class GLinkerValidComponent extends GLinkerComponent<IValidStateComponent, IGValidStateModel> {

	/**
	 * Default state if no model attached
	 */
	public static final EValidState	defaultNoModelValidState = EValidState.UNKNOWN;

	/** Default "valid state" when no model attached */
	protected EValidState noModelState;

	/**
	 * 
	 * @param component
	 * @param model
	 * @param noModelState
	 */
	public GLinkerValidComponent(IValidStateComponent component, IGValidStateModel model, EValidState noModelState)
	{
		super(component, model);
		DebugBox.predicateNotNull(noModelState);
		this.noModelState = noModelState;
	}

	/**
	 * 
	 * @param component
	 * @param model
	 */
	public GLinkerValidComponent(IValidStateComponent component, IGValidStateModel model)
	{
		this(component, model, defaultNoModelValidState);
	}

	/**
	 * 
	 * @param component
	 */
	public GLinkerValidComponent(IValidStateComponent component)
	{
		this(component, null);
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
			assert this.model == event.getModel();
			this.component.setValidState(this.model.getValidState());
		}
		else
		{
			this.component.setValidState(noModelState);
		}
	}

}
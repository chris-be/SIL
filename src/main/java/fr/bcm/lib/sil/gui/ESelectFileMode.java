/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui;

/**
 * SelectFileMode available
 */
public enum ESelectFileMode {

	  FILE_AND_DIRECTORY
	, FILES_ONLY
	, DIRECTORIES_ONLY
	/**
	 * File must be found in user path (example : /usr/bin/sh)
	 */
	, FILES_IN_PATHS_ONLY

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui;

import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * "Convenience" for Swing layouting
 * 
 * @Convenience
*/
public class CVS_Layout {

/*
	public static void setDefaultSize(JComponent jc, int width, int height) {
		DebugBox.predicateNotNull(jc);

		Dimension d = new Dimension(width, height);
		jc.setMinimumSize(d);
		jc.setPreferredSize(d);
		// jc.setMinimumSize(d);
	}
*/

	//
	// GridBagConstraint
	//

	public static final int DEFAULT_ANCHOR			= GridBagConstraints.WEST;
	public static final int DEFAULT_FILL			= GridBagConstraints.HORIZONTAL;
	public static final double DEFAULT_WEIGHT_X		= 0.0;
	public static final double DEFAULT_WEIGHT_Y		= 0.0;
	public static final Insets DEFAULT_INSETS		= new Insets(1, 1, 1, 1);

	public static GridBagConstraints createGBC(int x, int y, int w, int h)
	{
		return CVS_Layout.createGBC(x, y, w, h, DEFAULT_WEIGHT_X, DEFAULT_WEIGHT_Y);
	}

	public static GridBagConstraints createGBC(int x, int y, int w, int h, double weightx, double weighty)
	{
		return CVS_Layout.createGBC(x, y, w, h, weightx, weighty, DEFAULT_INSETS);
	}

	public static GridBagConstraints createGBC(int x, int y, int w, int h, double weightx, double weighty, final Insets insets)
	{
		return new GridBagConstraints(
			  x, y, w, h, weightx, weighty, DEFAULT_ANCHOR
			, DEFAULT_FILL, insets, 0, 0
		);
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.link;

import java.util.ArrayList;
import java.util.Iterator;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.IGLinker;

/**
 * Keep references to linkers and ensure detach at clear/finalize
 *
 *  @Convenience
 */
public class GLinkerList {

	/**  */
	protected ArrayList<IGLinker<?>>	linkers;

	/**
	 * 
	 */
	public GLinkerList()
	{
		this.linkers = new ArrayList<>();
	}

	/** Cleanup
	 */
	@Override
	protected void finalize() throws Throwable
	{
		this.clear();
		super.finalize();
	}

	/**
	 * Add linker
	 * @param linker
	 */
	public void add(IGLinker<?> linker)
	{
		DebugBox.predicate(!this.linkers.contains(linker));
		this.linkers.add(linker);
	}

	/**
	 * Remove linker (and detach model)
	 * @param linker
	 * @return if success
	 */
	public boolean remove(IGLinker<?> linker)
	{
		Iterator<IGLinker<?>> iter = this.linkers.iterator();
		while(iter.hasNext())
		{
			IGLinker<?> test = iter.next();
			if(test == linker)
			{
				// Detach before removing
				test.detachModel();
				iter.remove();
				return true;
			}
		}

		return false;
	}

	/**
	 *  Remove all linkers (and detach models)
	 */
	public void clear()
	{
		for(IGLinker<?> linker : this.linkers)
		{
			linker.detachModel();
		}

		this.linkers.clear();
	}

}
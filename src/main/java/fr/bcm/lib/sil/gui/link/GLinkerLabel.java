/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.link;

import javax.swing.JLabel;

import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGLabelModel;

/**
 * GLinker for JLabel
 *
 */
public class GLinkerLabel extends GLinkerComponent<JLabel, IGLabelModel> {

	/**
	 * 
	 * @param component
	 * @param model
	 */
	public GLinkerLabel(JLabel component, IGLabelModel model)
	{
		super(component, model);
	}

	/**
	 * 
	 * @param component
	 */
	public GLinkerLabel(JLabel component)
	{
		this(component, null);
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
			assert this.model == event.getModel();
			this.component.setText(this.model.getText());
		}
		else
		{
			this.component.setText(null);
		}
	}

}
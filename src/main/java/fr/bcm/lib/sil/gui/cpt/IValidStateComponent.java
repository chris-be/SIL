/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.cpt;

import fr.bcm.lib.sil.gui.EValidState;

/**
 * Contract for component that show a "valid state"
 *
 */
public interface IValidStateComponent {

	/**
	 * Get shown "valid state"
	 */
	public EValidState getValidState();

	/**
	 * Set shown "valid state"
	 */
	public void setValidState(EValidState state);

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.text.Caret;
import javax.swing.text.DefaultCaret;
import javax.swing.text.JTextComponent;

import fr.bcm.lib.sil.DebugBox;

/**
 * "Convenience" for Swing components
 * 
 * @Convenience
 */
public class CVS_Swing {

	/**
	 * Centralize code for transparent background
	 * TODO: use Look N' Feel for transparency
	 * @param jc
	 */
	public static void setTransparentBK(JComponent jc)
	{
		DebugBox.predicateNotNull(jc);
		jc.setBackground(GColors.TRANSPARENT_BK_COLOR);
	}

	/**
	 * 
	 * @param enabled
	 * @param cpts
	 */
	public static void setEnabled(boolean enabled, JComponent... cpts)
	{
		for(JComponent cpt : cpts)
		{
			cpt.setEnabled(enabled);
		}
	}

	/**
	 * Set JFileChooser mode
	 * @param fc
	 * @param selectMode
	 */
	public static void setMode(JFileChooser fc, ESelectFileMode selectMode)
	{
		DebugBox.predicateNotNull(fc);
		DebugBox.predicateNotNull(selectMode);

		int mode;
		switch(selectMode)
		{
			case FILE_AND_DIRECTORY:
			{
				mode = JFileChooser.FILES_AND_DIRECTORIES;
			} break;
			case DIRECTORIES_ONLY:
			{
				mode = JFileChooser.DIRECTORIES_ONLY;
			} break;
			case FILES_ONLY:
			case FILES_IN_PATHS_ONLY:
			{
				mode = JFileChooser.FILES_ONLY;
			} break;
			default:
			{
				assert false : "Unkown enum";
				mode = JFileChooser.FILES_AND_DIRECTORIES;
			}
		}

		fc.setFileSelectionMode(mode);
	}

	/**
	 * Disable autoscroll when appending text
	 * @param textArea
	 * @return True if could do the trick
	 */
	public static boolean disableAutoScroll(JTextComponent textArea)
	{
		Caret caret = textArea.getCaret();

		boolean ok = (caret instanceof DefaultCaret);
		if(ok)
		{
			DefaultCaret dc = (DefaultCaret)caret;
			dc.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
		}

		return ok;
	}

}
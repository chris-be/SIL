/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.link;

import javax.swing.AbstractButton;

import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGLabelModel;

/**
 * GLinker for AbstractButton
 *
 */
public class GLinkerTitleButton extends GLinkerComponent<AbstractButton, IGLabelModel> {

	/**
	 * 
	 * @param component
	 * @param model
	 */
	public GLinkerTitleButton(AbstractButton component, IGLabelModel model)
	{
		super(component, model);
	}

	/**
	 * 
	 * @param component
	 */
	public GLinkerTitleButton(AbstractButton component)
	{
		this(component, null);
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
			assert this.model == event.getModel();
			this.component.setText(this.model.getText());
		}
		else
		{
			this.component.setText(null);
		}
	}

}
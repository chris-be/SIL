/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.crt;

import javax.swing.ListModel;

/**
 * Contract for list model
 * 
 * @param <Item> @see ListModel
 */
public interface IGListModel<Item> extends IGModel, ListModel<Item> {

}
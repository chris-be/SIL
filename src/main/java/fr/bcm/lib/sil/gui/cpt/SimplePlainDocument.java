/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.gui.cpt;


import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import java.util.EventListener;

/**
 * Custom Document to avoid multiple event
 * <br>DocumentListener fire multiple event (remove, insert in case of update) which is annoying to handle.
 */
public class SimplePlainDocument extends PlainDocument {

	private static final long serialVersionUID = 7987116663829192897L;

	/**
	 * Contract for DocumentChanged events
	 */
	public interface DocumentChangedListener extends EventListener
	{
		/**
		 * Called when document has changed
		 */
		void documentChanged();
	}

	/**
	 * Flag to skip intermediate generated events
	 */
	private boolean ignoreEvents = false;

	/**
	 * 
	 * @param listener
	 */
	public SimplePlainDocument(DocumentChangedListener listener)
	{
		super();

		if(listener != null)
		{
			this.addDocumentChangedListener(listener);
		}
	}

	/**
	 * Add listener
	 * @param listener
	 */
	public void addDocumentChangedListener(DocumentChangedListener listener)
	{
		this.listenerList.add(DocumentChangedListener.class, listener);
	}

	/**
	 * Remove listener
	 * @param listener
	 */
	public void removeDocumentChangedListener(DocumentChangedListener listener)
	{
		this.listenerList.remove(DocumentChangedListener.class, listener);
	}

	@Override
	public void replace(int offset, int length, String text, AttributeSet attrs) throws BadLocationException
	{
		try
		{
			this.ignoreEvents = true;
			super.replace(offset, length, text, attrs);
		}
		finally
		{
			this.ignoreEvents = false;
		}

		this.fireDocumentChanged();
	}

	@Override
	public void remove(int offs, int len) throws BadLocationException
	{
		super.remove(offs, len);
		if (!ignoreEvents)
		{
			this.fireDocumentChanged();
		}
	}

	protected void fireDocumentChanged()
	{
		for(DocumentChangedListener l : this.listenerList.getListeners(DocumentChangedListener.class))
		{
			l.documentChanged();
		}
	}

}
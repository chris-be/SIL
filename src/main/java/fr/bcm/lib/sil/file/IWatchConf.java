/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.file;

import java.nio.file.Path;

/**
 * Contract to implement to handle watched files
 * @see Watcher
 */
public interface IWatchConf {

	/**
	 * Get watched path
	 */
	public Path getPath();

	/**
	 * Flag: watch sub directories
	 */
	public boolean isRecursiveMode();

	/**
	 * Permit to filter children 
	 * @param child
	 * @return True if child has to be returned by poll/take/...
	 */
	public boolean accept(Path child);

	/**
	 * Greate a new IWatchConf for sub directory
	 * @param childPath
	 * @return New conf
	 */
	public IWatchConf createChildConf(Path childPath);

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.file;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import fr.bcm.lib.sil.CVS_Stream;
import fr.bcm.lib.sil.DebugBox;

/**
 * Simplify store content of a stream into a string
 * @Convenience
 */
public class InputStream2String implements Closeable {

	/**
	 * InputStream to read
	 */
	protected InputStream input;

	/**
	 * Reader to handle "UTF-8"
	 */
	protected InputStreamReader reader;

	/**
	 * Buffer used
	 */
	protected char[] buffer;

	/**
	 * 
	 */
	protected StringBuilder sb;

	/**
	 * 
	 * @param input
	 * @throws UnsupportedEncodingException
	 */
	public InputStream2String(InputStream input) throws UnsupportedEncodingException
	{
		this(input, CVS_Stream.DEFAULT_BUFFER_SIZE);
	}

	/**
	 * 
	 * @param input
	 * @param readBufferSize
	 * @throws UnsupportedEncodingException
	 */
	public InputStream2String(InputStream input, int readBufferSize) throws UnsupportedEncodingException
	{
		DebugBox.predicateNotNull(input);
		DebugBox.predicate(readBufferSize > 0);

		this.input = input;
		this.reader = new InputStreamReader(input, "UTF-8");

		this.buffer = new char[readBufferSize];
		this.sb = new StringBuilder();
	}

	@Override
	public void close() throws IOException
	{
		if(this.reader != null)
		{
			this.reader.close();
			this.reader = null;
		}

		if(this.input != null)
		{
			this.input = null;
		}
	}

	/**
	 * Read what is available (non blocking)
	 * <br>WARNING ! Not all InputStream behaves correctly with the "available" method (ex: ZipFile.ZipFileInflaterInputStream).
	 * @return True if something was read
	 * @throws IOException
	 */
	public boolean readAvailable() throws IOException
	{
		if((this.input == null) || (this.reader == null))
		{
			throw new IOException();
		}

		int effRead = 0;

		int toRead = this.input.available();
		while(toRead > 0)
		{
			int r = this.reader.read(this.buffer);
			if (r <= 0)
			{	// -1: end of stream / 0: ??
				toRead = -1;
			}
			else
			{
				this.sb.append(this.buffer, 0, r);
				toRead-= r;
				effRead+= r;
			}
		}

		return effRead > 0;
	}

	/**
	 * Read all (blocking operation)
	 *
	 * @return True if something was read
	 * @throws IOException
	 */
	public boolean readAll() throws IOException
	{
		if((this.input == null) || (this.reader == null))
		{
			throw new IOException();
		}

		int effRead = 0;

		boolean goOn = true;
		while(goOn)
		{
			int r = this.reader.read(this.buffer);
			if (r <= 0)
			{	// -1: end of stream / 0: ??
				goOn = false;
			}
			else
			{
				this.sb.append(this.buffer, 0, r);
				effRead+= r;
			}
		}

		return effRead > 0;
	}

	/**
	 * Clear text
	 */
	public void clear()
	{
		this.sb.setLength(0);
	}

	/**
	 * 
	 * @return text
	 */
	public String getText()
	{
		return this.sb.toString();
	}

}
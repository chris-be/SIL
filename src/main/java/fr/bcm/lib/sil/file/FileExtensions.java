/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.file;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import fr.bcm.lib.sil.DebugBox;

/**
 * To work with file extensions
 */
public class FileExtensions implements Iterable<FileExtension> {

	protected ArrayList<FileExtension> fileExts;

	/**
	 * Ctor
	 */
	public FileExtensions()
	{
		this.fileExts = new ArrayList<>();
	}

	/**
	 * Add an extension
	 * @param fe
	 */
	public void addExtension(final FileExtension fe)
	{
		DebugBox.predicateNotNull(fe);
		this.fileExts.add(fe);
	}

	/**
	 * Add an extension
	 * @param ext
	 */
	public void addExtension(final String ext)
	{
		this.addExtension(new FileExtension(ext));
	}

	/**
	 * Add an extension
	 * @param ext
	 * @param desc
	 */
	public void addExtension(final String ext, final String desc)
	{
		this.addExtension(new FileExtension(ext, desc));
	}

	/**
	 * 
	 * @param fileName
	 * @param ignoreCase
	 * @return true if filename has one extension of this list
	 */
	public boolean hasExtension(final String fileName, boolean ignoreCase)
	{
		DebugBox.predicateNotNull(fileName);

		String ext = CVS_File.getExtension(fileName);
		for(FileExtension fe : this.fileExts)
		{
			if(fe.isExtension(ext, ignoreCase))
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * 
	 * @param file
	 * @param ignoreCase
	 * @return @see hasExtension
	 */
	public boolean hasExtension(final File file, boolean ignoreCase)
	{
		DebugBox.predicateNotNull(file);
		String fileName = file.getName();
		return this.hasExtension(fileName, ignoreCase);
	}

	@Override
	public Iterator<FileExtension> iterator()
	{
		return this.fileExts.iterator();
	}

}
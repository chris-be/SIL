/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.file;

import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.bcm.lib.sil.CVS_Collection;
import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.file.CVS_File;

/**
 * WatchConf with "file filter"
 * <br>Accept: no directory, files that match any expression or every file if no filter
 */
public class FileWatchConf extends WatchConf {

	/**
	 * Patterns
	 */
	protected ArrayList<Pattern> patterns;

	/**
	 * No filters
	 * @param path
	 * @param recursiveMode
	 */
	public FileWatchConf(final Path path, boolean recursiveMode)
	{
		super(path, recursiveMode);
		this.patterns = new ArrayList<>();
	}

	/**
	 * 
	 * @param path
	 * @param recursiveMode
	 * @param filters File names / can use * and ?
	 */
	public FileWatchConf(final Path path, boolean recursiveMode, Iterable<String> filters)
	{
		super(path, recursiveMode);
		DebugBox.predicateNotNull(filters);

		// Prepare match patterns
		String pfx = CVS_File.ensureSeparator(path.toString());
		pfx = Matcher.quoteReplacement(pfx);

		this.patterns = new ArrayList<>();
		for(String file : filters)
		{
			// Escape .
			String ndl, rpl;
			rpl = Matcher.quoteReplacement("\\.");
			file = file.replaceAll("\\.", rpl);
			// Convert *
			ndl = "\\*";
			rpl = Matcher.quoteReplacement(".*");
			file = file.replaceAll(ndl, rpl);
			//  Convert ?
			// ndl = Matcher.quoteReplacement("?");
			// file = file.replaceAll(ndl, "?");

			// Start with watched directory and must end with pattern
			file = pfx + file + "$";
			Pattern pattern = Pattern.compile(file, Pattern.CASE_INSENSITIVE);

			this.patterns.add(pattern);
		}
	}

	/**
	 * 
	 */
	public boolean accept(Path child)
	{
		if(Files.isDirectory(child, LinkOption.NOFOLLOW_LINKS))
		{
			return false;
		}

		if(this.patterns.isEmpty())
		{	// No patterns specified => accept everything
			return true;
		}

		String fileName = child.toString();
		for(Pattern pat : this.patterns)
		{
			if(pat.matcher(fileName).matches())
			{
				return true;
			}
		}

		return false;
	}

	@Override
	public IWatchConf createChildConf(Path childPath)
	{
		FileWatchConf copy = new FileWatchConf(childPath, this.recursiveMode);
		CVS_Collection.addAll(copy.patterns, this.patterns);

		return copy;
	}

}
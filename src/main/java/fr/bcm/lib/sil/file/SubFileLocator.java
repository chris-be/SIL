/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.file;

import java.io.File;

import fr.bcm.lib.sil.CVS_String;

/**
 * SubFileLocator : just to simplify handling files in a sub directory.
 * Typical usage : serve static files under a certain "root".
 */
public class SubFileLocator {

	protected File	root;

	public File getRoot()
	{
		return this.root;
	}

	/**
	 * Set root
	 * @param root if null default is "no path"
	 */
	public void setRoot(File root)
	{
		this.root = (root != null) ? root : new File("");
	}

	public SubFileLocator()
	{
		this((File)(null));
	}

	public SubFileLocator(File root)
	{
		this.setRoot(root);
	}

	public SubFileLocator(String root)
	{
		this.setRoot(root);
	}

	/**
	 * Set root
	 * @param rootName if null default is "no path"
	 */
	public void setRoot(String rootName)
	{
		File root = CVS_String.isNullOrEmpty(rootName) ? null : new File(rootName);
		this.setRoot(root);
	}

	/**
	 * Compute location regards to "root"
	 * @param resourceName
	 * @return File location
	 */
	public File cptFileLocation(String resourceName)
	{
		File file = new File(this.root, resourceName);

		return file;
	}

	/**
	 * Compute location regards to "root"
	 * @param resourceFile
	 * @return File location
	 */
	public File cptFileLocation(File resourceFile)
	{
		File file = new File(this.root, resourceFile.getPath());

		return file;
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.file;

import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;

import fr.bcm.lib.sil.DebugBox;

/**
 * Default implementation of IWatchConf
 * <br>Accept: no directory, all files
 */
public class WatchConf implements IWatchConf {

	protected Path path;

	protected boolean recursiveMode;

	/**
	 * @return path
	 */
	public Path getPath()
	{
		return this.path;
	}

	/**
	 * @return if recursive mode
	 */
	public boolean isRecursiveMode()
	{
		return this.recursiveMode;
	}

	/**
	 * 
	 * @param path
	 * @param recursiveMode
	 */
	public WatchConf(final Path path, boolean recursiveMode)
	{
		DebugBox.predicateNotNull(path);
		if(!Files.isDirectory(path))
		{
			throw new IllegalArgumentException();
		}

		this.path = path;
		this.recursiveMode = recursiveMode;
	}

	/**
	 * 
	 */
	public boolean accept(Path child)
	{
		return !Files.isDirectory(child, LinkOption.NOFOLLOW_LINKS);
	}

	@Override
	public IWatchConf createChildConf(Path childPath)
	{
		return new WatchConf(childPath, this.recursiveMode);
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.file;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import fr.bcm.lib.sil.DebugBox;

/**
 * Simplify "WatchService" usage (handle "recursive mode")
 */
public class Watcher {

	/**
	 * Watcher
	 */
	WatchService watchService;

	/**
	 * Watched dir
	 * <br/>Hash: WatchKey doesn't implement IComparable
	 */
	HashMap<WatchKey, IWatchConf> watchedDir;

	/**
	 * 
	 */
	public Watcher() throws IOException
	{
		this.watchService = FileSystems.getDefault().newWatchService();
		this.watchedDir = new HashMap<>();
	}

	/**
	 * 
	 * @return true if nothing is watched
	 */
	public boolean isEmpty()
	{
		return this.watchedDir.isEmpty();
	}

	/**
	 * Add default WatchConf
	 * @param path
	 * @param recursiveMode true: add watch to all sub directories too
	 * @throws IOException
	 * @Convenience
	 */
	public void addWatch(Path path, boolean recursiveMode) throws IOException
	{
		WatchConf wc = new WatchConf(path, recursiveMode);
		this.registerWatchConf(wc);
	}

	/**
	 * Add a WatchConf
	 * @param watchConf
	 * @throws IOException
	 */
	public void addWatch(IWatchConf watchConf) throws IOException
	{
		if(watchConf.isRecursiveMode())
		{
			this.addWatchTree(watchConf);
		}
		else
		{
			this.registerWatchConf(watchConf);
		}
	}

	/**
	 * Register a "watch conf" for root and all sub directories
	 * @param watchConf
	 * @throws IOException
	 */
	public void addWatchTree(IWatchConf watchConf) throws IOException
	{
		DebugBox.predicateNotNull(watchConf);

		Files.walkFileTree(watchConf.getPath(), new SimpleFileVisitor<Path>() {

			@Override
			public FileVisitResult preVisitDirectory(Path childPath, BasicFileAttributes attrs) throws IOException
			{
				IWatchConf watchChild = watchConf.createChildConf(childPath);
				Watcher.this.registerWatchConf(watchChild);

				return FileVisitResult.CONTINUE;
			}
		});
	}

	/**
	 * Internal method to add and register a "watch conf" (no recursive)
	 *
	 * @param watchConf 
	 * @throws IOException
	 */
	private void registerWatchConf(IWatchConf watchConf) throws IOException
	{
		DebugBox.predicateNotNull(watchConf);

		Path path = watchConf.getPath();
		WatchKey key = path.register(this.watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);

		this.watchedDir.put(key, watchConf);
	}

	/**
	 * 
	 * @param key WatchKey to treat
	 * @param changedFiles Set of files to update
	 * @return True if one or more files were added
	 * @throws IOException
	 */
	protected boolean handleKey(WatchKey key, Set<String> changedFiles) throws IOException
	{
		boolean isFile = false;

		IWatchConf wc = this.watchedDir.get(key);
		if(wc == null)
		{
			//
			return isFile;
		}

		for(WatchEvent<?> we : key.pollEvents())
		{
			WatchEvent.Kind<?> kind = we.kind();
			if(kind != StandardWatchEventKinds.OVERFLOW)
			{
				@SuppressWarnings("unchecked")
				WatchEvent<Path> wep = (WatchEvent<Path>)we;
				Path name = wep.context();
				Path child = wc.getPath().resolve(name);
				
				if(Files.isDirectory(child, LinkOption.NOFOLLOW_LINKS))
				{
					if((kind == StandardWatchEventKinds.ENTRY_CREATE) && wc.isRecursiveMode())
					{	// New directory: follow it because parent is in "recursive" mode
						IWatchConf childConf = wc.createChildConf(child);
						this.registerWatchConf(childConf);
					}
				}

				if(wc.accept(child))
				{
					isFile |= true;
					String fileName = child.toString();
					changedFiles.add(fileName);
				}
			}
		}

		if(!key.reset())
		{
			this.watchedDir.remove(key);
		}

		return isFile;
	}

	/**
	 * @see WatchService#poll
	 * @param changedFiles Set of files to update
	 * @throws IOException
	 */
	public boolean poll(Set<String> changedFiles) throws IOException
	{
		DebugBox.predicateNotNull(changedFiles);
		boolean polled = false;

		boolean goOn = true;
		while(goOn)
		{
			WatchKey key = this.watchService.poll();
			if(key != null)
			{
				polled |= this.handleKey(key, changedFiles);
			}
			else
			{
				goOn = false;
			}
		}

		return polled;
	}

	/**
	 * @see WatchService#poll
	 * @param changedFiles Set of files to update
	 * @throws IOException 
	 * @throws InterruptedException
	 */
	public boolean poll(final long timeout, final TimeUnit unit, Set<String> changedFiles) throws IOException, InterruptedException
	{
		DebugBox.predicateNotNull(changedFiles);
		boolean polled = false;

		boolean goOn = true;
		while(goOn)
		{
			WatchKey key = this.watchService.poll(timeout, unit);
			if(key != null)
			{
				polled |= this.handleKey(key, changedFiles);
				// Poll remaining
				polled |= this.poll(changedFiles);
			}
			else
			{
				goOn = false;
			}
		}

		return polled;
	}

	/**
	 * @see WatchService#take
	 * @param changedFiles Set of files to update
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean take(Set<String> changedFiles) throws IOException, InterruptedException
	{
		DebugBox.predicateNotNull(changedFiles);
		boolean polled = false;

		WatchKey key = this.watchService.take();
		assert key != null;
		polled |= this.handleKey(key, changedFiles);
		// Poll remaining
		polled |= this.poll(changedFiles);

		return polled;
	}

}
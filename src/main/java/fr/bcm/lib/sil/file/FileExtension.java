/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.file;

import fr.bcm.lib.sil.DebugBox;

/**
 * To ease use of file extension
 */
public class FileExtension {

	protected final String extension;
	protected final String description;

	/**
	 * Ctor
	 * @param ext
	 * @param desc
	 */
	public FileExtension(final String ext, final String desc)
	{
		DebugBox.predicateNotNull(ext);
		DebugBox.predicateNotNull(desc);

		this.extension = ext;
		this.description = desc;
	}

	/**
	 * Ctor
	 * @param ext
	 */
	public FileExtension(final String ext)
	{
		this(ext, ext + " file");
	}

	/**
	 * 
	 * @return extension
	 */
	public String getExtension()
	{
		return this.extension;
	}
	
	/**
	 * 
	 * @return description
	 */
	public String getDescription()
	{
		return this.description;
		
	}

	/**
	 * 
	 * @param ext tested extension
	 * @param ignoreCase
	 * @return true if tested extension is like this extension
	 */
	public boolean isExtension(final String ext, boolean ignoreCase)
	{
		DebugBox.predicateNotNull(ext);
		return ignoreCase ? this.extension.equalsIgnoreCase(ext) : this.extension.equals(ext);
	}

	/**
	 * 
	 * @param fileName
	 * @param ignoreCase
	 * @return true if filename has this extension
	 */
	public boolean hasExtension(final String fileName, boolean ignoreCase)
	{
		DebugBox.predicateNotNull(fileName);
		String ext = CVS_File.getExtension(fileName);
		return this.isExtension(ext, ignoreCase);
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.file;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import fr.bcm.lib.sil.CVS_String;
import fr.bcm.lib.sil.DebugBox;

/**
 * "Convenience" for file (get/add extension, ...)
 * 
 * @Convenience
 */
public class CVS_File {

	/**
	 * @return Extension of file (empty string if none)
	 */
	public static String getExtension(final String fileName)
	{
		DebugBox.predicateNotNull(fileName);
		int i = fileName.lastIndexOf('.');
		return (i != -1) ? fileName.substring(i+1) : "";
	}

	/**
	 * @return FileName with extension
	 */
	public static String addExtension(final String fileName, final String extension)
	{
		DebugBox.predicateNotNull(fileName);
		return CVS_String.isNullOrEmpty(extension) ? fileName : fileName + "." + extension;
	}

	/**
	 * Ensure that dirName ends with a separator
	 * @return dirName
	 */
	public static String ensureSeparator(final String dirName)
	{
		return (!dirName.endsWith(File.separator)) ? dirName + File.separator : dirName;
	}

//
// File
//

	/**
	 * Create URL for a local file
	 * @param fileName
	 * @throws MalformedURLException
	 */
	public static URL createLocalURL(File fileName) throws MalformedURLException
	{
		return fileName.toURI().toURL();
	}

	/**
	 * @return Extension of file (empty string if none)
	 */
	public static String getExtension(final File file)
	{
		DebugBox.predicateNotNull(file);
		String fileName = file.getName();
		return CVS_File.getExtension(fileName);
	}

	/**
	 * @return File with extension
	 */
	public static File addExtension(final File file, final String extension)
	{
		DebugBox.predicateNotNull(file);
		return new File(file.getParentFile(), CVS_File.addExtension(file.getName(), extension));
	}

	/**
	 * Delete file or directory
	 * @param file File or directory
	 */
	public static void delete(File file)
	{
		if(file.isFile())
		{
			file.delete();
		}
		else
		{	// Directory
			Path dir = Paths.get(file.getAbsolutePath());
			try(DirectoryStream<Path> ds = Files.newDirectoryStream(dir))
			{
	 			for(Path path : ds)
	 			{
	 				File subFile = path.toFile();
	 				CVS_File.delete(subFile);
	 			}
	 		}
			catch(IOException e)
			{

			}
		}
	}

}
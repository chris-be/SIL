/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.old2new;

import java.util.Enumeration;
import java.util.Iterator;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.patterns.IteratorWrapper;

/**
 * EnumerationIterator which cast from object to type / @see EnumerationIterator
 *
 * @Convenience
 */
public class CastedEnumerationIterator<T> implements Iterator<T> {

	/**
	 * Old enumeration still in use
	 */
	protected Enumeration<Object> enumeration;

	/**
	 * 
	 * @param enumeration
	 */
	public CastedEnumerationIterator(Enumeration<Object> enumeration)
	{
		DebugBox.predicateNotNull(enumeration);
		this.enumeration = enumeration;
	}

	@Override
	public boolean hasNext()
	{
		return this.enumeration.hasMoreElements();
	}

	@Override
	@SuppressWarnings("unchecked")
	public T next()
	{
		Object o = this.enumeration.nextElement();
		return (T)(o);
	}

	/**
	 * Create an iterable returning this iterator
	 * @Convenience
	 */
	public Iterable<T> createIterable()
	{
		return new IteratorWrapper<T>(this);
	}

}
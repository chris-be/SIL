/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.old2new;

import java.util.Enumeration;
import java.util.Iterator;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.patterns.IteratorWrapper;

/**
 * EnumerationIterator: let use an Iterator for old Enumeration objects.
 * 
 * @Convenience
 */
public class EnumerationIterator<T> implements Iterator<T> {

	/**
	 * Old enumeration still in use
	 */
	protected Enumeration<T> enumeration;

	/**
	 * 
	 * @param enumeration
	 */
	public EnumerationIterator(Enumeration<T> enumeration)
	{
		DebugBox.predicateNotNull(enumeration);
		this.enumeration = enumeration;
	}

	@Override
	public boolean hasNext()
	{
		return this.enumeration.hasMoreElements();
	}

	@Override
	public T next()
	{
		// Object o = this.enumeration.nextElement();
		// return (T)(o);
		return this.enumeration.nextElement();
	}

	/**
	 * Create an iterable returning this iterator
	 * @Convenience
	 */
	public Iterable<T> createIterable()
	{
		return new IteratorWrapper<T>(this);
	}

}
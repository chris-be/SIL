/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.old2new;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.bcm.lib.sil.DebugBox;

/**
 * To split a string "step by step"
 */
public class StringSplitIterator implements Iterator<String> {

	/**
	 * String to split
	 */
	final String toSplit;

	/**
	 * Index+1 of last character of toSplit
	 */
	final int endIndex;

	/**
	 * Regex
	 */
	protected Pattern pattern;

	/**
	 * Matcher
	 */
	protected Matcher matcher;

	/**
	 * Last index of last character of last returned token
	 */
	protected int lastIndex;

	/**
	 * Flag of last matcher.find()
	 */
	protected boolean matched;

	/**
	 * 
	 * @param toSplit String to split
	 * @param delimiter Regex to use as delimiter
	 * @param caseInsensitive All expression
	 */
	public StringSplitIterator(final String toSplit, String delimiter, boolean caseInsensitive)
	{
		DebugBox.predicateNotNull(toSplit);
		DebugBox.predicateNotNull(delimiter);

		this.toSplit	= toSplit;
		this.endIndex	= this.toSplit.length();
		this.pattern	= caseInsensitive ? Pattern.compile(delimiter, Pattern.CASE_INSENSITIVE) : Pattern.compile(delimiter);
		this.matcher	= this.pattern.matcher(toSplit);
		this.lastIndex	= 0;
	}

	@Override
	public boolean hasNext()
	{
		if(this.lastIndex >= this.endIndex)
		{
			return false;
		}

		this.matched = this.matcher.find();
		if(this.matched)
		{
			if(this.matcher.start() == 0)
			{	// toSplit begins with delimiter => skip it and test after
				this.lastIndex = this.matcher.end();
				return this.hasNext();
			}
/*
			if(this.matcher.end() >= this.endIndex)
			{	// toSplit ends with delimiter => skip it
				return false;
			}
*/
		}

		// Something remain
		return true;
	}

	@Override
	public String next()
	{
		int bgn = this.lastIndex;
		int end;

		if(this.matched)
		{
			end = this.matcher.start();
			this.lastIndex = this.matcher.end();
		}
		else
		{
			end = this.endIndex;
			this.lastIndex = end;
		}

		return toSplit.substring(bgn, end);
	}

}
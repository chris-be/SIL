/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.old2new;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * "Convenience" for Date (convert Java 8 to/from "old Date")
 *
 * @Convenience
 */
public class CVS_Date {

	/**
	 * 
	 * @param localDate
	 * @return Corresponding old date
	 */
	public static Date toDate(LocalDate localDate)
	{
		LocalDateTime ldt = localDate.atStartOfDay();
		return CVS_Date.toDate(ldt);
	}

	/**
	 * 
	 * @param localDateTime
	 * @return Corresponding old date
	 */
	public static Date toDate(LocalDateTime localDateTime)
	{
		ZonedDateTime zdt = localDateTime.atZone(ZoneId.systemDefault());
		return CVS_Date.toDate(zdt);
	}

	/**
	 * 
	 * @param zonedDateTime
	 * @return Corresponding old date
	 */
	public static Date toDate(ZonedDateTime zonedDateTime)
	{
		return Date.from(zonedDateTime.toInstant());
	}

	/**
	 * 
	 * @param date
	 * @return Corresponding Java 8 date
	 */
	public static ZonedDateTime toZonedDateTime(Date date)
	{
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault());
	}

	/**
	 * 
	 * @param date
	 * @return Corresponding Java 8 date
	 */
	public static LocalDate toLocalDate(Date date)
	{
		return CVS_Date.toZonedDateTime(date).toLocalDate();
	}

	/**
	 * 
	 * @param date
	 * @return Corresponding Java 8 date
	 */
	public static LocalDateTime toLocalDateTime(Date date)
	{
		return CVS_Date.toZonedDateTime(date).toLocalDateTime();
	}

}
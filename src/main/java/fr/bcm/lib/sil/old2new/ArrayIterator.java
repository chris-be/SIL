/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.old2new;

import java.util.Iterator;

/**
 * @Convenience
 */
public class ArrayIterator<Type> implements Iterator<Type> {

	protected Type[]	array;
	protected int		next;

	/**
	 * Ctor
	 * @param array
	 */
	public ArrayIterator(Type[] array)
	{
		this.array = array;
		this.next = 0;
	}

	@Override
	public boolean hasNext()
	{
		return (this.next < this.array.length);
	}

	@Override
	public Type next()
	{
		return this.array[this.next++];
	}

	/**
	 * Go directly to end (no more next)
	 */
	public void toEnd()
	{
		this.next = this.array.length;
	}

}
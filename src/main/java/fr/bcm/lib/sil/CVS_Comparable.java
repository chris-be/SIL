/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil;

import java.util.Iterator;

/**
 * "Convenience" for Comparable
 * @Convenience
 */
public class CVS_Comparable {

	/**
	 * Test if c1 and c2 are "equals"
	 * <br>considered same if both null
	 * <br>considered false if any of 2 is null
	 * <br>both not null: result of Object.equals
	 *
	 * @param c1
	 * @param c2
	 * @return True if c1 equals c2
	 */
	public static <T extends Object> boolean equals(final T c1, final T c2)
	{
		if(c1 == null)
		{
			return (c2 == null) ? true : false;
		}

		return (c2 == null) ? false : c1.equals(c2);
	}

	/**
	 * 
	 * @param <T>
	 */
	public class ComparableCouple<T extends Comparable<T>>
	{
		protected final T left;
		protected final T right;

		ComparableCouple(final T left, final T right)
		{
			this.left = left;
			this.right = right;
		}

		int compare()
		{
			return CVS_Comparable.compare(this.left, this.right);
		}
	}

	/**
	 * Compare c1 and c2
	 * <br>considered same if both null
	 * <br>considered inferior any of 2 which is null
	 * <br>both not null: result of Comparable.compareTo 
	 *
	 * @param c1
	 * @param c2
	 * @return @see Comparable#compareTo 
	 */
	public static <T extends Comparable<T>> int compare(final T c1, final T c2)
	{
		if(c1 == null)
		{
			return (c2 == null) ? 0 : -1;
		}

		return (c2 == null) ? +1 : c1.compareTo(c2);
	}

	/**
	 * Compare couple one by one
	 * @param couples
	 * @return @see Comparable#compareTo 
	 */
	public static int lexicographicalCompare(ComparableCouple<?>... couples)
	{
		int cmp;
		for(ComparableCouple<?> couple : couples)
		{
			cmp = couple.compare();
			if(cmp != 0)
			{
				return cmp;
			}
		}

		return 0;
	}

	/**
	 * Compare one by one in order and stop as soon as a difference is detected.
	 * If there is no difference but one iter has more element, then this iter is "greater"
	 * <br>!! Doesn't handle "null" values
	 * @param iter1
	 * @param iter2
	 * @return @see Comparable#compareTo
	 */
	public static <T extends Comparable<T>> int lexicographicalCompare(Iterator<T> iter1, Iterator<T> iter2)
	{
		DebugBox.predicateNotNull(iter1);
		DebugBox.predicateNotNull(iter2);

		int cmp;
		while(iter1.hasNext())
		{
			if(!iter2.hasNext())
			{	// iter1 has more elements than iter2
				return -1;
			}

			// cmp = lexicographicalCompare(iter1.next(), iter2.next()) handle null ??
			cmp = iter1.next().compareTo(iter2.next());
			if(cmp != 0)
			{
				return cmp;
			}
		}

		// iter2 has more elements than iter1 ?
		return iter2.hasNext() ? 1 : 0;
	}

	/**
	 * @Convenience
	 * @param i1
	 * @param i2
	 * @return Same as lexicographicalCompare with iterators
	 */
	public static <T extends Comparable<T>> int lexicographicalCompare(final Iterable<T> i1, final Iterable<T> i2)
	{
		DebugBox.predicateNotNull(i1);
		DebugBox.predicateNotNull(i2);
		return CVS_Comparable.lexicographicalCompare(i1.iterator(), i2.iterator());
	}

}
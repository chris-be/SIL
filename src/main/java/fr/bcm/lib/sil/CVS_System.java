/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil;

/**
 * "Convenience" for System
 * 
 * @Convenience
 */
public class CVS_System {

	/**
	 * @return Paths defined in environment
	 */
	public static Iterable<String> getUserPaths()
	{
		String path = System.getenv("PATH");
		String separator = System.getProperty("path.separator");

		return CVS_String.splitWithCharacters(path, separator);
	}

}
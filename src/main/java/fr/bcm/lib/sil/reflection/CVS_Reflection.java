/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.reflection;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import fr.bcm.lib.sil.DebugBox;

/**
 * "Convenience" for Reflection
 * 
 * @Convenience
 */
public class CVS_Reflection {

	/**
	 * Get class from name
	 * 
	 * @param canonicalClassName
	 * @return Class
	 * @throws ClassNotFoundException
	 */
	public static Class<?> getClass(String canonicalClassName) throws ClassNotFoundException
	{
		DebugBox.predicateNotNull(canonicalClassName);
		return Class.forName(canonicalClassName);
	}

	/**
	 * Get annotations from a class by it's name
	 *
	 * @param canonicalClassName
	 * @return Annotations
	 * @throws ClassNotFoundException
	 */
	public static Annotation[] getClassAnnotations(String canonicalClassName) throws ClassNotFoundException
	{
		Class<?> c = Class.forName(canonicalClassName);
		return c.getAnnotations();
	}

	/**
	 * Get typed annotations from a class by it's name
	 *
	 * @param canonicalClassName
	 * @param classA
	 * @return Annotations of type A
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public static <A extends Annotation> List<A> getClassAnnotations(String canonicalClassName, final Class<A> classA) throws ClassNotFoundException
	{
		Class<?> c = Class.forName(canonicalClassName);

		ArrayList<A> list = new ArrayList<A>();
		for(Annotation test : c.getAnnotations())
		{
			if(test.annotationType() == classA)
			{
				A a = (A)(test);
				list.add(a);
			}
		}

		return list;
	}

	/**
	 * Get default value of an annotation
	 *
	 * @param annotationClass
	 * @param paramName
	 * @return Default value
	 * @throws NoSuchMethodException
	 */
	public static Object getAnnotationDefaultValue(final Class<? extends Annotation> annotationClass, String paramName) throws NoSuchMethodException
	{
		return annotationClass.getMethod(paramName).getDefaultValue();
	}

	/**
	 * Get default value of an annotation
	 *
	 * @param type
	 * @param annotationClass
	 * @param paramName
	 * @return Default value
	 */
	public static <Type> Type getAnnotationDefaultValue(final Class<Type> type, final Class<? extends Annotation> annotationClass, String paramName)
	{
		Type value = null;

		try
		{
			Object o = CVS_Reflection.getAnnotationDefaultValue(annotationClass, paramName);
			value = type.cast(o);
		} catch(NoSuchMethodException e)
		{	// TODO
			e.printStackTrace();
		}

		return value;
	}

	/**
	 * Use "c" to create instance and return it only if "instanceof" Type
	 * 
	 * @param c
	 * @return An instance of Type
	 */
	public static <Type> Type createInstance(final Class<Type> c)
	{
		if(c == null)
		{
			return null;
		}

		try
		{
			return c.newInstance();
		}
		catch(Exception e)
		{

		}

		return null;
	}

	/**
	 * Create instance from name and cast it to type
	 * @param canonicalClassName
	 * @param type
	 * @throws IllegalArgumentException Can't create instance nor cast
	 */
	@SuppressWarnings("unchecked")
	public static <T> T createInstance(String canonicalClassName, final Class<T> type) throws IllegalArgumentException
	{
		try
		{
			Class<?> cl = CVS_Reflection.getClass(canonicalClassName);
			Class<T> ct = (Class<T>)(cl);

			return CVS_Reflection.createInstance(ct);
		}
		catch(ClassNotFoundException cnfe)
		{
			throw new IllegalArgumentException();
		}
		catch(ClassCastException cce)
		{
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Get caller class. Not efficient, should be used sparingly !
	 *
	 * @return Class
	 */
	public static Class<?> getCallerClass()
	{
		Class<?>	cl;

		try
		{
			StackTraceElement[] stack = Thread.currentThread().getStackTrace();
			// 0: Thread.getStackTrace()
			// 1: CVS_Reflection.getCallerClass()
			// 2: Class.method() calling "CVS_Reflection.getCallerClass()"
			// 3: Class.method() caller !!
			final int idx = 3;
			if(idx >= stack.length)
			{	// Shouldn't be
				// length = 0 : possible "under some circumstances" @see Thread.getStackTrace
				throw new RuntimeException();
			}

			String className = stack[idx].getClassName();
			cl = Class.forName(className);
		}
		catch(Exception e)
		{	// SecurityException ?
			cl = null;
		}

		return cl;
	}

}
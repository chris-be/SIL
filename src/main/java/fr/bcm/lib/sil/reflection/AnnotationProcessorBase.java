/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.reflection;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic.Kind;

import fr.bcm.lib.sil.CVS_Jar;

/**
 * Class embedding some useful methods when writing an annotation processor. Simply inherit from it instead of AbstractProcessor (if possible).
 *
 * @Convenience
 */
public abstract class AnnotationProcessorBase extends AbstractProcessor {

	protected static final String	ONLY_FOR_CLASS_MSG = "Bad use of [%s]: only appliable on class.";

	/**
	 * Find package of processed element
	 * 
	 * @param element
	 * @return PackageElement of Element
	 */
	protected PackageElement findPackage(Element element)
	{
		Elements utils = this.processingEnv.getElementUtils();
		return utils.getPackageOf(element);
	}

	/**
	 * Get canonical name of element (package name + name)
	 * @param element
	 * @return Canonical name of element (package + element) 
	 */
	protected String getCanonicalName(Element element)
	{
		PackageElement pck = this.findPackage(element);
		String name = element.getSimpleName().toString();
		if(!pck.isUnnamed())
		{	// Prefix with package
			name = pck.getQualifiedName().toString() + CVS_Jar.PACKAGE_SEPARATOR + name;
		}

		return name;
	}

	/**
	 * Returns only annotated classes.
	 * Put an error if found on another element than CLASS
	 * @param roundEnv
	 * @param a
	 * @return A set of classes annotated with 'a'
	 */
	protected Set<? extends Element> findClasses(RoundEnvironment roundEnv, Class<? extends Annotation> a)
	{
		// Eclipse JDT 'roundEnv.getElementsAnnotatedWith' returns an "immutable set"
		// => create results instead of removing bad elements from original set before returning it
		Set<Element> els = new HashSet<Element>();

		String badUseMsg = String.format(AnnotationProcessorBase.ONLY_FOR_CLASS_MSG, a.getCanonicalName());
		for(Element el : roundEnv.getElementsAnnotatedWith(a))
		{	// Clean and print errors
			if(el.getKind() == ElementKind.CLASS)
			{
				els.add(el);
			}
			else
			{
				this.printError(el, badUseMsg);
			}
		}

		return els;
	}

	/**
	 * Print a message
	 * @param kind
	 * @param element
	 * @param msg
	 */
	protected void message(Kind kind, Element element, String msg)
	{
		Messager messenger = this.processingEnv.getMessager();
		messenger.printMessage(kind, msg, element);
	}

	/**
	 * Print a message
	 * @param kind
	 * @param element
	 * @param msg
	 * @param args
	 */
	protected void message(Kind kind, Element element, String msg, Object... args)
	{
		Messager messenger = this.processingEnv.getMessager();
		messenger.printMessage(kind, String.format(msg, args), element);
	}

	/**
	 * Print a message
	 * @param kind
	 * @param msg
	 * @param args
	 */
	protected void message(Kind kind, String msg, Object... args)
	{
		Messager messenger = this.processingEnv.getMessager();
		messenger.printMessage(kind, String.format(msg, args));
	}

	/**
	 * Print an error
	 * @param element
	 * @param msg
	 * @param args
	 */
	protected void printError(Element element, String msg, Object... args)
	{
		this.message(Kind.ERROR, element, msg, args);
	}

	/**
	 * Print a warning
	 * @param element
	 * @param msg
	 * @param args
	 */
	protected void printMandatoryWarning(Element element, String msg, Object... args)
	{
		this.message(Kind.MANDATORY_WARNING, element, msg, args);
	}

	/**
	 * Print a note
	 * @param element
	 * @param msg
	 * @param args
	 */
	protected void printNote(Element element, String msg, Object... args)
	{
		this.message(Kind.NOTE, element, msg, args);
	}

	/**
	 * Print an error
	 * @param msg
	 * @param args
	 */
	protected void printError(String msg, Object... args)
	{
		this.message(Kind.ERROR, msg, args);
	}

	/**
	 * Print a warning
	 * @param msg
	 * @param args
	 */
	protected void printMandatoryWarning(String msg, Object... args)
	{
		this.message(Kind.MANDATORY_WARNING, msg, args);
	}

	/**
	 * Print a note
	 * @param msg
	 * @param args
	 */
	protected void printNote(String msg, Object... args)
	{
		this.message(Kind.NOTE, msg, args);
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.string;

import java.util.Collection;

/**
 * Default implementation - simply returns predefined string
 */
public class StringCreator implements IStringCreator {

	protected String text;

	public StringCreator(String text)
	{
		this.text = text;
	}

	@Override
	public String createString()
	{
		return this.text;
	}

	/**
	 * Iterate through generators and concatenate generated strings
	 * @param generators
	 */
	static public String createWith(Collection<? extends IStringCreator> generators)
	{
		StringBuilder sb = new StringBuilder();
		for(IStringCreator gen : generators)
		{
			String toAppend = gen.createString();
			if(toAppend != null)
			{
				sb.append(toAppend);
			}
		}

		return sb.toString();
	}

}
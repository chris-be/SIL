/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.string;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

import fr.bcm.lib.sil.CVS_String;

/**
 * Parse a string to create a list of StringCreator
 */
public class ParserStringCreator {

	/**
	 * Char to detect key
	 */
	protected char startToken;
	/**
	 * Char to detect end of key
	 */
	protected char stopToken;
	/**
	 * Text to use when empty key
	 */
	protected String emptyKeyText;

	protected TreeMap<String, IStringCreator>	keywords;

	public ParserStringCreator()
	{
		this('%', '%', "%");
	}

	public ParserStringCreator(char startToken, char stopToken, String emptyKeyText)
	{
		this.keywords = new TreeMap<>();
		this.startToken = startToken;
		this.stopToken = stopToken;
		this.emptyKeyText = emptyKeyText;
	}

	public void setKeyword(String keyword, IStringCreator functor)
	{
		this.keywords.put(keyword, functor);
	}

	public void removeKeyword(String keyword)
	{
		this.keywords.remove(keyword);
	}

	protected enum ParseMode {
		  FIND_OPEN_TOKEN
		, FIND_CLOSE_TOKEN
	};

	protected IStringCreator forEmptyKeyword()
	{
		return new StringCreator(this.emptyKeyText);
	}

	protected IStringCreator forUnknownKeyword(String keyword)
	{
		return new StringCreator(this.startToken + keyword + this.stopToken);
	}

	/**
	 * Scan for startToken key stopToken (ex: %key%)
	 * @param pattern Pattern to parse
	 */
	public ArrayList<IStringCreator> parse(String pattern)
	{
		ArrayList<IStringCreator>	creatorList = new ArrayList<>();

		if(CVS_String.isNullOrEmpty(pattern))
		{
			return creatorList;
		}

		ParseMode mode = ParseMode.FIND_OPEN_TOKEN;
		StringBuilder sb = new StringBuilder();

		Iterator<Integer> iter = pattern.codePoints().iterator();
		while(iter.hasNext())
		{
			Integer c = iter.next();
			char[] chars = Character.toChars(c);

			switch(mode)
			{
				case FIND_OPEN_TOKEN:
				{
					if(chars[0] == this.startToken)
					{
						if(sb.length() > 0)
						{
							creatorList.add(new StringCreator(sb.toString()));
							sb.setLength(0);
						}

						mode = ParseMode.FIND_CLOSE_TOKEN;
					}
					else
					{
						sb.append(chars);
					}
				} break;
				case FIND_CLOSE_TOKEN:
				{
					if(chars[0] == this.stopToken)
					{
						IStringCreator creator;
						if(sb.length() == 0)
						{	// Empty key special case
							creator = this.forEmptyKeyword();
						}
						else
						{
							String kwd = sb.toString();
							sb.setLength(0);

							creator = this.keywords.get(kwd);
							if(creator == null)
							{	// Keep text ?
								creator = this.forUnknownKeyword(kwd);
							}
						}

						if(creator != null)
						{
							creatorList.add(creator);
						}
						mode = ParseMode.FIND_OPEN_TOKEN;
					}
					else
					{
						sb.append(chars);
					}
				} break;
			}
		}

		if(mode == ParseMode.FIND_CLOSE_TOKEN)
		{
			throw new IllegalArgumentException("Malformed pattern");
		}

		if(sb.length() > 0)
		{
			creatorList.add(new StringCreator(sb.toString()));
		}

		return creatorList;
	}

}
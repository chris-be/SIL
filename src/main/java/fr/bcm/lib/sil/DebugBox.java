/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil;

import fr.bcm.lib.sil.reflection.CVS_Reflection;

/**
 * Debug tools
 *
 * @Convenience
 */
public class DebugBox {

	/**
	 * Compile in "debug mode"
	 */
	public static boolean DEBUG_MODE = !false;

	/**
	 * Call assert false with formatted message
	 * @param caller
	 * @param detail
	 */
	private static void _assert(final Class<?> caller, final String detail)
	{
		String msg = String.format("Error predicate [%s] : %s", caller.getCanonicalName(), detail);
		assert false : msg;
	}

	/**
	 * Check condition
	 * @param condition
	 * @throws IllegalArgumentException
	 */
	public static void predicate(boolean condition) throws IllegalArgumentException
	{
		DebugBox.predicate(condition, "undefined");
	}

	/**
	 * Check condition
	 * @param condition
	 * @param detail
	 * @throws IllegalArgumentException
	 */
	public static void predicate(boolean condition, final String detail) throws IllegalArgumentException
	{
		if(condition)
		{	// ok
			return;
		}

		if(DebugBox.DEBUG_MODE)
		{
			Class<?> caller = CVS_Reflection.getCallerClass();
			DebugBox._assert(caller, detail);
		}
		throw new IllegalArgumentException(detail);
	}

	/**
	 * Check if object is not null
	 * @param o
	 * @throws IllegalArgumentException
	 */
	public static void predicateNotNull(final Object o) throws IllegalArgumentException
	{
		DebugBox.predicateNotNull(o, "null");
	}

	/**
	 * Check if object is not null
	 * @param o
	 * @throws IllegalArgumentException
	 */
	public static void predicateNotNull(final Object o, final String detail) throws IllegalArgumentException
	{
		if(o != null)
		{	// ok
			return;
		}

		if(DebugBox.DEBUG_MODE)
		{
			Class<?> caller = CVS_Reflection.getCallerClass();
			DebugBox._assert(caller, detail);
		}
		// Objects.requireNonNull();
		throw new IllegalArgumentException(detail);
	}

	/**
	 * Can be used in "default case"
	 * @param e
	 * @throws RuntimeException
	 */
	public static void unhandledEnum(final Object e) throws RuntimeException
	{
		String name = e.getClass().getCanonicalName();
		String value = e.toString();
		throw new RuntimeException("Unhandled enum [" + name + "] " + value);
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil;

/**
 * "Convenience" forJava Web Start Service
 *
 * @Convenience
 */
public class CVS_JNLP {

	/**
	 * @return true if Java Web Start is running
	 */
	public static boolean isRunning()
	{
		return System.getProperty("javawebstart.version", null) != null;
	}

}
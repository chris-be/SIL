/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

/**
 * SImple Library
 *
 * @see fr.bcm.lib.sil.Defines
 */
package fr.bcm.lib.sil;

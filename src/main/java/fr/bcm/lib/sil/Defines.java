/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil;

/**
 * Some defines
 */
public class Defines {

	/**
	 * Lib version
	 */
	public static final String VERSION = "0.1.0a";

	/**
	 * If any refactor...
	 */
	public static final String PACKAGE_I18N = "fr.bcm.lib.sil.i18n";

}
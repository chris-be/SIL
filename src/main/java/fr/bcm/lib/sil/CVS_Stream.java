/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * "Convenience" for Stream
 *
 * @Convenience
 */
public class CVS_Stream {

	/**
	 * Default size of buffer used for read/write
	 */
	public static final int DEFAULT_BUFFER_SIZE = 1024;

	/**
	 * Create a BufferedReader from an InputStream
	 */
	public static BufferedReader toBufferedReader(InputStream is) throws IllegalArgumentException
	{
		DebugBox.predicateNotNull(is);

		InputStreamReader r = new InputStreamReader(is);
		return new BufferedReader(r);
	}

	/**
	 * Read all "is" and write it to "os"
	 * @param os
	 * @param is
	 * @param bufferSize Size of buffer to use
	 * @throws IOException
	 */
	public static void forward(OutputStream os, InputStream is, int bufferSize) throws IOException
	{
		DebugBox.predicateNotNull(os);
		DebugBox.predicateNotNull(is);
		DebugBox.predicate(bufferSize > 0);

		byte[] buffer = new byte[bufferSize];
		boolean goOn = (is.available() > 0);
		while(goOn)
		{
			int r = is.read(buffer);
			if(r < 0)
			{
				goOn = false;
			}
			else
			{
				os.write(buffer ,0, r);
			}
		}
	}

	/**
	 * @see #forward(OutputStream os, InputStream is, int bufferSize)
	 * @param os
	 * @param is
	 */
	public static void forward(OutputStream os, InputStream is) throws IOException
	{
		CVS_Stream.forward(os, is, DEFAULT_BUFFER_SIZE);
	}

	/**
	 * Try to close (catch exception)
	 * 
	 * @param toClose
	 * @return false if any exception was caught
	 */
	public static boolean closeSilently(Closeable toClose)
	{
		try
		{
			if(toClose != null)
			{
				toClose.close();
			}
		}
		catch(IOException e)
		{
			return false;
		}

		return true;
	}

}
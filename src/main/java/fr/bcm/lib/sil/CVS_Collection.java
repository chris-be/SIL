/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * "Convenience" for Collection
 *
 * @Convenience
 */
public class CVS_Collection {

	/**
	 * Return if collection is null or empty
	 * @param col
	 * @return true if list is null or empty
	 */
	public static boolean isNullOrEmpty(final Collection<?> col)
	{
		return (col != null) ? col.isEmpty() : true;
	}

	/**
	 * Return last element of list
	 * @param list
	 * @return null if list is null or empty
	 */
	public static <T> T last(final List<T> list)
	{
		int li = (list != null) ? list.size()-1 : -1;
		return (li >= 0) ? list.get(li) : null;
	}

	/**
	 * Create an array with collection's elements
	 * @param cl Type wanted
	 * @param col
	 * @return Empty if col is null or empty
	 */
	public static <T> T[] createArray(final Class<T> cl, final Collection<T> col)
	{
		int size;
		Iterator<T> iter;
		if(col == null)
		{
			size = 0;
			iter = Collections.emptyIterator();
		}
		else
		{
			size = col.size();
			iter = col.iterator();
		}

		@SuppressWarnings("unchecked")
		T[] array = (T[]) Array.newInstance(cl, size);
		for(int i = 0 ; i < size ; ++i)
		{
			array[i] = iter.next();
		}

		return array;
	}

	/**
	 *
	 */
	public static <T> void addAll(Collection<T> dst, Iterator<? extends T> iter)
	{
		DebugBox.predicateNotNull(dst);
		DebugBox.predicateNotNull(iter);

		while(iter.hasNext())
		{
			dst.add(iter.next());
		}
	}

	/**
	 *
	 */
	public static <T> void addAll(Collection<T> dst, final Iterable<? extends T> iterable)
	{
		DebugBox.predicateNotNull(iterable);
		CVS_Collection.addAll(dst, iterable.iterator());
	}

	/**
	 *
	 */
	public static <T, E extends T> void addAll(Collection<T> dst, final E[] src)
	{
		DebugBox.predicateNotNull(dst);
		DebugBox.predicateNotNull(src);

		for(T toAdd : src)
		{
			dst.add(toAdd);
		}
	}



	/**
	 *
	 */
	public static <T> void replaceAll(Collection<T> dst, final Iterable<? extends T> src)
	{
		DebugBox.predicateNotNull(dst);

		dst.clear();
		CVS_Collection.addAll(dst, src);
	}

	/**
	 *
	 */
	public static <T, E extends T> void replaceAll(Collection<T> dst, final E[] src)
	{
		DebugBox.predicateNotNull(dst);

		dst.clear();
		CVS_Collection.addAll(dst, src);
	}

}
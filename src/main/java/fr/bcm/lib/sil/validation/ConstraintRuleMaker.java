/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.validation;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.Defines;
import fr.bcm.lib.sil.i18n.Jar_i18n;

/**
 * Default service for "getRule" translations
 */
public class ConstraintRuleMaker {

	protected Jar_i18n	i18n;

	/**
	 * Ctor
	 * @throws IOException
	 */
	public ConstraintRuleMaker() throws IOException
	{
		this.i18n = new Jar_i18n(Defines.PACKAGE_I18N);
	}

	/**
	 * Create rule message for constraint: all conditions
	 * <br>With some HID, only one error is shown at a time, it's quite annoying.
	 * <br>So here is the "complete contract" for value.
	 * @param constraint
	 * @param locale
	 * @return All conditions for value
	 */
	public String createRule(IConstraint<?> constraint, final Locale locale)
	{
		DebugBox.predicateNotNull(locale);
		ResourceBundle rb = this.i18n.getResourceBundle("validation", locale);

		return this.getString(constraint, rb);
	}

	/**
	 * 
	 * @param constraint
	 * @param rb
	 */
	protected String getString(IConstraint<?> constraint, ResourceBundle rb)
	{
		EConstraintType type = constraint.getConstraintType();
		String format = rb.getString(type.getTranslateKey());

		if(constraint instanceof IBinaryConstraint<?>)
		{
			IBinaryConstraint<?> ibc = (IBinaryConstraint<?>)(constraint);
			String l = this.getString(ibc.getLeft(), rb);
			String r = this.getString(ibc.getRight(), rb);

			return String.format(format, l, r);
		}

		if(constraint instanceof IConstraintOneParam<?, ?>)
		{
			IConstraintOneParam<?, ?> iop = (IConstraintOneParam<?, ?>)(constraint);
			return String.format(format, iop.getP1());
		}

		return format;
	}

}
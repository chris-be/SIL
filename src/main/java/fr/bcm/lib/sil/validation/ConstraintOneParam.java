/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.validation;

import fr.bcm.lib.sil.DebugBox;

/**
 * Constraint with one parameter default implementation
 * 
 * @param <Type> @see IConstraintOneParam
 * @param <Param> @see IConstraintOneParam
 */
public class ConstraintOneParam<Type, Param> extends Constraint<Type> implements IConstraintOneParam<Type, Param> {

	protected final Param param;

	@Override
	public EConstraintType getConstraintType()
	{
		return this.constraintType;
	}

	@Override
	public Param getP1()
	{
		return this.param;
	}

	/**
	 * Ctor
	 * @param type
	 * @param creator
	 * @param param
	 */
	public ConstraintOneParam(final EConstraintType type, final ITestExpressionOneParamCreator<Type, Param> creator, Param param)
	{
		super(type);
		DebugBox.predicateNotNull(creator);

		this.param = param;
		this.test = creator.createTestExpression(this.value, this.param);
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.validation;

/**
 * Contract for constraints
 * 
 * @param <Type> Value type that is constrained
 */
public interface IConstraint<Type> {

	/**
	 * @return Type of constraint
	 */
	public EConstraintType getConstraintType();

	/**
	 * Check constraint
	 * @param value Value to test
	 * @return True if value respects constraint
	 */
	public Boolean check(final Type value);

}
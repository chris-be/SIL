/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.validation;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.i18n.ITranslateKey;

/**
 * Type of constraint
 */
public enum EConstraintType implements ITranslateKey {

// Unary

	  NOT_NULL			("constraint.not.null")
	, NOT_EMPTY			("constraint.not.empty")
	, REQUIRED			("constraint.required")

	// String
	, STRING_MAX_SIZE	("constraint.string.size.max")	// format: %i

// Binary

	, BINARY_AND		("constraint.binary.and")	// format: %s %s
	, BINARY_OR			("constraint.binary.or")	// format: %s %s

	;

	private final String translateKey;

	@Override
	public String getTranslateKey()
	{
		return this.translateKey;
	}

	private EConstraintType(String translateKey)
	{
		DebugBox.predicateNotNull(translateKey);
		this.translateKey = translateKey;
	}

}
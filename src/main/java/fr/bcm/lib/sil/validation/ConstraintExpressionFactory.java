/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.validation;

import fr.bcm.lib.sil.math.expr.crt.IExpression;
import fr.bcm.lib.sil.math.expr.test.ITestExpression;
import fr.bcm.lib.sil.math.expr.test.NotOperator;
import fr.bcm.lib.sil.math.expr.test.TestEqual;
import fr.bcm.lib.sil.math.expr.test.TestIsNull;
import fr.bcm.lib.sil.math.expr.test.TestLessEqual;
import fr.bcm.lib.sil.math.expr.value.ExpressionValue;
import fr.bcm.lib.sil.math.expr.value.StringSize;

/**
 * Simple factory
 */
public class ConstraintExpressionFactory {

	/**
	 * 
	 * @param value
	 * @return expression verifying if value is not null
	 */
	public static <Type> ITestExpression createNotNull(IExpression<Type> value)
	{
		return new NotOperator( new TestIsNull<Type>(value) );
	}

	/**
	 * 
	 * @param value
	 * @return expression verifying if string is empty
	 */
	public static ITestExpression createStringIsEmpty(IExpression<String> value)
	{
		return new TestEqual<Integer>(new StringSize(value), new ExpressionValue<Integer>(0));
	}

	/**
	 * 
	 * @param value
	 * @return expression verifying if string is not empty
	 */
	public static ITestExpression createStringNotEmpty(IExpression<String> value)
	{
		return new NotOperator( ConstraintExpressionFactory.createStringIsEmpty(value) );
	}

	/**
	 * 
	 * @param value
	 * @param max
	 * @return expression verifying if string length is {@code <= } max given
	 */
	public static ITestExpression createStringMaxSize(IExpression<String> value, int max)
	{
		return new TestLessEqual<Integer>(new StringSize(value), new ExpressionValue<Integer>(max));
	}

}
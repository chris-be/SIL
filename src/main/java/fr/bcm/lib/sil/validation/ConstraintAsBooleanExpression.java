/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/
package fr.bcm.lib.sil.validation;

import fr.bcm.lib.sil.math.expr.crt.EExpressionType;
import fr.bcm.lib.sil.math.expr.crt.IExpression;

/**
 * Constraint wrapper to check rule at demand
 *
 * @param <Type>
 */
public class ConstraintAsBooleanExpression<Type> implements IExpression<Boolean> {

	protected IConstraint<Type> constraint;
	protected Type value;

	IConstraint<Type> getConstraint()
	{
		return this.constraint;
	}

	/**
	 * 
	 * @param value
	 */
	public void setValue(final Type value)
	{
		this.value = value;
	}

	/**
	 * Ctor
	 * @param constraint
	 */
	public ConstraintAsBooleanExpression(IConstraint<Type> constraint)
	{
		this.constraint = constraint;
	}

	@Override
	public Boolean eval()
	{
		return constraint.check(this.value);
	}

	@Override
	public EExpressionType getExpressionType()
	{
		return EExpressionType.TEST_UNARY;
	}

	@Override
	public String createRepresentation()
	{
		return "eval constraint()";
	}

}
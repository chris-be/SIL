/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.validation;

/**
 * Contract for constraints
 * 
 * @param <Type> Type of value to constraint
 * @param <Param> Type of parameter to sue
 */
public interface IConstraintOneParam<Type, Param> extends IConstraint<Type> {

	/**
	 * @return Parameter
	 */
	public Param getP1();

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.validation;

/**
 * Contract for constraints
 * 
 * @param <Type> @see IConstraint
 */
public interface IBinaryConstraint<Type> extends IConstraint<Type> {

	/**
	 * @return Left constraint
	 */
	public IConstraint<Type> getLeft();

	/**
	 * @return Right constraint
	 */
	public IConstraint<Type> getRight();

}
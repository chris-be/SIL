/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.validation;

import fr.bcm.lib.sil.math.expr.crt.IExpression;
import fr.bcm.lib.sil.math.expr.test.ITestExpression;

/**
 * Contract to create test expression "at demand"
 */
public interface ITestExpressionCreator<Type> {

	/**
	 * @param value Value to test
	 * @return TestExpression for value
	 */
	public ITestExpression createTestExpression(final IExpression<Type> value);

}
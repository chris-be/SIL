/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.validation;

/**
 * Simple factory
 */
public class ConstraintFactory {

	/**
	 * 
	 * @return constraint "value is not null"
	 */
	public static <Type> IConstraint<Type> createNotNull()
	{
		return new Constraint<Type>(EConstraintType.NOT_NULL, v -> ConstraintExpressionFactory.createNotNull(v));
	}

	/**
	 * 
	 * @return constraint "string is not empty"
	 */
	public static final IConstraint<String> createStringNotEmpty()
	{
		return new Constraint<String>(EConstraintType.NOT_EMPTY, v -> ConstraintExpressionFactory.createStringNotEmpty(v));
	}

	/**
	 * 
	 * @return constraint "string is required"
	 */
	public static final IConstraint<String> createStringRequired()
	{
		return new Constraint<String>(EConstraintType.REQUIRED, v -> ConstraintExpressionFactory.createStringNotEmpty(v));
	}

	/**
	 * 
	 * @param max
	 * @return constraint "string length {@code <= } max
	 */
	public static final IConstraint<String> createStringMaxSize(int max)
	{
		return new ConstraintOneParam<String, Integer>(EConstraintType.STRING_MAX_SIZE, (v, m) -> ConstraintExpressionFactory.createStringMaxSize(v, m), max);
	}

	/**
	 * 
	 * @param left
	 * @param right
	 * @return constraint "constraint left AND constraint right"
	 */
	public static final <Type> IConstraint<Type> createAnd(IConstraint<Type> left, IConstraint<Type> right)
	{
		return new BinaryConstraint<Type>(EConstraintType.BINARY_AND, left, right);
	}

	/**
	 * 
	 * @param left
	 * @param right
	 * @return @return constraint "constraint left OR constraint right"
	 */
	public static final <Type> IConstraint<Type> createOr(IConstraint<Type> left, IConstraint<Type> right)
	{
		return new BinaryConstraint<Type>(EConstraintType.BINARY_OR, left, right);
	}

}
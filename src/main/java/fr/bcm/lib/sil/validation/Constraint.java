/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.validation;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.math.expr.test.ITestExpression;
import fr.bcm.lib.sil.math.expr.value.ExpressionValue;

/**
 * Constraint default implementation
 * 
 * @param <Type> @see IConstraint
 */
public class Constraint<Type> implements IConstraint<Type> {

	protected final EConstraintType constraintType;

	// Value used in expressions
	protected ExpressionValue<Type> value;
	// Test to use
	protected ITestExpression test;

	@Override
	public EConstraintType getConstraintType()
	{
		return this.constraintType;
	}

	protected Constraint(final EConstraintType type)
	{
		DebugBox.predicateNotNull(type);
		this.constraintType = type;
		this.value = new ExpressionValue<>();
	}

	/**
	 * Ctor
	 * @param type
	 * @param creator
	 */
	public Constraint(final EConstraintType type, final ITestExpressionCreator<Type> creator)
	{
		this(type);
		DebugBox.predicateNotNull(creator);

		this.test = creator.createTestExpression(this.value);
	}

	@Override
	public Boolean check(final Type value)
	{
		this.value.set(value);
		return this.test.eval();
	}

}
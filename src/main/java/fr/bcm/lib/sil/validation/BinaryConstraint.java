/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.validation;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.math.expr.test.IDualTest;
import fr.bcm.lib.sil.math.expr.test.TestBooleanAnd;
import fr.bcm.lib.sil.math.expr.test.TestBooleanOr;

/**
 * Constraint: binary (and, or, xor, ..)
 *
 * @param <Type> @see IBinaryConstraint 
 */
public class BinaryConstraint<Type> implements IBinaryConstraint<Type> {

	protected final EConstraintType constraintType;

	protected final ConstraintAsBooleanExpression<Type> left;
	protected final ConstraintAsBooleanExpression<Type> right;
	protected IDualTest<Boolean> test;

	@Override
	public EConstraintType getConstraintType()
	{
		return this.constraintType;
	}

	@Override
	public IConstraint<Type> getLeft()
	{
		return this.left.getConstraint();
	}

	@Override
	public IConstraint<Type> getRight()
	{
		return this.right.getConstraint();
	}

	/**
	 * Ctor
	 * @param type
	 * @param left
	 * @param right
	 */
	public BinaryConstraint(final EConstraintType type, final IConstraint<Type> left, final IConstraint<Type> right)
	{
		DebugBox.predicateNotNull(type);
		DebugBox.predicateNotNull(left);
		DebugBox.predicateNotNull(right);

		this.constraintType = type;
		this.left = new ConstraintAsBooleanExpression<Type>(left);
		this.right = new ConstraintAsBooleanExpression<Type>(right);

		switch(type)
		{
			case BINARY_AND:
			{
				this.test = new TestBooleanAnd(this.left, this.right);
			} break;
			case BINARY_OR:
			{
				this.test = new TestBooleanOr(this.left, this.right);
			} break;
			default:
				throw new IllegalArgumentException("Not binary constraint, or unhandled enum");
		}

	}

	@Override
	public Boolean check(Type value)
	{
		this.left.setValue(value);
		this.right.setValue(value);

		return test.eval();
	}

}
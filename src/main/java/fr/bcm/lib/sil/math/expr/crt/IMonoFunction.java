/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.crt;

/**
 * Contract for expression taking one parameter
 * @param <TResult> Type of result
 * @param <TParam1> Type of parameter
 */
public interface IMonoFunction<TResult, TParam1> extends IExpression<TResult> {

	/**
	 * @return param1 value
	 */
	public TParam1 getP1();

	/**
	 * Set param1 value
	 */
	public void setP1(TParam1 p1);

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.test;

import fr.bcm.lib.sil.math.expr.crt.EOperatorName;
import fr.bcm.lib.sil.math.expr.crt.IExpression;

/**
 * Contract boolean expressions / tests: operator type
 */
public interface ITestExpression extends IExpression<Boolean> {

	/**
	 * @return Operator name
	 */
	public EOperatorName getOperatorName();

}
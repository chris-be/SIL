/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.crt;

/**
 * Type of expression
 */
public enum EExpressionType {

	// 0 argument, return a value
	  VALUE

	// 1 argument, return Boolean	get/set P1
	, TEST_UNARY
	// 2 arguments, return Boolean	get/set P1, P2
	, TEST_BINARY

	;

}
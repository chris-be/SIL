/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.test;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.math.expr.crt.DualFunctionBase;
import fr.bcm.lib.sil.math.expr.crt.EExpressionType;
import fr.bcm.lib.sil.math.expr.crt.IExpression;

/**
 * Base class for IBinaryTest
 * 
 * @param <Type> IDualTest
 */
public abstract class DualTestBase<Type> extends DualFunctionBase<Boolean, IExpression<Type>, IExpression<Type>> implements IDualTest<Type> {

	@Override
	public EExpressionType getExpressionType()
	{
		return EExpressionType.TEST_BINARY;
	}

	@Override
	public void set(IExpression<Type> p1, IExpression<Type> p2)
	{
		DebugBox.predicateNotNull(p1);
		DebugBox.predicateNotNull(p2);

		super.set(p1, p2);
	}

	/**
	 * Create (p1 operator p2) form
	 */
	@Override
	public String createRepresentation()
	{
		String rp1 = this.param1.createRepresentation();
		String rp2 = this.param2.createRepresentation();
		String op = this.getOperatorName().getRepresentation();

		return String.format(IExpression.DUAL_TEST_REPRESENTATION, rp1, op, rp2);
	}

}
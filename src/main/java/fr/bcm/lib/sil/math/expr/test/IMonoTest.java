/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.test;

import fr.bcm.lib.sil.math.expr.crt.IExpression;
import fr.bcm.lib.sil.math.expr.crt.IMonoFunction;

/**
 * Contract for unary tests: operator 'value'
 * 
 * @param <Type> @see IExpression
 */
public interface IMonoTest<Type> extends IMonoFunction<Boolean, IExpression<Type>>, ITestExpression {

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.crt;

/**
 * Contract for expression
 * 
 * @param <Result> Result type
 */
public interface IExpression<Result> {

	/**
	 * @return Type of expression
	 */
	public EExpressionType getExpressionType();

	/**
	 * Evaluate expression - parameters must be set before !
	 * @return Result of eval
	 */
	public Result eval();

	/**
	 * @return Human readable representation of expression (if possible)
	 */
	public String createRepresentation();

//
// Representation constants
//

	/**
	 * Used for "null" representation
	 */
	public static final String NULL_VALUE_REPRESENTATION		= "\"null\"";

	/**
	 * Used for "undefined" representation (ex: size of collection which is null")
	 */
	public static final String UNDEFINED_VALUE_REPRESENTATION	= "\"undef\"";

	/**
	 * Used for MonoFunction representation - function(p1)
	 */
	public static final String MONO_FUNCTION_REPRESENTATION = "%s(%s)";

	/**
	 * Used for DualFunction representation - function(p1, p2)
	 */
	public static final String DUAL_FUNCTION_REPRESENTATION = "%s(%s, %s)";

	/**
	 * Used for DualTest representation - (p1 operator p2)
	 */
	public static final String DUAL_TEST_REPRESENTATION = "(%s %s %s)";

}
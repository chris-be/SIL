/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/
package fr.bcm.lib.sil.math.expr.test;

import fr.bcm.lib.sil.math.expr.crt.EOperatorName;
import fr.bcm.lib.sil.math.expr.crt.IExpression;

/**
 * Test: {@code 'left' < 'right' }
 * 
 * @param <Type> @see IDualTest
 */
public class TestLessStrictly<Type extends Comparable<Type>> extends DualTestBase<Type> {

	@Override
	public EOperatorName getOperatorName()
	{
		return EOperatorName.LESS_STRICTLY;
	}

	/**
	 * 
	 */
	public TestLessStrictly()
	{	}

	/**
	 * 
	 * @param p1
	 * @param p2
	 */
	public TestLessStrictly(IExpression<Type> p1, IExpression<Type> p2)
	{
		super.set(p1, p2);
	}

	@Override
	public Boolean eval()
	{
		Type l = this.param1.eval();
		Type r = this.param2.eval();
		return l.compareTo(r) < 0;
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import fr.bcm.lib.sil.DebugBox;

/**
 * Handle number representation in different bases.
 */
public class MathBase {

	/**
	 * Default base 62 - used to create bases
	 */
	static private String	base62 = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	/**
	 * Base
	 */
	protected BigInteger base;

	/**
	 * Digits of base
	 */
	protected String digits;

	/**
	 * Digits in map for "fast" computation
	 */
	protected TreeMap<Integer, BigInteger> digitCache;

	/**
	 * Create base by size
	 * @param size Size of base (2..62)
	 */
	public MathBase(int size)
	{
		DebugBox.predicate(size >= 2 && size <= 62);
		String base = MathBase.base62.substring(0, size);
		this.setDigits(base);
	}

	/**
	 * Create base with a string
	 * @param base Codes to use for base
	 */
	public MathBase(String base)
	{
		DebugBox.predicateNotNull(base);
		this.setDigits(base);
	}

	/**
	 * Initialize digits
	 * @param base Codes to use for base
	 */
	protected void setDigits(String base)
	{
		DebugBox.predicateNotNull(base);
		this.digits = base;
		this.base = BigInteger.valueOf(this.digits.length());
	}

	private static TreeMap<Integer, BigInteger> createDigitCache(final String digits)
	{
		DebugBox.predicate((digits != null) && (digits.length() > 0));

		TreeMap<Integer, BigInteger> cache = new TreeMap<>();
		long pos = 0;
		Iterator<Integer> iter = digits.codePoints().iterator();
		while(iter.hasNext())
		{
			Integer i = iter.next();
			DebugBox.predicate(!cache.containsKey(i), "One code used more than once");
			cache.put(i, BigInteger.valueOf(pos));
			++pos;
		}

		return cache;
	}

	/**	Create integer representation
		@param value Value in this.digits base
		@return BigInteger representation
	*/
	public BigInteger toBigInteger(String value)
	{
		if(this.digitCache == null)
		{
			// First call ?
			this.digitCache = MathBase.createDigitCache(this.digits);
		}

		BigInteger	sum = BigInteger.ZERO;

		Iterator<Integer> iter = value.codePoints().iterator();
		while(iter.hasNext())
		{
			Integer digit = iter.next();

			sum = sum.multiply(this.base);
			// Converts each "digit" in decimal value
			BigInteger decimal = this.digitCache.get(digit);

			sum = sum.add(decimal);
		}

		return sum;
	}

	/**	Create list of this base values
		@param value 
		@return List of values in this base
	*/
	protected LinkedList<Integer> createBaseValues(BigInteger value)
	{
		LinkedList<Integer> baseValues = new LinkedList<>();

		do
		{
			BigInteger[] valueMod = value.divideAndRemainder(this.base);
			value = valueMod[0];

			int mod = valueMod[1].intValue();
			baseValues.addFirst(mod);

		} while(value.compareTo(BigInteger.ZERO) > 0);

		return baseValues;
	}

	/**	Create String representation
		@param value 
		@return String with this.digits base
	*/
	public String toString(BigInteger value)
	{
		LinkedList<Integer> baseValues = this.createBaseValues(value);
		return this.toString(baseValues);
	}

	/**	Create String representation from values in this base
	 * @param baseValues
	 * @return String with this.digits base
	 */
	protected String toString(final List<Integer> baseValues)
	{
		StringBuilder sb = new StringBuilder(baseValues.size());
		for(Integer index : baseValues)
		{
			Integer code = this.digits.codePointAt(index);
			sb.append( Character.toChars(code) );
		}

		return sb.toString();
	}

	/**	Create list of this base values
		@param value 
		@return List of values in this base
	*/
	protected LinkedList<Integer> createBaseValues(long value)
	{
		LinkedList<Integer> baseValues = new LinkedList<>();

		int base = this.digits.length();
		do
		{
			int mod = (int)(value % base);
			value = value / base;

			baseValues.addFirst(mod);

		} while(value > 0);

		return baseValues;
	}

	/**	Create String representation
		@param value
		@return String with this.digits base
	*/
	public String toString(long value)
	{
		LinkedList<Integer> baseValues = this.createBaseValues(value);
		return this.toString(baseValues);
	}

	/** Convert from a base to this on
		@param value Value to convert
		@param fromBase Base of value
		@return Converted value
	*/
	public String convertFrom(final MathBase fromBase, String value)
	{
		BigInteger bi = fromBase.toBigInteger(value);
		return this.toString(bi);
	}

	/** Convert from a base to another
		@param fromBase Base of value
		@param toBase Base to convert to 
		@param value Value to convert
		@return Converted value
	*/
	public static String convert(final MathBase fromBase, final MathBase toBase, String value)
	{
		BigInteger bi = fromBase.toBigInteger(value);
		return toBase.toString(bi);
	}

}
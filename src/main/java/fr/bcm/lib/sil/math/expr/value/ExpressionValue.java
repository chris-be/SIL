/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.value;

import fr.bcm.lib.sil.math.expr.crt.EExpressionType;
import fr.bcm.lib.sil.math.expr.crt.IExpression;

/**
 * Expression returning a value
 * 
 * @param <Result> @see IExpression
 */
public class ExpressionValue<Result> implements IExpression<Result> {

	Result value;

	/**
	 * Set value
	 * @param v
	 */
	public void set(final Result v)
	{
		this.value = v;
	}

	@Override
	public EExpressionType getExpressionType()
	{
		return EExpressionType.VALUE;
	}

	/**
	 * Ctor
	 */
	public ExpressionValue()
	{
		this(null);
	}

	/**
	 * Ctor
	 * @param v
	 */
	public ExpressionValue(final Result v)
	{
		this.set(v);
	}

	@Override
	public Result eval()
	{
		return this.value;
	}

	@Override
	public String createRepresentation()
	{
		return (this.value == null) ? IExpression.NULL_VALUE_REPRESENTATION : this.value.toString();
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/
package fr.bcm.lib.sil.math.expr.test;

import fr.bcm.lib.sil.CVS_Comparable;
import fr.bcm.lib.sil.math.expr.crt.EOperatorName;
import fr.bcm.lib.sil.math.expr.crt.IExpression;

/**
 * Test: 'left' != 'right', using 'equal'
 */
public class TestNotEqual<Type> extends DualTestBase<Type> {

	@Override
	public EOperatorName getOperatorName()
	{
		return EOperatorName.NOT_EQUAL;
	}

	public TestNotEqual()
	{	}

	public TestNotEqual(IExpression<Type> p1, IExpression<Type> p2)
	{
		super.set(p1, p2);
	}

	@Override
	public Boolean eval()
	{
		Type l = this.param1.eval();
		Type r = this.param2.eval();
		return !CVS_Comparable.equals(l, r);
	}

}
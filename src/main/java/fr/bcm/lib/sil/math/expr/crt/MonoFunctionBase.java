/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.crt;

/**
 * Base class for IMonoFunction
 */
public abstract class MonoFunctionBase<TResult, TParam1> implements IMonoFunction<TResult, TParam1> {

	protected TParam1 param1;

	@Override
	public TParam1 getP1()
	{
		return this.param1;
	}

	@Override
	public void setP1(TParam1 p1)
	{
		this.param1 = p1;
	}

	/**
	 * Create function(p1) form
	 */
	protected String createRepresentation(String funcName)
	{
		String rp1 = (this.param1 == null) ? IExpression.NULL_VALUE_REPRESENTATION : this.param1.toString();
		return String.format(IExpression.MONO_FUNCTION_REPRESENTATION, funcName, rp1);
	}

	@Override
	public String toString()
	{
		return this.createRepresentation();
	}

}
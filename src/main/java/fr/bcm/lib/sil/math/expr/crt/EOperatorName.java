/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.crt;

import fr.bcm.lib.sil.DebugBox;

/**
 * Name of operator
 */
public enum EOperatorName {

	// Tests
	  IS_NULL				("is null")
	, IS_EMPTY				("is empty")

	// Comparisons
	, EQUAL					("==")
	, NOT_EQUAL				("!=")

	, LESS_STRICTLY			("<")
	, LESS_EQUAL			("<=")
	, GREATER_STRICTLY		(">")
	, GREATER_EQUAL			(">=")

	// Logical operator
	, NOT					("!")
	, AND					("&&")
	, OR					("||")

	// MonoFunc
	, SIZE					("size")

	;

	private String representation;

	/**
	 * Get operator representation
	 */
	public String getRepresentation()
	{
		return this.representation;
	}

	private EOperatorName(String representation)
	{
		DebugBox.predicateNotNull(representation);
		this.representation = representation;
	}

}
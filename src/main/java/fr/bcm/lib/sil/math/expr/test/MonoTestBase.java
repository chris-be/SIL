/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.test;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.math.expr.crt.EExpressionType;
import fr.bcm.lib.sil.math.expr.crt.IExpression;
import fr.bcm.lib.sil.math.expr.crt.MonoFunctionBase;

/**
 * Base class for IMonoTest
 * 
 * @param <Type> @see IMonoTest
 */
public abstract class MonoTestBase<Type> extends MonoFunctionBase<Boolean, IExpression<Type>> implements IMonoTest<Type> {

	@Override
	public EExpressionType getExpressionType()
	{
		return EExpressionType.TEST_UNARY;
	}

	@Override
	public void setP1(IExpression<Type> p1)
	{
		DebugBox.predicateNotNull(p1);
		super.setP1(p1);
	}

	/**
	 * Create function(p1) form
	 */
	@Override
	public String createRepresentation()
	{
		String rp1 = this.param1.createRepresentation();
		String op = this.getOperatorName().getRepresentation();

		return String.format(IExpression.MONO_FUNCTION_REPRESENTATION, op, rp1);
	}

}
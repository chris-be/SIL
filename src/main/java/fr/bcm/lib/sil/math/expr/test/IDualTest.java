/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.test;

import fr.bcm.lib.sil.math.expr.crt.IDualFunction;
import fr.bcm.lib.sil.math.expr.crt.IExpression;

/**
 * Contract for binary tests: 'left value' operator 'right value'
 * 
 * @param <Type> @see IExpression
 */
public interface IDualTest<Type> extends IDualFunction<Boolean, IExpression<Type>, IExpression<Type>>, ITestExpression {

}
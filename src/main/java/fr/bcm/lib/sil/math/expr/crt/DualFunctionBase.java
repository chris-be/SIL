/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.crt;

/**
 * Base class for IDualFunction
 */
public abstract class DualFunctionBase<TResult, TParam1, TParam2> extends MonoFunctionBase<TResult, TParam1> implements IDualFunction<TResult, TParam1, TParam2> {

	protected TParam2 param2;

	@Override
	public TParam2 getP2()
	{
		return this.param2;
	}

	@Override
	public void setP2(TParam2 v)
	{
		this.param2 = v;
	}

	@Override
	public String toString()
	{
		return this.createRepresentation();
	}

	/**
	 * @Convenience
	 * @param p1
	 * @param p2
	 */
	public void set(TParam1 p1, TParam2 p2)
	{
		this.setP1(p1);
		this.setP2(p2);
	}

}
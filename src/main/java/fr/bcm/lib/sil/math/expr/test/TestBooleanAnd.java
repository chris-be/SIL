/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/
package fr.bcm.lib.sil.math.expr.test;

import fr.bcm.lib.sil.math.expr.crt.EOperatorName;
import fr.bcm.lib.sil.math.expr.crt.IExpression;

/**
 * Boolean test: 'left' and 'right'
 */
public class TestBooleanAnd extends DualTestBase<Boolean> {

	@Override
	public EOperatorName getOperatorName()
	{
		return EOperatorName.AND;
	}

	/**
	 * 
	 */
	public TestBooleanAnd()
	{	}

	/**
	 * 
	 * @param p1
	 * @param p2
	 */
	public TestBooleanAnd(IExpression<Boolean> p1, IExpression<Boolean> p2)
	{
		super.set(p1, p2);
	}

	@Override
	public Boolean eval()
	{
		Boolean l = this.param1.eval();
		Boolean r = this.param2.eval();
		return l && r;
	}

}
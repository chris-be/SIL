/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.test;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.math.expr.crt.EOperatorName;
import fr.bcm.lib.sil.math.expr.crt.IExpression;

/**
 * Boolean operator: not
 */
public class NotOperator extends MonoTestBase<Boolean> {

	@Override
	public EOperatorName getOperatorName()
	{
		return EOperatorName.NOT;
	}

	/**
	 * 
	 */
	public NotOperator()
	{
		this(null);
	}

	/**
	 * 
	 * @param p1
	 */
	public NotOperator(IExpression<Boolean> p1)
	{
		this.setP1(p1);
	}

	@Override
	public Boolean eval()
	{
		DebugBox.predicateNotNull(this.param1);
		return !this.param1.eval();
	}

	@Override
	public String createRepresentation()
	{
		return "!" + this.param1.createRepresentation();
	}

}
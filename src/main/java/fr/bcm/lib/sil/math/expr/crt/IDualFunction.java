/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.math.expr.crt;

/**
 * Contract for expression taking two parameters
 * @param <TParam2> Type of 2th parameter
 */
public interface IDualFunction<TResult, TParam1, TParam2> extends IMonoFunction<TResult, TParam1> {

	/**
	 * @return param2 value
	 */
	public TParam2 getP2();

	/**
	 * Set param2 value
	 */
	public void setP2(TParam2 p2);

}
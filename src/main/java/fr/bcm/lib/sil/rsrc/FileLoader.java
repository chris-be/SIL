/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.rsrc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.file.CVS_File;
import fr.bcm.lib.sil.file.SubFileLocator;

/**
 * Uses "file system" to load resources
 * @see IResourceLoader
 */
public class FileLoader implements IResourceLoader {

	/**
	 * SubFileLocator
	 */
	protected SubFileLocator	locator;

	/**
	 * 
	 * @param root Path where to search files
	 * @throws IOException
	 */
	public FileLoader(final File root) throws IllegalArgumentException, IOException
	{
		DebugBox.predicate(root.isDirectory(), "File does not exist or is not a directory");

		this.locator = new SubFileLocator(root);
	}

	/**
	 * @see AutoCloseable
	 */
	@Override
	public void close() throws Exception
	{	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public boolean fileExists(final File fileName)
	{
		File file = this.locator.cptFileLocation(fileName);
		return file.isFile();
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public ResourceInfo getResourceInfo(final File fileName)
	{
		File file = this.locator.cptFileLocation(fileName);
		return file.exists() ? new ResourceInfo(file) : null;
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public InputStream openResource(final File fileName) throws IOException
	{
		File file = this.locator.cptFileLocation(fileName);
		if(!file.exists())
		{
			throw new FileNotFoundException();
		}

		return new FileInputStream(file);
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public URL createURL(final File fileName)
	{
		URL url;

		try
		{
			File file = this.locator.cptFileLocation(fileName);
			url = CVS_File.createLocalURL(file);
		}
		catch(MalformedURLException e)
		{
			url = null;
		}

		return url;
	}

}
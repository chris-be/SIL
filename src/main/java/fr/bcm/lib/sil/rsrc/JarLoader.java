/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.rsrc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import fr.bcm.lib.sil.CVS_Jar;
import fr.bcm.lib.sil.DebugBox;

/**
 * Load resources in jar file
 * @see IResourceLoader
 */
public class JarLoader implements IResourceLoader {

	protected JarFile		jar;

	/**
	 *
	 * @param fileName Jar file to use
	 * @throws IOException
	 */
	public JarLoader(final File fileName) throws IllegalArgumentException, IOException
	{
		DebugBox.predicate(fileName.exists() && !fileName.isDirectory());
		this.jar = new JarFile(fileName);
	}

	/**
	 * @see AutoCloseable
	 */
	@Override
	public void close() throws Exception
	{
		if(this.jar != null)
		{
			this.jar.close();
		}
	}

	/**
	 * Internal method to get jar entry
	 * @param fileName
	 */
	protected JarEntry cptJarEntry(final File fileName)
	{
		assert this.jar != null : "Shouldn't be. How was this object created ?";

		return jar.getJarEntry(fileName.getPath());
	}

	/**
	 * @see IResourceLoader
	 */
	public boolean fileExists(final File fileName)
	{
		JarEntry je = this.cptJarEntry(fileName);
		return (je != null) && (!je.isDirectory());
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public ResourceInfo getResourceInfo(final File fileName)
	{
		JarEntry je = this.cptJarEntry(fileName);
		return (je != null) ? new ResourceInfo(je) : null;
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public InputStream openResource(File fileName) throws IOException
	{
		JarEntry je = this.cptJarEntry(fileName);
		if(je == null)
		{
			throw new FileNotFoundException();
		}

		return jar.getInputStream(je);
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public URL createURL(final File fileName)
	{
		URL url;

		try
		{
			url = CVS_Jar.createLocalURL(this.jar.getName(), fileName.getPath());
		}
		catch(MalformedURLException e)
		{
			url = null;
		}

		return url;
	}

}
/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.rsrc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import fr.bcm.lib.sil.DebugBox;

/**
 * Uses "class loader" to load resources
 * @see IResourceLoader
 */
public class JNLPLoader implements IResourceLoader {

	/**
	 * ClassLoader to use
	 */
	protected ClassLoader		classLoader;

	/**
	 * 
	 * @param cl ClassLoader to use
	 * @throws IOException
	 */
	public JNLPLoader(final ClassLoader cl) throws IllegalArgumentException, IOException
	{
		DebugBox.predicateNotNull(cl);

		//this.locator = new SubFileLocator(root);
		this.classLoader = cl;
	}

	/**
	 * @Convenience
	 */
	protected URL getResourceURL(final File fileName)
	{
		String path = fileName.getPath();
		return this.classLoader.getResource(path);
	}

	/**
	 * @see AutoCloseable
	 */
	@Override
	public void close() throws Exception
	{	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public boolean fileExists(final File fileName)
	{
		return this.getResourceURL(fileName) != null;
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public ResourceInfo getResourceInfo(final File fileName)
	{
		URL url = this.getResourceURL(fileName);
		File file;
		try
		{
			file = new File(url.toURI());
		}
		catch(Exception e)
		{
			file = null;
		}

		return (file != null) ? new ResourceInfo(file) : null;
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public InputStream openResource(final File fileName) throws IOException
	{
		String path = fileName.getPath();
		return this.classLoader.getResourceAsStream(path);
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public URL createURL(final File fileName)
	{
		return this.getResourceURL(fileName);
	}

}
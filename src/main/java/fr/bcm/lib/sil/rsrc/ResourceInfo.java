/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.rsrc;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.zip.ZipEntry;

import fr.bcm.lib.sil.DebugBox;

/**
 * Get info for resource (file, zip/jar entry)
 */
public class ResourceInfo {

	static final ZoneId ZONE_ID = ZoneId.systemDefault();

	// protected Instant creationDate;

	/**
	 * null if unknown
	 */
	protected LocalDateTime lastModified;

	/**
	 * null if unknown
	 */
	protected LocalDateTime lastAccess;

	public LocalDateTime getLastModified()
	{
		return this.lastModified;
	}

	public LocalDateTime getLastAccess()
	{
		return this.lastAccess;
	}

	public ResourceInfo(final File file)
	{
		DebugBox.predicateNotNull(file);
		this.set(file.getAbsolutePath());
	}

	public ResourceInfo(String fileName)
	{
		this.set(fileName);
	}

	public ResourceInfo(final ZipEntry ze)
	{
		DebugBox.predicateNotNull(ze);

		this.lastModified = ResourceInfo.from( ze.getLastModifiedTime() );
		this.lastAccess = ResourceInfo.from( ze.getLastAccessTime() );
	}

	protected void set(String fileName)
	{
		DebugBox.predicateNotNull(fileName);

		BasicFileAttributes attributes;
		try
		{
			Path path = Paths.get(fileName);
			attributes = Files.readAttributes(path, BasicFileAttributes.class);
		}
		catch(IOException e)
		{
			this.lastModified = null;
			this.lastAccess = null;
			return;
		}

		this.lastModified = ResourceInfo.from( attributes.lastModifiedTime() );
		this.lastAccess = ResourceInfo.from( attributes.lastAccessTime() );
	}

	static public LocalDateTime from(FileTime fileTime)
	{
		return LocalDateTime.ofInstant(fileTime.toInstant(), ZONE_ID);
	}

}
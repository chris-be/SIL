/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.rsrc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.file.SubFileLocator;

/**
 * SubResourceLoader: IResourceLoader to "sub resources" - resource names are prefixed.
 * Typical usage: serve static files
 */
public class SubResourceLoader implements IResourceLoader {

	protected IResourceLoader resourceLoader;

	protected SubFileLocator fileLocator;

	/**
	 * Close used IResourceLoader when this.closed
	 */
	protected boolean closeSubLoader;

	public boolean isCloseSubLoader()
	{
		return this.closeSubLoader;
	}

	public void setCloseSubLoader(boolean v)
	{
		this.closeSubLoader = v;
	}

	public SubResourceLoader(IResourceLoader resourceLoader)
	{
		DebugBox.predicateNotNull(resourceLoader);
		this.resourceLoader = resourceLoader;
		this.fileLocator = new SubFileLocator();
	}

	/**
	 * Set root
	 * @param root
	 */
	public void setRoot(String root)
	{
		this.fileLocator.setRoot(root);
	}

	/**
	 * Prepare location regards to "prefix" and "suffix".
	 * <br>Always call this method (permits inheritance)
	 * @param resourceFile
	 */
	protected File cptFileLocation(File resourceFile)
	{
		return this.fileLocator.cptFileLocation(resourceFile);
	}

	@Override
	public void close() throws Exception
	{
		if(this.closeSubLoader)
		{
			this.resourceLoader.close();
		}
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public boolean fileExists(final File file)
	{
		File location = this.cptFileLocation(file);
		return this.resourceLoader.fileExists(location);
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public ResourceInfo getResourceInfo(File fileName)
	{
		File location = this.cptFileLocation(fileName);
		return this.resourceLoader.getResourceInfo(location);
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public InputStream openResource(final File file) throws IOException
	{
		File location = this.cptFileLocation(file);
		return this.resourceLoader.openResource(location);
	}

	/**
	 * @see IResourceLoader
	 */
	@Override
	public URL createURL(final File fileName)
	{
		File location = this.cptFileLocation(fileName);
		return this.resourceLoader.createURL(location);
	}

}
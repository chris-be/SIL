/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.rsrc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Contract to implement to load resources
 * @see fr.bcm.lib.sil.CVS_Jar
 * @see JarLoader
 * @see FileLoader
 */
public interface IResourceLoader extends AutoCloseable {

	/**
	 * Check if resource exist
	 *
	 * @param fileName 'dom/xxx/.../fff.ext'
	 * @return false if not found 
	 */
	public boolean fileExists(final File fileName);

	/**
	 * Get resource info
	 *
	 * @param fileName 'dom/xxx/.../fff.ext'
	 * @return null If not found
	 */
	public ResourceInfo getResourceInfo(final File fileName);

	/**
	 * Create an input stream from a resource
	 *
	 * @param fileName 'dom/xxx/.../fff.ext'
	 * @return opened stream
	 * @throws IOException
	 */
	public InputStream openResource(final File fileName) throws IOException;

	/**
	 * For ClassLoader
	 * @param fileName
	 */
	public URL createURL(final File fileName);

}
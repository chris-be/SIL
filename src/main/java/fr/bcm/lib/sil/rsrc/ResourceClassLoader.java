/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of SIL - SImple Library.
   For the full copyright and license information, please refer
   to the LICENSE file that was distributed with this source code.
*/

package fr.bcm.lib.sil.rsrc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import fr.bcm.lib.sil.DebugBox;

/**
 * Wrapper to use IResourceLoader as ClassLoader
 * <br>Tested for ResourceBundle only
 * @see IResourceLoader
 */
public class ResourceClassLoader extends ClassLoader {

	IResourceLoader resourceLoader;

	/**
	 * 
	 * @param resourceLoader
	 * @throws IllegalArgumentException
	 */
	public ResourceClassLoader(IResourceLoader resourceLoader) throws IllegalArgumentException
	{
		DebugBox.predicateNotNull(resourceLoader);
		this.resourceLoader = resourceLoader;
	}

	@Override
	public URL getResource(String name)
	{
		File fileName = new File(name);
		return resourceLoader.createURL(fileName);
	}

	@Override
	protected URL findResource(String name)
	{
		// TODO Auto-generated method stub
		return super.findResource(name);
	}

	@Override
	public InputStream getResourceAsStream(String name)
	{
		InputStream is;
		try
		{
			File fileName = new File(name);
			is = this.resourceLoader.openResource(fileName);
		}
		catch(IOException e)
		{
			is = null;
		}

		return is;
	}

}